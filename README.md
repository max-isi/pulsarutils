# pulsarutils
This repo includes python tools to set up 
`lalapps_pulsar_parameter_estimation_nested` workflows and manipulate  their 
results. It is organized with the structure of a Python package, with the core 
files located in the `pulsarutils/` directory and the executables in `bin/`. 
Scripts built to achieve a particular "one-time" task are included in 
`scripts/`.

At the moment of writing this, the main executable is `cw_model_selection`. 
This can be used in two "modes": `open` and `closed`. The former analyzes the 
data directly without noise shuffling or injections, the later searches over 
several blinded instatiations of data with or without injections. In both cases,
the behavior is controled by configuration files in the INI format.

For postprocessing use:
 * `cw_nest2pos` to obtain posterior samples from nested samples
 * `post_cw_model_selection` to produces Bayes factor and posterior plots
 * `plot_svt_uls` to plot upper limits from scalar-vector-tensor models (1F or 2F)
