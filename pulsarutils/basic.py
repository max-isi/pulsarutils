import numpy as np
import random
import socket
import os
import itertools
from warnings import warn
from scipy.interpolate import interp1d
from scipy.optimize import minimize
from . import stats
from lal import DAYSID_SI

###############################################################################
# CONSTANTS

SS = DAYSID_SI  # Seconds in a sidereal day
SIDFREQ = 2 * np.pi / SS  # Sidereal angular frequency of Earth
SECTORAD = 2 * np.pi / 86400.0
EARTHRADIUS = 6378.137e3  # Earth radius (m)
C = 299792458.  # Speed of light (m/s)
G = 6.67408e-11  # Newton's constant (m^3/(kg s^2) )

H95_BAYES_SCALE = 10.8  # Bayesian time domain search 95% scaling factor

IFOS = ['H1', 'H2', 'L1', 'V1', 'G1']

RUNS = ['S5', 'S6', 'VSR2', 'VSR4', 'O1', 'O2', 'O1O2']

MPLPARAMS = {
    'text.usetex': True,  # use LaTeX for all text
    'axes.linewidth': 1,  # set axes linewidths to 0.5
    'axes.grid': False,   # add a grid
    'axes.labelweight': 'normal',
    'font.family': 'serif',
    'font.size': 24,
    'font.serif': 'Computer Modern Roman'
}


def tokenize(string, **kwargs):
    """Performs common wildcard substitutions in strings.

    Example:
        $ tokenize('my_string_(IFO).ext', ifo='H1')
        "my_string_H1.ext"

    :param string: string on which to effect substitution (str).
    :param kwargs: token = "string" pairs to substitute (str).
    """
    if string is not None:
        for wildcard, meaning in kwargs.iteritems():
            string = string.replace("(%s)" % wildcard.upper(), meaning)
    return string


def get_label(key, m0='', m1='', logbase=None, prob='', ul=False):
    key = key.upper()
    # odds ratio or bayes factor?
    if any([m in ['S', 'nGR', 'NGR', 'ngr', 'C'] for m in [m0, m1]]):
        prob = prob or "O"
    else:
        prob = "B"
    # what kind of log?
    if logbase is None:
        log = r"\ln"
    else:
        log = r"\log_{%i}" % logbase
    # if key == 'HINJ' and m0 == 'GR':
    #     key == 'HGR'
    # choose label
    labels = {
        'SQRTB': r"${\rm sgn}\left(%(log)s {\cal %(p)s}^{\rm %(k0)s}"
                 r"_{\rm %(k1)s}\right)|%(log)s {\cal %(p)s}^{\rm %(k0)s}"
                 r"_{\rm %(k1)s}|^\frac{1}{2}$"
                 % {'p': prob, 'k0': m0, 'k1': m1, 'log': log},
        'B': r"$%s {\cal %s}^{\rm %s}_{\rm %s}$" % (log, prob, m0, m1),
        'HBUL': r"$h_{\rm s}^{95\%}$",
        'HVUL': r"$h_{\rm v}^{95\%}$",
        'HTUL': r"$h_{\rm t}^{95\%}$",
        'H0': r"$h_0$",
        'H0UL': r"$h_0^{95\%}$",
        'LOGHBUL': r"$\log_{10} h_{\rm b}^{95\%}$",
        'HGR': r"$h_{\rm t}$", #=\left(a_+^2 + a_\times^2\right)^\frac{1}{2}}$",
        'HB': r"$h_{\rm s}$",
        'HINJ': r"$h_{\rm %s}$" % (m0 or "inj"),
        'SCALED': r"$\ln {\cal %(p)s}^{\rm %(k0)s}_{\rm %(k1)s}/\left(\ln^2"\
                   " {\cal B}^{\rm %(k0)s}_{\rm N} + \ln^2 "\
                   "{\cal B}^{\rm %(k1)s}_{\rm N}\right)^\frac{1}{4}$"
                  % {'p': prob, 'k0': m0, 'k1': m1},
        'ASD': r"$\sqrt{S_n}$ ($1/\sqrt{\rm Hz}$)", # "r'$|\tilde{h}|\sqrt{f}$',
        'PSD': r"$S_n$ ($1/{\rm Hz}$)",  # "r'$|\tilde{h}|\sqrt{f}$',
        'FGW': r"$f_{\rm GW}$ (Hz)",
        'LNBCI': r"$%s {\cal O}^{\rm S}_{\rm I}$" % log,
        'HSCALAR': r"$h_{\rm s}$",
        'HVECTOR': r"$h_{\rm v}$",
        'HTENSOR': r"$h_{\rm t}$",
        'HSCALAR_F': r"$h_{\rm s_1}$",
        'HVECTOR_F': r"$h_{\rm v_1}$",
        'HTENSOR_F': r"$h_{\rm t_1}$",
    }
    if key in labels.keys():
        label = labels[key]
        if ul:
            label = r"$%s^{%i \%%}$" % (label.replace('$', ''), ul)
    else:
        warn("Unrecognized label key: %r" % key)
        label = key.replace('_', '-')
    if key[0] == 'H':
        if 'G4V' in label:
            label = label.replace('G4V', 'v')
        if 'GR' in label:
            label = label.replace('GR', 't')
    return label


def get_psd(ifo, advanced=True):
    ifo = ifo.upper()
    if advanced:
        if ifo == 'V1':
            from lalsimulation import SimNoisePSDAdvVirgo
            psd = SimNoisePSDAdvVirgo
        else:
            from lalsimulation import SimNoisePSDaLIGOZeroDetHighPower
            psd = SimNoisePSDaLIGOZeroDetHighPower
    else:
        raise NotImplementedError("Initial-era PSDs not implemented.")
    return psd


def is_hdf5(path):
    """ Determines whether `path` corresponds to an HDF5 file.

    Arguments
    ---------
    path: str
        path to file.

    Returns
    -------
    ishdf5: bool
    """
    path_file, path_ext = os.path.splitext(path)
    if path_ext.lower() in ['.hdf', '.h5', '.hdf5']:
        ishdf5 = True
    else:
        ishdf5 = False
    return ishdf5


###############################################################################
# CONVERSIONS (FROM polHTC)

def hmsformat(*args):
    # Takes in time string ("HH:MM:SS") or string tuple ("HH", "MM", "SS") and
    # returns floats h, m & s corresponding to hours, minutes and seconds.
    if isinstance(args[0], basestring):
        args = (args,)
    if len(args[0]) == 1:
        # Assume hh:mm:ss format
        if not isinstance(args, basestring):
            argument = args[0][0]
        else:
            argument = args

        hms = argument.split(':')
        if len(hms) == 3:
            h, m, s = [float(x) for x in hms]
        elif len(hms) == 2:
            m, s = [float(x) for x in hms]
            h = 0.0
        elif len(hms) == 1:
            s = [float(x) for x in hms]
            h = 0.0
            m = 0.0
        else:
            raise AttributeError('hmsformat cannot convert: %r' % argument)

    elif len(args[0]) == 3:
        h, m, s = [float(x) for x in args[0]]

    else:
        raise AttributeError('hmsformat can\'t take %d arguments' % len(args))

    return h, m, s


def hms_rad(*args):
    # Converts hours, minutes, seconds to radians using the sidereal angular
    # frequency of the Earth. Argument: time string "HH:MM:SS" or tuple of
    # strings ["HH","MM","SS"].
    h, m, s = hmsformat(args)
    sign = -1 if h < 0 else 1
    h = np.abs(h)
    sec = s + 60. * (m + 60. * h)
    output = sign * sec * SECTORAD
    return output


def dms_rad(*args):
    # Converts degrees, minutes, seconds to radians. Argument: time string
    # "HH:MM:SS" or tuple of strings ["HH","MM","SS"].
    d, m, s = hmsformat(args)
    sign = np.sign(d)
    d = np.abs(d)
    dec_degrees = d + m / 60. + s / (60. ** 2)
    return np.radians(sign * dec_degrees)


def masyr_rads(masyr):
    # Converts milliarcseconds/yr to radians/second. Argument: float.
    asyr = masyr * 10 ** -3  # mas/yr to arcseconds/yr
    radyr = asyr * np.pi / (180 * 60 * 60.)  # as/yr to rad/yr
    rads = radyr / SS  # rad/yr to rad/s
    return rads


def mjd_gps(mjd):
    # Converts MJD time to GPS time (taken from LALBarycenter.c line 749)
    # Argument: float.
    tgps = 86400. * (mjd - 44244.) - 51.184
    return tgps


###############################################################################
# Classes

class Parameter(object):
    _FLOAT_PARAMS = ["F", "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8",
                     "F9", "P", "P0", "P1", "P2", "P3", "P4", "P5", "P6", "P7",
                     "P8", "P9", "PEPOCH", "POSEPOCH", "DMEPOCH", "DM", "DM1",
                     "START", "FINISH", "NTOA", "EPHVER", "CHI2R", "TRES",
                     "TZRMJD", "TZRFRQ", "NITS", "NE_SW", "A1", "XDOT", "E",
                     "ECC", "EDOT", "T0", "PB", "PBDOT", "OM", "OMDOT", "EPS1",
                     "EPS2", "EPS1DOT", "EPS2DOT", "TASC", "LAMBDA", "BETA",
                     "RA_RAD", "DEC_RAD", "GAMMA", "SINI", "M2", "MTOT", "FB0",
                     "FB1", "FB2", "ELAT", "ELONG", "PMRA", "PMDEC", "DIST",
                     # GW PARAMETERS
                     "H0", "COSIOTA", "PSI", "PHI0", "THETA", "I21", "I31",
                     "C22", "C21", "PHI22", "PHI21", "SNR", "COSTHETA", "IOTA",
                     "HPLUS", "HCROSS", "PHI0TENSOR", "PSITENSOR", "HVECTORX",
                     "HVECTORY", "PHI0VECTOR", "PSIVECTOR", "HSCALARB",
                     "HSCALARL", "PHI0SCALAR", "PSISCALAR", "Q22",
                     # 21 HARMONIC
                     "HPLUS_F", "HCROSS_F", "PHI0TENSOR_F", "PSITENSOR_F",
                     "HVECTORX_F", "HVECTORY_F", "PHI0VECTOR_F", "PSIVECTOR_F",
                     "HSCALARB_F", "HSCALARL_F", "PHI0SCALAR_F", "PSISCALAR_F",
                     "H0_F"]

    _STR_PARAMS = ["FILE", "PSR", "PSRJ", "NAME", "RAJ", "DECJ", "RA", "DEC",
                   "EPHEM", "CLK", "BINARY", "UNITS", "TZRSITE", "TIMEEPH",
                   "DILATEFREQ", "PLANET_SHAPIRO", "T2CMETHOD",
                   "CORRECT_TROPOSPHERE"]

    _COMPOSITE_PARAMS = ['HTENSOR', 'HVECTOR', 'HSCALAR',
                         'HTENSOR_F', 'HVECTOR_F', 'HSCALAR_F']

    _ALL_PARAMS = list(set(_FLOAT_PARAMS + _STR_PARAMS + _COMPOSITE_PARAMS))

    _DEFAULT_RANGES = {
        # source
        "COSIOTA": (-1., 1.),
        "IOTA": (0., 2.0*np.pi),
        "PSI": (-np.pi, np.pi),
        # amplitude
        "C22": (1E-28, 1E-25),
        "H0": (2E-28, 2E-25),
        "HPLUS": (1E-28, 1E-25),
        "HCROSS": (1E-28, 1E-25),
        "HVECTORX": (1E-28, 1E-25),
        "HVECTORY": (1E-28, 1E-25),
        "HSCALARB": (1E-28, 1E-25),
        "HSCALARL": (1E-28, 1E-25),
        # amplitude (21 harmonic)
        "H0_F": (2E-28, 2E-25),
        "HPLUS_F": (1E-28, 1E-25),
        "HCROSS_F": (1E-28, 1E-25),
        "HVECTORX_F": (1E-28, 1E-25),
        "HVECTORY_F": (1E-28, 1E-25),
        "HSCALARB_F": (1E-28, 1E-25),
        "HSCALARL_F": (1E-28, 1E-25),
        # phase
        "PHI0": (0., 2.0*np.pi),
        "PHI22": (0., 2.0*np.pi),
        "PHI0TENSOR": (0., 2.0*np.pi),
        "PSITENSOR": (0., 2.0*np.pi),
        "PHI0VECTOR": (0., 2.0*np.pi),
        "PSIVECTOR": (0., 2.0*np.pi),
        "PHI0SCALAR": (0., 2.0*np.pi),
        "PSISCALAR": (0., 2.0*np.pi),
        # phase (21 harmonic)
        "PHI0_F": (0., 2.0*np.pi),
        "PHI0TENSOR_F": (0., 2.0*np.pi),
        "PSITENSOR_F": (0., 2.0*np.pi),
        "PHI0VECTOR_F": (0., 2.0*np.pi),
        "PSIVECTOR_F": (0., 2.0*np.pi),
        "PHI0SCALAR_F": (0., 2.0*np.pi),
        "PSISCALAR_F": (0., 2.0*np.pi)
    }

    _LABELS = {
        # 22 HARMONIC
        "C22": r"$C_{22}$",
        "PHI22": r"$\phi_{22}$",
        "HPLUS": r"$h_+$",
        "HCROSS": r"$h_\times$",
        "HVECTORX": r"$h_{\rm x}$",
        "HVECTORY": r"$h_{\rm y}$",
        "HSCALARB": r"$h_{\rm b}$",
        "HSCALARL": r"$h_{\rm l}$",
        "PHI0TENSOR": r"$\phi_+$", #r"$\phi_0^{\rm t}$",
        "PSITENSOR": r"$\phi_\times - \phi_+$", #r"$\psi^{\rm t}$",
        "PHI0VECTOR": r"$\phi_{\rm x}$",
        "PSIVECTOR": r"$\phi_{\rm y} - \phi_{\rm x}$",
        "PHI0SCALAR": r"$\phi_{\rm b}$",
        "PSISCALAR": r"$\phi_{\rm l} - \phi_{\rm b}$",
        # 21 HARMONIC
        "HPLUS_F": r"$h_+^{\rm (r)}$",
        "HCROSS_F": r"$h_\times^{\rm (r)}$",
        "HVECTORX_F": r"$h_{\rm x}^{\rm (r)}$",
        "HVECTORY_F": r"$h_{\rm y}^{\rm (r)}$",
        "HSCALARB_F": r"$h_{\rm b}^{\rm (r)}$",
        "HSCALARL_F": r"$h_{\rm l}^{\rm (r)}$",
        "PHI0TENSOR_F": r"$\phi_+^{\rm (r)}$", #r"$\phi_0^{\rm t}^{\rm (r)}$",
        "PSITENSOR_F": r"$\phi_\times^{\rm (r)} - \phi_+^{\rm (r)}$", #r"$\psi^{\rm t}^{\rm (r)}$",
        "PHI0VECTOR_F": r"$\phi_{\rm x}^{\rm (r)}$",
        "PSIVECTOR_F": r"$\phi_{\rm y}^{\rm (r)} - \phi_{\rm x}^{\rm (r)}$",
        "PHI0SCALAR_F": r"$\phi_{\rm b}^{\rm (r)}$",
        "PSISCALAR_F": r"$\phi_{\rm l}^{\rm (r)} - \phi_{\rm b}^{\rm (r)}$",
        # ORIENTATION
        "COSIOTA": r"$\cos\iota$",
        "PSI": r"$\psi$",
        # COMPOSITE
        "HTENSOR": r"$h_{\rm t}$",
        "HVECTOR": r"$h_{\rm v}$",
        "HSCALAR": r"$h_{\rm s}$",
        "HTENSOR_F": r"$h_{\rm t}^{\rm (r)}$",
        "HVECTOR_F": r"$h_{\rm v}^{\rm (r)}$",
        "HSCALAR_F": r"$h_{\rm s}^{\rm (r)}$",
    }

    def isparam(self, key):
        return key.upper().replace("_ERR", '') in self._ALL_PARAMS

    def __init__(self, key, value=0):
        """ Represents a single pulsar parameter. Internally stores value as
        float or string. RA and DEC are stored as a float in radians.
        :param key: argument name (str).
        :param value: argument value (float or str).
        """
        key = key.upper()
        if not self.isparam(key):
            raise ValueError("invalid key %r" % key)

        if key in ['RA_ERR', 'RAJ_ERR']:
            if isinstance(value, basestring):
                storevalue = hms_rad(0., 0., value)
            elif isinstance(value, float):
                storevalue = value
            else:
                raise TypeError('invalid %s value type %r' % (key, value))
        elif key in ['DEC_ERR', 'DECJ_ERR']:
            if isinstance(value, basestring):
                storevalue = dms_rad(0., 0., value)
            elif isinstance(value, float):
                storevalue = value
            else:
                raise TypeError('invalid %s value type %r' % (key, value))
        else:
            try:
                storevalue = float(value)
            except ValueError:
                if key in ['RA', 'RAJ']:
                    storevalue = hms_rad(value)
                elif key in ['DEC', 'DECJ']:
                    storevalue = dms_rad(value)
                elif isinstance(value, basestring) and key in self._STR_PARAMS:
                    storevalue = value
                else:
                    raise ValueError('invalid PAR %s value %r.' % (key, value))
        self.isnumber = key.replace("_ERR", "") in \
                        self._FLOAT_PARAMS + ["RA", "RAJ", "DEC", "DECJ"]
        amps = ['H0', 'C22', 'HPLUS', 'HCROSS', 'HVECTORX', 'HVECTORY',
                'HSCALARB', 'HSCALARL', 'HTENSOR', 'HVECTOR', 'HSCALAR']
        amps += ['%s_F' % k for k in amps]
        self.isamplitude = key in amps
        phis = ['PHI22', 'PHI0TENSOR,' 'PSITENSOR', 'PHI0VECTOR', 'PSIVECTOR',
                'PHI0SCALAR', 'PSISCALAR']
        phis += ['%s_F' % k for k in phis]
        self.isphase = key in phis
        self.range = self._DEFAULT_RANGES.get(key)
        self.label = self._LABELS.get(key, key)
        self.key = key
        self.value = storevalue

    def __str__(self):
        """
        Returns string representation of the object.
        :return: formatted string
        """
        key = self.key.upper()
        value = self.value

        if value < 0:
            sign = "-"
        else:
            sign = ""

        if not self.isparam(key):
            raise ValueError("invalid parameter %r" % key)

        if key in self._FLOAT_PARAMS or isinstance(value, basestring):
            return str(value)
        elif key in ['RA', 'RAJ', 'RA_ERR', 'RAJ_ERR']:
            sec = value / SECTORAD
            if '_ERR' in key:
                return str(sec)
            m, s = divmod(sec, 60)
            h, m = divmod(m, 60)
            h = np.mod(h, 24)
            sign = ""
            value_2 = s + m*60 + h*60*60
#             if h == 0:
#                 print value
#                 print value_2
#                 print h, m, s
            if s >= 9.9995:
                return "%s%.2d:%.2d:%.15f" % (sign, h, m, s)
            else:
                return "%s%.2d:%.2d:0%.15f" % (sign, h, m, s)
        elif key in ['DEC', 'DECJ', 'DEC_ERR', 'DECJ_ERR']:
            # taken from: lscsoft/src/lalsuite/lalapps/src/pulsar
            # /HeterodyneSearch/pulsarpputils.py: coord_to_string
            arc = np.degrees(np.fmod(np.fabs(value), np.pi))
            d = int(arc)
            arc = (arc - d) * 60.0
            m = int(arc)
            s = (arc - m) * 60.0
            if '_ERR' in key:
                return str(s)
            if s >= 9.9995:
                return "%s%.2d:%.2d:%.15f" % (sign, d, m, s)
            else:
                return "%s%.2d:%.2d:0%.15f" % (sign, d, m, s)
        elif key in self._STR_PARAMS:
            return str(value)
        else:
            raise TypeError("cannot format argument %s with value %r"
                            % (key, value))

    def __float__(self):
        """
        Returns float representation, if it exists.
        """
        if self.isnumber:
            return float(self.value)
        else:
            raise TypeError("Parameter %r lacks a float form" % self.key)


class ParameterArray(np.ndarray):
    """ Subclass of numpy.ndarry containing values of a given parameter.
    """
    _attributes = ['key']

    _gkde = None

    def __new__(cls, input_data, key=''):
        if isinstance(input_data, ParameterArray):
            return input_data
        obj = np.asarray(input_data).view(cls)
        if isinstance(key, basestring):
            try:
                parameter = Parameter(key, 0)
                if parameter.isnumber:
                    obj.key = parameter.key
                else:
                    raise TypeError("can only create arrays for numeric "
                                    "parameters.")
            except ValueError:
                warn("Key does not correspond to known parameter.")
                obj.key = key.upper()
        else:
            raise TypeError("label argument must be str not %r" % type(key))
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        # set metadata using kwargs or default value specified above:
        for attr in self._attributes:
            setattr(self, attr, getattr(obj, attr, None))

    @classmethod
    def construct(cls, sampler_name, value1, value2, size, key='', seed=None):
        """ Constructs ParameterArray by picking regularly or randomly spaced
        samples in a certain range.

        Arguments
        ---------
        sampler_name: str
            determines how the array elements are chosen, valid options being:
            'linspace'
                linearly spaced samples between the provided range;
            'logspace'
                logarthimically spaced samples between the provided range;
            'uniform'
                random samples from uniform PDF within provided range;
            'loguniform'
                random samlpes from PDF ~ 1/x withing provided range;
            'fermidirac'
                random samples from Fermi-Dirac-like PDF with provided r and s.
        value1: float
            minimum value, or r FD parameter if sampler is 'fermidirac'.
        value2: float
            maximum value, or sigma FD parameter if sampler is 'fermidirac'.
        size: integer, tuple
            number of samples to be drawn: length int(n), or shape tuple(n, m).
        key: str
            parameter name.
        seed: float, None
            random seed, applied if not None (optional, default None).

        Returns
        -------
        paramarray: ParameterArray
        """
        if seed is not None:
            random.seed(seed)
        if sampler_name == 'linspace':
            input_array = np.linspace(value1, value2, size)
        elif sampler_name == 'logspace':
            input_array = np.logspace(value1, value2, size)
        elif sampler_name == 'loguniform':
            # loguniform treated independently because it takes (a, b) bounds
            # rather than (loc, scale).
            distr = stats.loguniform(a=10**value1, b=10**value2)
            input_array = distr.rvs(size=size)
        elif sampler_name == 'fermidirac':
            # fermidirac treated independently because it takes (mu, sigma)
            # rather than (sigma, r); note that pulsar_parameter_estimation
            # takes FD parameters in the opposite to usual order, e.g.
            # `H0 fermidirac sigma r` rather than `H0 fermidirac r sigma`
            # hence `value1` is assumed to be `sigma`, while `value2` is `r`
            distr = stats.fermidirac()
            # r = mu / sigma ==> mu = sigma * r
            mu = value1 * value2
            input_array = distr.rvs(size=size, loc=mu, scale=value1)
        else:
            # attempt to load custom or builtin scipy distribution from name
            # this will fail if `sampler_name` is not found
            if sampler_name == 'uniform':
                # by default, `scipy.stats.uniform` returns values between
                # `loc` and `loc + scale`, but we want values between `value1`
                # and `value2` so shift `value2` accordingly.
                value2 -= value1
            distr = stats.distribution(sampler_name)
            input_array = distr.rvs(loc=value1, scale=value2, size=size)
        return cls(input_array, key=key)


class Model(object):
    """ Contains all information necessary to characterize a signal model.

    This includes: required parameters, injection numbers, spin properties and
    effective strength functions.

    NOTE: NEW MODELS MUST BE ADDED HERE.
    """
    # note fixed parameters are intended to be the "minimum" requirement
    _PARAMS = {
        # General Relativity
        'GR': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
               'C22', 'PHI22', 'COSIOTA', 'PSI'],
        # General Relativity (physical)
        'GRP': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                'Q22', 'DIST', 'PHI22', 'COSIOTA', 'PSI'],
        # Four-Vector Gravity
        'G4V': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                'H0', 'PHI0VECTOR', 'IOTA', 'PSI'],
        # Scalar-tensor (GR + breathing)
        'ST': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
               'H0', 'PHI0TENSOR', 'HSCALARB', 'PHI0SCALAR',
               'COSIOTA', 'PSI'],
        # Vector-tensor (GR + vector x/y)
        'VT': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
               'H0', 'PHI0TENSOR',
               'HVECTORX', 'HVECTORY', 'PHI0VECTOR', 'PSIVECTOR',
               'COSIOTA', 'PSI'],
        # Vector-tensor (GR + vector x/y)
        'SVT': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                'H0', 'PHI0TENSOR',
                'HVECTORX', 'HVECTORY', 'PHI0VECTOR', 'PSIVECTOR',
                'HSCALARB', 'PHI0SCALAR',
                'COSIOTA', 'PSI'],
        # Brans-Dicke
        'BD': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
               'H0', 'PHI0TENSOR', 'HSCALARB_F', 'PHI0SCALAR_F',
               'COSIOTA', 'PSI'],
        # Generic antenna patterns
        'AP': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
               'HPLUS', 'HCROSS', 'PHI0TENSOR', 'PSITENSOR',
               'HVECTORX', 'HVECTORY', 'PHI0VECTOR', 'PSIVECTOR',
               'HSCALARB', 'PHI0SCALAR', 'PSI'],
        # Generic antenna patterns (1F)
        'AP1F': ['RAJ', 'DECJ', 'PEPOCH', 'F0', 'PSI',
                 'HPLUS_F', 'HCROSS_F', 'PHI0TENSOR_F', 'PSITENSOR_F',
                 'HVECTORX_F', 'HVECTORY_F', 'PHI0VECTOR_F', 'PSIVECTOR_F',
                 'HSCALARB_F', 'PHI0SCALAR_F'],
        # Generic antenna patterns
        'APALL': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                  'HPLUS', 'HCROSS', 'PHI0TENSOR', 'PSITENSOR',
                  'HVECTORX', 'HVECTORY', 'PHI0VECTOR', 'PSIVECTOR',
                  'HSCALARB', 'PHI0SCALAR', 'PSI',
                  # First harmonic of rotation
                  'HPLUS_F', 'HCROSS_F', 'PHI0TENSOR_F', 'PSITENSOR_F',
                  'HVECTORX_F', 'HVECTORY_F', 'PHI0VECTOR_F', 'PSIVECTOR_F',
                  'HSCALARB_F', 'PHI0SCALAR_F'],
    }

    # quantities on which to place upper limits
    _UPPER_LIMITS = {
        # General Relativity
        'GR': ['HTENSOR'],#['H0'],
        # General Relativity (physical)
        'GRP': ['Q22'],
        # Four-Vector Gravity
        'G4V': ['H0'],
        # Scalar-tensor (GR + breathing)
        'ST': ['H0', 'HSCALAR'],
        # Vector-tensor (GR + vector x/y)
        'VT': ['H0', 'HVECTOR'],
        # Vector-tensor (GR + vector x/y)
        'SVT': ['H0', 'HSCALAR', 'HVECTOR'],
        # Brans-Dicke (GR triaxial + 1F breathing)
        'BD': ['H0', 'HSCALAR_F'],
        # Generic antenna patterns
        'AP': ['HSCALAR', 'HVECTOR', 'HTENSOR'],
        'AP1F': ['HSCALAR_F', 'HVECTOR_F', 'HTENSOR_F'],
        'APALL': ['HSCALAR', 'HVECTOR', 'HTENSOR',
                  'HSCALAR_F', 'HVECTOR_F', 'HTENSOR_F'],
        # Helicities
        'scalar': ['HSCALAR'],
        'vector': ['HVECTOR'],
        'tensor': ['HTENSOR'],
        'scalar1f': ['HSCALAR_F'],
        'vector1f': ['HVECTOR_F'],
        'tensor1f': ['HTENSOR_F']
    }

    # keys to be appended to --nonGR in command line options; if '', --nonGR
    # used as flag; if None, --nonGR option is omitted altogether.
    _PPEKEYS = {
        'GR': None,
        'GRP': None,
        'G4V': 'G4V',
        'ST': 'EGR',
        'VT': 'EGR',
        'SVT': 'EGR',
        'BD': 'EGR',
        'AP': '',
        'AP1F': '',
        'APALL': '',
    }

    _SPINS_ALL = {
        'tensor': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                   'HPLUS', 'HCROSS', 'PHI0TENSOR', 'PSITENSOR', 'PSI'],
        'vector': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                   'HVECTORX', 'HVECTORY', 'PHI0VECTOR', 'PSIVECTOR', 'PSI'],
        'scalar': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                   'HSCALARB', 'PHI0SCALAR', 'PSI'],
        'tensor1f': ['RAJ', 'DECJ', 'PEPOCH', 'F0', 'HPLUS_F', 'HCROSS_F',
                     'PHI0TENSOR_F', 'PSITENSOR_F', 'PSI'],
        'vector1f': ['RAJ', 'DECJ', 'PEPOCH', 'F0', 'HVECTORX_F', 'HVECTORY_F',
                     'PHI0VECTOR_F', 'PSIVECTOR_F', 'PSI'],
        'scalar1f': ['RAJ', 'DECJ', 'PEPOCH', 'F0',
                     'HSCALARB_F', 'PHI0SCALAR_F', 'PSI'],
    }

    _SPINS = {k: v for k,v in _SPINS_ALL.iteritems() if '1f' not in k}

    _SPINS_1F = {k: v for k,v in _SPINS_ALL.iteritems() if '1f' in k}

    # Injection number options to be used by InjectionParArray.from_model() to
    # generate injection files. `_INJECT_NUMBERS` contains the
    # injection numbers associated to each amplitude parameter for a given
    # model, e.g. in GR, `n` corresponds to number of different values of `C22`
    # to be injected; in `ST`, `n0` corresponds to `H0` and `nb` to `HSCALARB`.
    _INJECT_NUMBERS = {
        'GR': {'C22': 'n'},
        'GRP': {'Q22': 'n'},
        'G4V': {'H0': 'n'},
        'ST': {'H0': 'n0', 'HSCALARB': 'nb'},
        'VT': {'H0': 'n0', 'HVECTORX': 'nx', 'HVECTORY': 'ny'},
        'SVT': {'H0': 'n0', 'HSCALARB': 'nb', 'HVECTORX': 'nx',
                'HVECTORY': 'ny'},
        'BD': {'H0': 'n0', 'HSCALARB_F': 'nb'},
        'AP': {'HPLUS': 'np', 'HCROSS': 'nc', 'HSCALARB': 'nb',
               'HVECTORX': 'nx', 'HVECTORY': 'ny'},
        'AP1F': {'HPLUS_F': 'np1f', 'HCROSS_F': 'nc1f', 'HSCALARB_F': 'nb1f',
                 'HVECTORX_F': 'nx1f', 'HVECTORY_F': 'ny1f'},
        'APALL': {'HPLUS': 'np', 'HCROSS': 'nc', 'HSCALARB': 'nb',
                  'HVECTORX': 'nx', 'HVECTORY': 'ny', 'HPLUS_F': 'np1f',
                  'HCROSS_F': 'nc1f', 'HSCALARB_F': 'nb1f',
                  'HVECTORX_F': 'nx1f', 'HVECTORY_F': 'ny1f'},
    }

    _H_EFFECTIVE = {
        'GR': lambda c22, cosi: c22 * np.sqrt(0.25*(1. + cosi**2)**2 + cosi**2),
        'GRP': lambda q22, dist, f0, cosi:
        0.5 * (q22 * np.sqrt(8.*np.pi/15.)*16.*np.pi**2*G*f0**2/(C**4*dist)) \
        * np.sqrt(0.25*(1. + cosi**2)**2 + cosi**2),
        'G4V': lambda h0, i: 0.5 * h0 * np.sqrt(np.sin(i)**2 +\
                                                (np.sin(i)*np.cos(i))**2),
        'ST': lambda h0, cosi, hb: 0.5 * h0 * np.sqrt(0.25*(1.+cosi**2)**2 +\
                                                      cosi**2 + hb**2),
        'VT': lambda h0, cosi, hx, hy: 0.5*h0 * np.sqrt(0.25*(1.+cosi**2)**2+\
                                                        cosi**2+hx**2+hy**2),
        'SVT': lambda h0, cosi, hb, hx, hy: 0.5 * h0 *\
                                            np.sqrt(0.25*(1.+cosi**2)**2 +\
                                                    cosi**2 + hb**2 + hx**2+\
                                                    hy**2),
        'BD': lambda h0, cosi, hb: 0.5 * h0 * np.sqrt(0.25*(1.+cosi**2)**2 +\
                                                      cosi**2 + hb**2),
        'AP': lambda *args: np.sqrt(np.sum(np.array(args)**2)),
        'AP1F': lambda *args: np.sqrt(np.sum(np.array(args)**2)),
        'APALL': lambda *args: np.sqrt(np.sum(np.array(args)**2)),
    }

    # Arguments needed to compute the affective strength with `_H_EFFECTIVE()`
    _H_EFFECTIVE_ARGS = {
        'GR': ['C22', 'COSIOTA'],
        'GRP': ['Q22', 'DIST', 'F0', 'COSIOTA'],
        'G4V': ['H0', 'IOTA'],
        'ST': ['H0', 'COSIOTA', 'HSCALARB'],
        'VT': ['H0', 'COSIOTA', 'HVECTORX', 'HVECTORY'],
        'SVT': ['H0', 'COSIOTA', 'HSCALARB', 'HVECTORX', 'HVECTORY'],
        'BD': ['H0', 'COSIOTA', 'HSCALARB_F'],
        'AP': ['HPLUS', 'HCROSS', 'HVECTORX', 'HVECTORY', 'HSCALARB'],
        'AP1F': ['HPLUS_F', 'HCROSS_F', 'HVECTORX_F', 'HVECTORY_F',
                 'HSCALARB_F'],
        'APALL': ['HPLUS', 'HCROSS', 'HVECTORX', 'HVECTORY', 'HSCALARB',
                  'HPLUS_F', 'HCROSS_F', 'HVECTORX_F', 'HVECTORY_F',
                  'HSCALARB_F'],
    }

    _MODELS = _PARAMS.keys()

    def __init__(self, model_key):
        self.SPIN_COMB_KEYS = self._spin_comb_keys()
        self.SPIN_COMB_KEYS_1F = self._spin_comb_keys(harmonics=[1])
        self.SPIN_COMB_KEYS_ALL = self._spin_comb_keys(harmonics=[1,2])
        self.harmonics = []
        if self.ismodel(model_key):
            self.key = model_key.upper()
            self.ppekey = self._PPEKEYS[self.key]
            self.params = self._PARAMS[self.key]
            self.inj_numbers_dict = self._INJECT_NUMBERS[self.key]
            self.inj_numbers = self.inj_numbers_dict.values()
            self.amplitudes = self.inj_numbers_dict.keys()
            self.heff = self._H_EFFECTIVE[self.key]
            self.heff_args = self._H_EFFECTIVE_ARGS[self.key]
            self.uls = self._UPPER_LIMITS[self.key]
        elif model_key is None or model_key == '':
            self.key = ''
            self.ppekey = None
            self.params = None
            self.inj_numbers_dict = None
            self.inj_numbers = None
            self.amplitudes = None
            self.heff = None
            self.heff_args = None
            self.uls = []
        elif model_key.lower() in ['n', 'noise', 's', 'signal', 'ngr', 'none',
                                   'nongr', 'non-gr']:
            if model_key.lower() in ['n', 'noise', 'none']:
                self.key = 'noise'
            elif model_key.lower() in ['s', 'signal']:
                self.key = 'signal'
            elif model_key.lower() in ['ngr', 'nongr', 'non-gr']:
                self.key = 'nongr'
            self.ppekey = None
            self.params = None
            self.inj_numbers_dict = None
            self.inj_numbers = None
            self.amplitudes = None
            self.heff = None
            self.heff_args = None
            self.uls = []
        elif any([x in model_key for x in self._SPINS_ALL]):
            # assume `model_key` is of the form 'tensor_vector'
            # determine parameters corresponding to spins listed
            spins = model_key.split('_')
            if all([s in self._SPINS_ALL for s in spins]):
                params = []
                for spin in spins:
                    for param in self._SPINS_ALL[spin]:
                        params.append(param)
                self.params = list(set(params))
                self.key = model_key.lower()
                self.ppekey = ''
                self.inj_numbers_dict = {amp: n for amp, n in
                                         self._INJECT_NUMBERS['APALL'].iteritems()
                                         if amp in self.params}
                self.amplitudes = self.inj_numbers_dict.keys()
                self.inj_numbers = self.inj_numbers_dict.values()
                self.heff = self._H_EFFECTIVE['AP']
                self.heff_args = [p for p in self.params
                                  if Parameter(p, 0).isamplitude]
                self.uls = [self._UPPER_LIMITS[s.lower()][0] for s in spins]
            else:
                raise ValueError("invalid model key %r" % model_key)
        else:
            raise ValueError("invalid model key %r" % model_key)
        self.label = self._display_index()
        # set rotational frequency harmonics
        harmonics = []
        for a in self.amplitudes or []:
            if '_F' in a:
                harmonics.append(1)
            else:
                harmonics.append(2)
        self.harmonics = list(set(harmonics))

    def _display_index(self):
        """Returns model key for display in plots."""
        model_key = self.key
        if model_key in ['ST', 'VT', 'SVT']:
            model_index = "GR+%s" % model_key.lower().strip('t')
        elif any([x in model_key for x in self._SPINS_ALL]):
            model_index = ''
            for s in model_key.split('_'):
                model_index += s[0]
                if '1f' in s:
                    model_index += '1'
        elif model_key in ['n', 'noise', 's', 'signal']:
            model_index = model_key[0].upper()
        elif  model_key in ['ngr', 'nongr', 'non-gr']:
            model_index = 'nGR'
        else:
            model_index = model_key
        return model_index

    def ismodel(self, key):
        if isinstance(key, basestring):
            if key.upper() in self._MODELS:
                return True
            else:
                return False
        else:
            return False

    def total_injection_number(self, inject_numbers):
        """Determines the total number of injections from the model-specific
         injection options. For examlpe, for GR will return N = n, while for
         ST it will return N = n0 * nB

        :param inject_numbers: dictionary with injection options (dict)
        :return: total number of injections (int)
        """
        if self.key in self._MODELS and all([n_name in inject_numbers for\
                                             n_name in self.inj_numbers]): 
            ntot = 1
            for n_name in self.inj_numbers:
                ntot = ntot * inject_numbers[n_name]
        elif 'n' in inject_numbers:
            ntot = inject_numbers['n']
        else:
            raise NotImplementedError("no injections opts for %r" % self.key)
        return int(ntot)

    def _spin_combs(self, harmonics=[2]):
        """Returns a list of tuples representing all valid combinations of spin

        :returns spin_comb: list of tuples.
        """
        spins = []
        if 2 in harmonics:
            spins += self._SPINS.keys()
        if 1 in harmonics:
            spins += self._SPINS_1F.keys()
        spin_comb = []
        for r in range(1, len(spins) + 1):
            for comb in itertools.combinations(spins, r):
                spin_comb.append(comb)
        return spin_comb

    def _spin_comb_keys(self, **kwargs):
        """Produces a list of strings with spin combination names.
        spin_comb_keys = ['scalar, 'tensor', 'vector', 'scalar_tensor', ... ]

        :returns spin_comb_keys: list of spin combination keys (list of strs)
        """
        spin_combs = self._spin_combs(**kwargs)
        spin_comb_keys = []
        for sc in spin_combs:
            sc_key = ''
            for s in sc:
                sc_key += "_%s" % s
            spin_comb_keys.append(sc_key.strip("_"))
        return spin_comb_keys

    def _nongr_keys(self):
        spin_comb_keys = self._spin_comb_keys(harmonics=[1,2])
        nongr_keys = ['ST', 'VT', 'SVT'] + spin_comb_keys
        return nongr_keys


class ModelList(list):
    """ Subclass of list designed to contain multiple Model objects.
    """
    def __init__(self, input_data):
        # check contents are of right type
        new_data = []
        for element in input_data:
            if isinstance(element, Model):
                new_data.append(element)
            else:
                # assume `element` is a model key
                new_data.append(Model(element))
        super(ModelList, self).__init__(new_data)
        self.keys = [m.key for m in self]
        self.labels = [m.label for m in self]
        self.harmonics = list(set([h for m in self for h in m.harmonics]))

    def sorted_nongr_keys(self):
        nongr_keys = ['ST', 'VT', 'SVT', 'scalar', 'vector', 'tensor',
                      'scalar_vector', 'scalar_tensor', 'vector_tensor',
                      'scalar_vector_tensor']
        extra = [k for k in Model('AP').SPIN_COMB_KEYS_ALL
                 if k not in nongr_keys]
        sorted_nongr_keys = nongr_keys + extra
        return [k for k in sorted_nongr_keys if k in self.keys]

    def filter_keys(self, no_gr=False, no_tensor=False, exclude=()):
        """Return search model keys, excluding GR keys (GR, GR+s, GR+v, GR+sv),
        tensor keys (t, ts, tv, tsv), or specific keys contained in `exclude`.
        """
        sks = self.keys
        if no_gr:
            sks = list(set(sks) - {'ST', 'VT', 'SVT', 'GR'})
        if no_tensor:
            sks = [k for k in sks if 'tensor' not in k]
        sks = list(set(sks) - set(exclude))
        return ModelList(sks)

    def get_gr_key(self):
        for grkey in ['GR', 'tensor', 'tensor1f']:
            if grkey in self.keys:
                return grkey
        # if no GR key in list, return empy string
        # TODO: what if both 'tensor' and 'tensor1f' present?
        return ''


class Run(object):

    _IFOS = {
        'S5': ['H1', 'H2', 'L1'],
        'S6': ['H1', 'L1'],
        'O1': ['H1', 'L1'],
        'O2': ['H1', 'L1', 'V1'],
        'O1O2': ['H1', 'L1'],
        'VSR24' : ['V1'],
    }

    # EPOCH = (start GPS time, end GPS time) from finehet
    _EPOCHS = {
        'S5': {
            'H1': (815403041.000000, 875231967.000000),
            'H2': (815201064.000000, 875231983.000000),
            'L1': (816062389.000000, 875231983.000000)
        },
        'S6': {
            'H1': (931052820.000000, 971622007.000000),
            'L1': (931071886.000000, 971614842.000000)
        },
        'VSR24': {
            'V1': (931035427.000000, 999233969.000000),  # VSR2 + VSR4
        },
        'O1': {
            'H1': (1125972384.000000, 1136649575.000000),
            'L1': (1126072274.000000, 1136649566.000000)
        },
        'O2': {
            'H1': (1164556817.000000, 1187733618.000000),
            'L1': (1164556817.000000, 1187733618.000000),
            # TODO: double check V1 start time
            #'V1': (1185616128.000000, 1187733618.000000),
        },
        'O1O2': {
            'H1': (1125972384.000000, 1187733618.000000),
            'L1': (1125972384.000000, 1187733618.000000),
            # TODO: double check V1 start time
            #'V1': (1185616128.000000, 1187733618.000000),
        },
    }

    # effective number of days in seconds
    _DURATIONS = {
        # from P1200104-v4 code
        'S5': {
            'H1': 527.*SS,
            'H2': 535.*SS,
            'L1': 405.*SS
        },
        'S6': {
            'H1': 238.*SS,
            'L1': 225.*SS
        },
        'VSR24': {
            'V1': 218.*SS,  # VSR2 + VSR4
        },
        # from number of samples in finehet data for J1932+17
        'O1':{
            'H1': 91792.*60,
            'L1': 107546.*60
        },
        # from number of samples in 1f finehet data for J0605+37
        'O2':{
            'H1': 225422.*60,
            'L1': 220090.*60
        },
        'O1O2':{
            'H1': 338072.*60,
            'L1': 314207.*60
        },
    }

    def __init__(self, run):
        if isinstance(run, basestring) and run.upper() in RUNS:
            self.key = run.upper()
            self.ifos = self._IFOS[self.key]
            self.epoch = self._EPOCHS[self.key]
            self.duration = self._DURATIONS[self.key]
        else:
            raise AttributeError("Unknown run %r." % run)


NONGR_KEYS = Model(None)._nongr_keys()


class Cluster(object):
    def __init__(self, name=''):
        """Contains LDAS cluster info (scratch and public directories).

        :param name: optional cluster hostname (str).
        """

        user = os.getenv('USER')
        # Set identity
        if name == '':
            # get hostname to determine what server we are on
            self.hostname = socket.gethostname()
        else:
            self.hostname = name
        # Determine scratch and public directories
        if 'ldas' in self.hostname:
            self.scratch_dir = '/usr1/%s/' % user
            self.public_dir = '/home/%s/public_html/' % user
        elif 'atlas' in self.hostname:
            # get result of hostname -s command in bash
            hs = self.hostname.split('.')[0]
            self.scratch_dir = '/atlas/user/%s/%s/' % (hs, user)
            self.public_dir = '/home/%s/WWW/LSC/' % user
        elif self.hostname in ['pcdev1', 'pcdev2', 'hydra', 'trout']:
            # assuming Nemo cluster
            self.scratch_dir = '/home/%s/scratch/' % user
            self.public_dir = '/home/%s/public_html/' % user
        else:
            self.scratch_dir = ''
            self.public_dir = ''


class Spectrum(np.ndarray):
    _attributes = ['freq', 'run', 'ifo', 'duration', '_interp1d', 'kind', '_smooth']

    def __new__(cls, input_data, freq=None, run=None, ifo=None, duration=None,
                epoch=None, kind=None):
        if isinstance(input_data, Spectrum):
            if run is None:
                run = input_data.run
            if ifo is None:
                ifo = input_data.ifo
            if kind is None:
                kind = input_data.kind
        obj = np.asarray(input_data).view(cls)
        if kind is None:
            obj.kind = kind
        elif isinstance(kind, basestring):
            kind = kind.lower()
            if kind in ['asd', 'psd']:
                obj.kind = kind
            else:
                raise ValueError("Invalid spectrum type: %r" % kind)
        else:
            raise ValueError("Invalid spectrum type: %r" % kind)
        if freq is not None:
            farray = np.array(freq)
            if len(farray) == len(obj):
                obj.freq = farray
                obj._interp1d = interp1d(farray, obj, kind="linear")
                spec_smooth, freq_smooth = reject_outliers(obj, x=farray)
                obj._smooth = interp1d(freq_smooth, spec_smooth, kind="linear")
            else:
                raise ValueError("Spectrum and frequency do not have same "
                                 "length.")
        else:
            obj.freq = np.arange(len(obj))
            obj._interp1d = None
            obj._smooth = None
        if isinstance(ifo, basestring):
            obj.ifo = ifo.upper()
        if isinstance(run, basestring):
            run_obj = Run(run)
            obj.run = run_obj.key
            if obj.run in RUNS and ifo in run_obj.ifos:
                if duration is None:
                    obj.duration = run_obj.duration[ifo]
                else:
                    obj.duration = float(duration)
                    if run_obj.duration[ifo] <= obj.duration:
                        warn("Specified duration longer than %s run."
                             % obj.run)
                if epoch is None:
                    obj.epoch = run_obj.epoch[ifo]
                else:
                    try:
                        obj.epoch = (float(epoch), np.inf)
                        if not (run_obj.epoch[ifo][0] <= float(epoch) <=
                                run_obj.epoch[ifo][0]):
                            warn("Specified epoch outside %s run range."
                                 % obj.run)
                    except TypeError:
                        obj.epoch = (float(epoch[0]), float(epoch[1]))
        else:
            obj.run = run
            obj.duration = duration
            obj.epoch = epoch
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        # set metadata using kwargs or default value specified above:
        for attr in self._attributes:
            setattr(self, attr, getattr(obj, attr, None))

    @classmethod
    def load(cls, path, run=None, ifo=None, duration=None, epoch=None,
             kind=None):
        """ Import ASDfrom :path:. The assumed format is two columns, first
        frequency, second real valued ASD.

        :param path: path or paths to ASD file.
        """
        if isinstance(path, basestring):
            if os.path.exists(path):
                origin = os.path.abspath(path)
                try:
                    data = np.loadtxt(origin, comments='%')
                except ValueError:
                    data = np.loadtxt(origin, comments='#')
                freq = data[:, 0]
                ads = data[:, 1]
            else:
                raise IOError("ASD file does not exist: %r" % path)
        else:
            raise TypeError("Path must be string, not %r." % type(path))
        return cls(ads, freq=freq, run=run, ifo=ifo, duration=duration,
                   epoch=epoch, kind=kind)

    def __call__(self, f):
        if self._interp1d is not None:
            return Spectrum(self._interp1d(f), freq=f, kind=self.kind)
        else:
            raise AttributeError("Cannot interpolate without frequency vector")

    @classmethod
    def from_lal(cls, key, freq=np.logspace(-1, 4, 1e4), kind='asd', **kwargs):
        advanced = kwargs.pop('advanced', True)
        if key in ['H1', 'L1', 'V1']:
            if advanced:
                if key == 'V1':
                    key = "SimNoisePSDAdvVirgo"
                else:
                    key = "SimNoisePSDaLIGOZeroDetHighPower"
            else:
                raise NotImplementedError("Initial-era PSDs not implemented. "
                                          "Must call directly.")
        try:
            exec("from lalsimulation import %s" % key)
            psd = locals()[key]
        except ImportError:
            raise ValueError("Invalid PSD key: %r" % key)
        if kind == 'asd':
            spec_data = [np.sqrt(psd(f)) for f in freq]
        elif kind == 'psd':
            spec_data = [psd(f) for f in freq]
        else:
            raise ValueError("Unrecognized spectrum kind: %r" % kind)
        return cls(spec_data, freq=freq, kind=kind, **kwargs)

    def smooth(self, f):
        return Spectrum(self._smooth(f), freq=f, kind=self.kind)

    def psd(self):
        if self.kind == 'asd':
            spec = Spectrum(self**2, freq=self.freq, kind='psd')
        elif self.kind == 'psd':
            spec = self
        else:
            raise ValueError("Must specify spctrum kind: asd or psd?")
        return spec

    def asd(self):
        if self.kind == 'asd':
            spec = self
        elif self.kind == 'psd':
            spec = Spectrum(np.sqrt(self), freq=self.freq, kind='asd')
        else:
            raise ValueError("Must specify spctrum kind: asd or psd?")
        return spec

    def fit(self, x, y, err=None, intercept=False):
        """Fit to spectrum"""
        spec_data = self(x)
        scale_factor = max(np.abs(spec_data))
        scale_factor_y = max(np.abs(y))
        y_scaled = y / scale_factor_y
        spec_scaled = spec_data / scale_factor
        err = err if err is not None else np.ones(len(x))
        err /= max(err)
        if intercept:
            chi = lambda arg: np.sum(
                ((y_scaled - arg[0] * spec_scaled - arg[1]) / err) ** 2)
        else:
            chi = lambda arg: np.sum(
                ((y_scaled - arg[0] * spec_scaled) / err) ** 2)
        alpha_guess = np.average(y_scaled / spec_scaled)
        beta_guess = np.average(np.abs(y_scaled - spec_scaled))
        # cons = ({'type': 'ineq', 'fun': lambda x:  x[0]})
        fit_result = minimize(chi, [alpha_guess, beta_guess])#,  method='SLSQP',
        #                      constraints=cons)
        prop_factor = fit_result.x[0] * scale_factor_y / scale_factor
        if intercept:
            offset = fit_result.x[1] * scale_factor_y
        else:
            offset = 0
        print "Spectrum fit success: %s" % str(fit_result.success)
        return prop_factor, offset


class SpectrumList(object):
    def __init__(self, spectra, kind=None):
        """Series of spectrum objects.

        :param spectra (dict)
        """
        kinds = []
        if isinstance(spectra, Spectrum):
            self.spectra = [spectra]
        else:
            for spectrum in spectra:
                if not isinstance(spectrum, Spectrum):
                    raise TypeError("Can only store ASD objects.")
                else:
                    kinds.append(spectrum.kind)
            kinds = list(set(kinds))
            self.spectra = list(spectra)
        if kind is None:
            if len(kinds) == 1:
                self.kind = kinds[0]
            else:
                self.kind = None
        elif isinstance(kind, basestring):
            kind = kind.lower()
            if kind in ['asd', 'psd']:
                self.kind = kind
            else:
                raise ValueError("Invalid spectrum type: %r" % kind)
        else:
            raise ValueError("Invalid spectrum type: %r" % kind)

    def __getitem__(self, item):
        return self.spectra[item]

    @classmethod
    def load(cls, paths, runs=None, ifos=None, durations=None, epochs=None,
             kind=None):
        """Load set of ASD objects from disk.
        """
        # TODO: implement epochs
        if epochs is not None:
            raise NotImplementedError("cannot accept epochs argument, fix me!")
        if isinstance(paths, basestring):
            # assume paths is comma-separated string
            path_list = [os.path.abspath(dur) for dur in paths.split(',')]
        else:
            # assume paths is list
            path_list = [os.path.abspath(dur) for dur in paths]
        if runs is None:
            run_list = [None] * len(path_list)
        elif isinstance(runs, basestring):
            # assume runs is comma-separated string.
            run_list = runs.split(',')
        else:
            # assume runs is list-like.
            run_list = list(runs)
        if ifos is None:
            ifo_list = [None] * len(path_list)
        elif isinstance(ifos, basestring):
            # assume ifos is comma-separated string.
            ifo_list = ifos.split(',')
        else:
            # assume ifos is list-like.
            ifo_list = list(ifos)
        if len(run_list) == 1:
            run_list = run_list * len(ifo_list)
        elif len(run_list) != len(ifo_list):
            raise ValueError("Number of runs (%i) must be one or "
                             "equal to number of ifos (%i)." %
                             (len(run_list), len(ifo_list)))
        if durations is None:
            duration_list = [None] * len(path_list)
        elif isinstance(durations, basestring):
            duration_list = []
            for dur in durations.split(','):
                try:
                    duration_list.append(float(dur))
                except ValueError:
                    if run_list[0] is None:
                        # assume p is of the format 'run_ifo'
                        run, ifo = dur.split('_')
                        runobj = Run(run)
                        duration_list.append(runobj.duration[ifo.upper()])
                    else:
                        raise ValueError("An element of the durations "
                                         "argument, is a string but the run "
                                         "has already been specified via "
                                         "keyword argument. Offending element:"
                                         "%r." % dur)
        else:
            try:
                duration_list = [float(durations)]
            except TypeError:
                # assume durations is list of numbers
                duration_list = [float(dur) for dur in durations]
        # check all arrays have same length
        if len(duration_list) != len(path_list):
            raise ValueError("Must have same number of durations as paths.")
        elif len(path_list) != len(run_list):
            raise ValueError("Must have same number of runs and ifos as paths.")
        # load spectra
        specs = []
        for (path, run, ifo, duration) in zip(path_list, run_list, ifo_list
                , duration_list):
            specs.append(Spectrum.load(path, run=run, ifo=ifo, duration=duration, kind=kind))
        return cls(specs, kind=kind)

    @classmethod
    def from_lal(cls, keys, **kwargs):
        if isinstance(keys, basestring):
            key_list = keys.split(',')
        else:
            # assume keys is list
            key_list = keys
        spectra = []
        for key in keys:
            spectra.append(Spectrum.from_lal(key, **kwargs))
        return cls(spectra)

    def __call__(self, f):
        # interpolate each spectrum
        return np.array([spec(f) for spec in self.spectra])

    def time_weighted(self):
        # return a ASDs divided by their duration
        # fails if duration is None
        return SpectrumList([spec / spec.duration for spec in self.spectra])

    def power(self, power):
        return SpectrumList([spec ** power for spec in self.spectra])

    def h95ul(self, f, ispsd=False):
        """ Computes 95% confidence upper limit estimate on h based on the
        spectral density at a given frequency.  Based on P1200104-v4 code.

        Note that by default assumes Spectrum is ASD.
        """
        if ispsd:
            psds_tw = [spec(f) / spec.duration for spec in self.spectra]
        else:
            psds_tw = [spec(f) ** 2 / spec.duration for spec in self.spectra]
        psd_hmean = 1./np.sum(1./np.array(psds_tw))
        h95 = H95_BAYES_SCALE * np.sqrt(psd_hmean)
        return h95

    def hmean_asd(self):
        """ output the ASD of the harmonic mean of the input spectra (where the
        input is a list of numpy arrays of time weighted power spectra)"""
        spectra = [spec.psd() for spec in self.spectra]
        for i, v in enumerate(spectra):
            if i == 0:
                mspec = np.divide(1., v)
            else:
                mspec = np.add(mspec, np.divide(1., v))
        spec_data = np.divide(1., mspec)
        psd = Spectrum(np.array(spec_data), freq=spec_data.freq, kind='psd')
        return psd.asd()

    def hmean_psd(self):
        """ output the ASD of the harmonic mean of the input spectra (where the
        input is a list of numpy arrays of time weighted power spectra)"""
        spectra = [spec.psd() for spec in self.spectra]
        for i, v in enumerate(spectra):
            if i == 0:
                mspec = np.divide(1., v)
            else:
                mspec = np.add(mspec, np.divide(1., v))
        spec_data = np.divide(1., mspec)
        return spec_data.psd()


###############################################################################
# FUNCTIONS

def ismodel(key):
    try:
        Model(key)
        return True
    except ValueError:
        return False


def isnumber(*args):
    """Checks whether keys correspond to number arguments.
    """
    arenums = []
    for k in args:
        arenums.append(Parameter(k, 0).isnumber)
    return all(arenums)


def reject_outliers(data, m = 2., x=None):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/mdev if mdev else 0.
    if x is not None:
        x = np.array(x)
        return data[s<m], x[s<m]
    else:
        return data[s<m]


# TODO: incorporate into `Parameter`
def translate_parameter(key, origin=None):
    """Return the basic parameters and function used to compute a parameter
    from others, e.g. HGR as a function of COSI and H0
    """
    key = key.upper()
    origin = origin or []
    suf = '_F' if '_F' in key else ''
    if 'HTENSOR' in key:
        args = ['HPLUS%s' % suf, 'HCROSS%s' % suf]
        func = lambda hp, hc: np.sqrt(hp ** 2 + hc ** 2)
    elif 'HVECTOR' in key:
        args = ['HVECTORX%s' % suf, 'HVECTORY%s' % suf]
        func = lambda hx, hy: np.sqrt(hx ** 2 + hy ** 2)
    elif 'HSCALAR' in key:
        args = ['HSCALARB%s' % suf]
        func = lambda hb: hb
    elif key == 'H0':
        args = ['C22']
        func = lambda c22: 2. * c22
    elif key == 'HGR':
        args = list({'C22', 'H0'} & set(origin))
        if len(args) > 1:
            args = [args[0]]
        elif len(args) < 1:
            raise ValueError("Cannot produce %s from given parameters: %r" %
                             (key, origin))
        args.append('COSIOTA')
        gr = Model('GR')
        if args[0] == 'C22':
            func = gr.heff
        else:
            func = lambda h0, cosi: gr.heff(h0/2.0, cosi)
    else:
        raise ValueError("Cannot translate parameter %r" % key)
    return args, func


##############################################################################
# NUMPY COMPATIBILITY

# copied from Numpy 1.9 to allow for N-dimensional matrices
# this is so the code can run on systems with older installations of numpy
def meshgrid(*xi, **kwargs):
    """
    Return coordinate matrices from coordinate vectors.

    Make N-D coordinate arrays for vectorized evaluations of
    N-D scalar/vector fields over N-D grids, given
    one-dimensional coordinate arrays x1, x2,..., xn.

    .. versionchanged:: 1.9
       1-D and 0-D cases are allowed.

    Parameters
    ----------
    x1, x2,..., xn : array_like
        1-D arrays representing the coordinates of a grid.
    indexing : {'xy', 'ij'}, optional
        Cartesian ('xy', default) or matrix ('ij') indexing of output.
        See Notes for more details.

        .. versionadded:: 1.7.0
    sparse : bool, optional
        If True a sparse grid is returned in order to conserve memory.
        Default is False.

        .. versionadded:: 1.7.0
    copy : bool, optional
        If False, a view into the original arrays are returned in order to
        conserve memory.  Default is True.  Please note that
        ``sparse=False, copy=False`` will likely return non-contiguous
        arrays.  Furthermore, more than one element of a broadcast array
        may refer to a single memory location.  If you need to write to the
        arrays, make copies first.

        .. versionadded:: 1.7.0

    Returns
    -------
    X1, X2,..., XN : ndarray
        For vectors `x1`, `x2`,..., 'xn' with lengths ``Ni=len(xi)`` ,
        return ``(N1, N2, N3,...Nn)`` shaped arrays if indexing='ij'
        or ``(N2, N1, N3,...Nn)`` shaped arrays if indexing='xy'
        with the elements of `xi` repeated to fill the matrix along
        the first dimension for `x1`, the second for `x2` and so on.

    Notes
    -----
    This function supports both indexing conventions through the indexing
    keyword argument.  Giving the string 'ij' returns a meshgrid with
    matrix indexing, while 'xy' returns a meshgrid with Cartesian indexing.
    In the 2-D case with inputs of length M and N, the outputs are of shape
    (N, M) for 'xy' indexing and (M, N) for 'ij' indexing.  In the 3-D case
    with inputs of length M, N and P, outputs are of shape (N, M, P) for
    'xy' indexing and (M, N, P) for 'ij' indexing.  The difference is
    illustrated by the following code snippet::

        xv, yv = meshgrid(x, y, sparse=False, indexing='ij')
        for i in range(nx):
            for j in range(ny):
                # treat xv[i,j], yv[i,j]

        xv, yv = meshgrid(x, y, sparse=False, indexing='xy')
        for i in range(nx):
            for j in range(ny):
                # treat xv[j,i], yv[j,i]

    In the 1-D and 0-D case, the indexing and sparse keywords have no effect.

    See Also
    --------
    index_tricks.mgrid : Construct a multi-dimensional "meshgrid"
                     using indexing notation.
    index_tricks.ogrid : Construct an open multi-dimensional "meshgrid"
                     using indexing notation.

    Examples
    --------
    >>> nx, ny = (3, 2)
    >>> x = np.linspace(0, 1, nx)
    >>> y = np.linspace(0, 1, ny)
    >>> xv, yv = meshgrid(x, y)
    >>> xv
    array([[ 0. ,  0.5,  1. ],
           [ 0. ,  0.5,  1. ]])
    >>> yv
    array([[ 0.,  0.,  0.],
           [ 1.,  1.,  1.]])
    >>> xv, yv = meshgrid(x, y, sparse=True)  # make sparse output arrays
    >>> xv
    array([[ 0. ,  0.5,  1. ]])
    >>> yv
    array([[ 0.],
           [ 1.]])

    `meshgrid` is very useful to evaluate functions on a grid.

    >>> x = np.arange(-5, 5, 0.1)
    >>> y = np.arange(-5, 5, 0.1)
    >>> xx, yy = meshgrid(x, y, sparse=True)
    >>> z = np.sin(xx**2 + yy**2) / (xx**2 + yy**2)
    >>> h = plt.contourf(x,y,z)

    """
    ndim = len(xi)

    copy_ = kwargs.pop('copy', True)
    sparse = kwargs.pop('sparse', False)
    indexing = kwargs.pop('indexing', 'xy')

    if kwargs:
        raise TypeError("meshgrid() got an unexpected keyword argument '%s'"
                        % (list(kwargs)[0],))

    if indexing not in ['xy', 'ij']:
        raise ValueError(
            "Valid values for `indexing` are 'xy' and 'ij'.")

    s0 = (1,) * ndim
    output = [np.asanyarray(x).reshape(s0[:i] + (-1,) + s0[i + 1::])
              for i, x in enumerate(xi)]

    shape = [x.size for x in output]

    if indexing == 'xy' and ndim > 1:
        # switch first and second axis
        output[0].shape = (1, -1) + (1,)*(ndim - 2)
        output[1].shape = (-1, 1) + (1,)*(ndim - 2)
        shape[0], shape[1] = shape[1], shape[0]

    if sparse:
        if copy_:
            return [x.copy() for x in output]
        else:
            return output
    else:
        # Return the full N-D matrix (not only the 1-D vector)
        if copy_:
            mult_fact = np.ones(shape, dtype=int)
            return [x * mult_fact for x in output]
        else:
            return np.broadcast_arrays(*output)


# copied from Numpy 1.10
# this is so the code can run on systems with older installations of numpy
def stack(arrays, axis=0):
    """
    Join a sequence of arrays along a new axis.
    The `axis` parameter specifies the index of the new axis in the dimensions
    of the result. For example, if ``axis=0`` it will be the first dimension
    and if ``axis=-1`` it will be the last dimension.
    .. versionadded:: 1.10.0
    Parameters
    ----------
    arrays : sequence of array_like
        Each array must have the same shape.
    axis : int, optional
        The axis in the result array along which the input arrays are stacked.
    Returns
    -------
    stacked : ndarray
        The stacked array has one more dimension than the input arrays.
    See Also
    --------
    concatenate : Join a sequence of arrays along an existing axis.
    split : Split array into a list of multiple sub-arrays of equal size.
    Examples
    --------
    >>> arrays = [np.random.randn(3, 4) for _ in range(10)]
    >>> np.stack(arrays, axis=0).shape
    (10, 3, 4)
    >>> np.stack(arrays, axis=1).shape
    (3, 10, 4)
    >>> np.stack(arrays, axis=2).shape
    (3, 4, 10)
    >>> a = np.array([1, 2, 3])
    >>> b = np.array([2, 3, 4])
    >>> np.stack((a, b))
    array([[1, 2, 3],
           [2, 3, 4]])
    >>> np.stack((a, b), axis=-1)
    array([[1, 2],
           [2, 3],
           [3, 4]])
    """
    arrays = [np.asanyarray(arr) for arr in arrays]
    if not arrays:
        raise ValueError('need at least one array to stack')

    shapes = set(arr.shape for arr in arrays)
    if len(shapes) != 1:
        raise ValueError('all input arrays must have the same shape')

    result_ndim = arrays[0].ndim + 1
    if not -result_ndim <= axis < result_ndim:
        msg = 'axis {0} out of bounds [-{1}, {1})'.format(axis, result_ndim)
        raise IndexError(msg)
    if axis < 0:
        axis += result_ndim

    sl = (slice(None),) * axis + (np.newaxis,)
    expanded_arrays = [arr[sl] for arr in arrays]
    return np.concatenate(expanded_arrays, axis=axis)

