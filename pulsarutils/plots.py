import numpy as np
from scipy import interpolate
from matplotlib import pyplot as plt
from matplotlib import gridspec, colors, ticker
from . import basic
tokenize = basic.tokenize
get_label = basic.get_label


def shifted_color_map(cmap, start=0, midpoint=0.5, stop=1.0,
                      name='shiftedmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero.

    Source:
    http://stackoverflow.com/questions/7404116/defining-the-midpoint-of-a-colormap-in-matplotlib
    Corrected version:
    https://github.com/TheChymera/chr-helpers/blob/d05eec9e42ab8c91ceb4b4dcc9405d38b7aed675/chr_matplotlib.py
    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.
    '''
    from matplotlib import colors
    from matplotlib import pyplot as plt
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }
    # regular index to compute the colors
    reg_index = np.hstack([
        np.linspace(start, 0.5, 128, endpoint=False),
        np.linspace(0.5, stop, 129)
    ])
    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False),
        np.linspace(midpoint, 1.0, 129)
    ])
    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)
        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))
    newcmap = colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)
    return newcmap


def hmean_spec(wasds):
    """ output the ASD of the harmonic mean of the input spectra (where the
    input is a list of numpy arrays of time weighted power spectra)"""
    for i, v in enumerate(wasds):
        if i == 0:
            mspec = np.divide(1., v)
        else:
            mspec = np.add(mspec, np.divide(1., v))
    return np.sqrt(np.divide(1., mspec))


def plot2d(x, y, c, method='hexbin', cm='RdGy', alpha=1, vmin=None, 
           vmax=None, linthresh=None, scale='symlog', hscale='log', xlabel='x',
           ylabel='y', clabel=''):
    method = method.lower()
    fig, ax = plt.subplots(1)
    cmin = c.min()
    cmax = c.max()
    vmin = vmin or cmin
    vmax = vmax or cmax
    # set linear or log color normalization
    if scale == 'linear':
        normalization = colors.Normalize(vmin=vmin, vmax=vmax)
        cmap = plt.get_cmap(cm)
    elif scale == 'log':
        normalization = colors.LogNorm(vmin=vmin, vmax=vmax)
        cmap = plt.get_cmap(cm)
    elif scale == 'symlog':
        linscale = np.log10(cmax/np.abs(cmin))/4.
        linthresh = linthresh or 10.0*min(c[c>0])
        logthresh = int(np.log10(linthresh))
        normalization = colors.SymLogNorm(linthresh=linthresh,
                                          linscale=linscale,
                                          vmin=vmin, vmax=vmax)
        # compute midpoint location and shift cmap
        if cmin < 0:
            midpoint = normalization([0])[0]
            if -10*linthresh < cmin:
                start = midpoint*(vmax-vmin)/vmax
                if False: # midxs[1] != 'N':
                    start *= 10
                if start > 1:
                    start = 0
            else:
                start = 0
        else:
            midpoint = 1
            start = 0
        cmap = shifted_color_map(plt.get_cmap(cm), start=start,
                                 midpoint=midpoint)
    else:
        raise ValueError("unsupported colorbar scale: %r" % scale)
    # plot with indicated method
    if method == 'imshow':
        xi = np.logspace(np.log10(x.min()), np.log10(x.max()), 100)
        yi = np.logspace(np.log10(y.min()), np.log10(y.max()), 100)
        xi, yi = np.meshgrid(xi, yi)
        rbf = interpolate.Rbf(x, y, c, function='linear')
        zi = rbf(xi, yi)
        im = ax.imshow(zi, vmin=vmin, vmax=vmax, origin='lower',
                       extent=[x.min(), x.max(), y.min(), y.max()], cmap=cmap,
                       norm=normalization, interpolation=None, alpha=alpha)
        ax.set_yscale(hscale)
        ax.set_xscale(hscale)
    elif method == 'scatter':
        ax.set_yscale(hscale)
        ax.set_xscale(hscale)
        im = ax.scatter(x, y, c=c, marker='s', lw=0, s=60, alpha=alpha,
                        cmap=cmap, norm=normalization)
        ax.set_xlim(x.min(), x.max())
        ax.set_ylim(y.min(), y.max())
    elif method == 'hexbin':
        im = ax.hexbin(x, y, C=c, xscale=hscale, yscale=hscale, cmap=cmap,
                       gridsize=35, norm=normalization, alpha=alpha)
    else:
        raise ValueError("unsupported plot method %r" % method)
    if scale == 'symlog':
        maxlog = int(np.ceil(np.log10(vmax)))
        minlog = int(np.ceil(np.log10(np.abs(vmin))))
        logstep = 2
        ticks = [vmin, 0] + [(10**x) for x in xrange(-logthresh, maxlog+1,
                                                     logstep)][1:]
        if not (vmin < 0 and 2*minlog < maxlog):
            ticks = [-(10**x) for x in xrange(-logthresh, minlog+1,
                                              logstep)][::-1] + ticks
        formatter = ticker.LogFormatterMathtext()
        cb = plt.colorbar(im, ax=ax, ticks=ticks, format=formatter)
    else:
        cb = plt.colorbar(im, ax=ax)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    cb.set_label(clabel)
    return fig, ax


def freqplot(results_dict, yval=None, ylabel=None, figsize=None,
             flim=(15, 1.5e3), ylim=None, spectrum=None, spec_ylim=None,
             spec_slope=None, spec_offset=None, spec_color='red', spec_alpha=1,
             legend_loc='best', spec_intercept=False, spec_lw=1,
             legend_size=22, yscale='linear'):
    """

    Parameters
    ----------
    results_dict
    yval
    ylabel
    figsize
    flim
    ylim
    spectrum : ppe.Spectrum

    Returns
    -------

    """
    mods = results_dict.search_models
    m0, m1 = mods.labels
    fgws = results_dict.get_psr_param('FGW')
    means = []
    stds_u = []
    stds_l = []
    if yval is None and ylabel is None:
        # warn: assumes all logs have same base
        results = results_dict[results_dict.keys()[0]]
        if results.bayes.logbase == np.exp(1):
            logbase = None
        else:
            logbase = results.bayes.logbase
        ylabel = basic.get_label('B', m0=m0, m1=m1, logbase=logbase)
    for rkey, results in results_dict.iteritems():
        if yval is not None:
            if isinstance(yval, dict):
                yvals = np.array(yval[rkey])
            else:
                yvals = yval
        else:
            yvals = np.array(results.bayes)
        mean = np.average(yvals)
        means.append(mean)
        if len(yvals) > 1:
            stds_u.append(np.std(yvals[yvals > mean]))
            stds_l.append(np.std(yvals[yvals < mean]))
    means = np.array(means)
    stds_u = np.array(stds_u)
    stds_l = np.array(stds_l)
    if any(stds_u):
        stds_avg = np.average([stds_u, stds_l], axis=0)
    else:
        stds_avg = np.ones(len(means))
    # Plot spectrum
    fig = plt.figure(figsize=figsize)
    ax1 = fig.add_subplot(111)
    ax1.set_yscale(yscale)
    ax1.set_xlim(flim[0], flim[1])
    ax1.plot(fgws, means, 'k.', label='Results', zorder=3)
    if len(yvals) > 1:
        ax1.errorbar(fgws, means, yerr=[stds_l, stds_u], fmt='k.',
                     elinewidth=1, alpha=0.5, zorder=2)
    ax1.set_xlabel(basic.get_label('FGW'))
    ax1.set_ylabel(ylabel or basic.get_label('B', m0=m0, m1=m1))
    ax1.yaxis.tick_left()
    ax1.grid(True)
    if ylim:
        ymin, ymax = ylim
        ax1.set_ylim(ymin, ymax)
    else:
        ymin, ymax = ax1.get_ylim()
    ax2 = fig.add_subplot(111, sharex=ax1, frameon=False)
    if spectrum is not None:
        # Fit spectrum
        spectrum_key = spectrum.kind.upper()
        ylabel1 = basic.get_label(spectrum_key)
        if spec_slope and spec_offset:
            slope, offset = (spec_slope, spec_offset)
        else:
            slope, offset = spectrum.fit(fgws, means, err=stds_avg,
                                         intercept=spec_intercept)
        left_to_right = lambda y: (y - offset)/slope
        right_to_left = lambda spec: slope*spec + offset
        # Plot spectrum
        ax1.plot(spectrum.freq, right_to_left(spectrum), color=spec_color,
                 alpha=spec_alpha, zorder=1, linewidth=spec_lw,
                 label=spectrum_key)
        # To check scale uncomment following line,
        # this should overlap with the above:
        # ax2.plot(spectrum.freq, spectrum, color='magenta', alpha=0.4,
        #          linewidth=2)
        # Reset limits to make sure spectrum doesn't affect range
        ax1.set_ylim(ymin, ymax)
        ax2.set_yscale(yscale)
        ylims_right = spec_ylim or (left_to_right(ymin), left_to_right(ymax))
        ax2.set_ylim(min(ylims_right), max(ylims_right))
        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        ax2.set_ylabel(ylabel1)
        ax1.legend(numpoints=1, loc=legend_loc, fontsize=legend_size)
    else:
        ax2.yaxis.set_major_locator(plt.NullLocator())
    # Set limits
    for ax in [ax1, ax2]:
        ax.set_xscale('log')
        ax.set_xlim(flim[0], flim[1])
    return fig, ax1, ax2


def freqplot_list(results_dicts, yvals=None, labels=None, ylabel=None,
                  figsize=None, flim=(15, 1.5e3), ylim=None, spectrum=None,
                  spec_ylim=None, spec_slope=None, spec_offset=None,
                  spec_color='red', spec_alpha=1, legend_loc='best',
                  spec_intercept=False, spec_lw=1, legend_size=22,
                  yscale='linear', fit_spec_to=-1, plot_styles=[]):
    """

    Parameters
    ----------
    results_dicts : list
        list of ResultsDict objects.
    yvals
    ylabel
    figsize
    flim
    ylim
    spectrum : ppe.Spectrum
    fit_spec_to: int
        index indicating to which of the results to fit the spectrum.

    Returns
    -------

    """
    means_lst = []
    stds_u_lst = []
    stds_l_lst = []
    stds_avg_lst = []
    labels_lst = labels or []
    for i, results_dict in enumerate(results_dicts):
        fgws = results_dict.get_psr_param('FGW')
        means = []
        stds_u = []
        stds_l = []
        if yvals is None and labels is None:
            # warn: assumes all logs have same base
            results = results_dict[results_dict.keys()[0]]
            if results.bayes.logbase == np.exp(1):
                logbase = None
            else:
                logbase = results.bayes.logbase
            label = "%s vs %s" % results_dict.search_models.labels
            labels_lst.append(label)
        for rkey, results in results_dict.iteritems():
            if yvals is not None:
                yval = yvals[i]
                if isinstance(yval, dict):
                    local_yvals = np.array(yval[rkey])
                else:
                    local_yvals = yval
            else:
                local_yvals = np.array(results.bayes)
            mean = np.average(local_yvals)
            means.append(mean)
            if len(yvals) > 1:
                stds_u.append(np.std(local_yvals[local_yvals > mean]))
                stds_l.append(np.std(local_yvals[local_yvals < mean]))
        means_lst.append(np.array(means))
        stds_u_lst.append(np.array(stds_u))
        stds_l_lst.append(np.array(stds_l))
        if any(stds_u):
            stds_avg_lst.append(np.average([stds_u, stds_l], axis=0))
        else:
            stds_avg_lst.append(np.ones(len(means)))
    if not plot_styles:
        plot_styles = [{'marker': '.', 'linestyle': 'None'} for m in means_lst]
    fig = plt.figure(figsize=figsize)
    ax1 = fig.add_subplot(111)
    ax1.set_yscale(yscale)
    ax1.set_xlim(flim[0], flim[1])
    zorder = 2
    properties = zip(means_lst, stds_u_lst, stds_l_lst, stds_avg_lst,\
                     labels_lst, plot_styles)
    for means, stds_u, stds_l, stds_avg, label, kwargs in properties:
        ax1.plot(fgws, means, label=label, zorder=zorder+1, **kwargs)
        if not any(np.isnan(stds_u)):
            ax1.errorbar(fgws, means, yerr=[stds_l, stds_u], fmt='k.',
                         elinewidth=1, alpha=0.5, zorder=zorder)
        zorder += 2
    ax1.set_xlabel(basic.get_label('FGW'))
    ax1.set_ylabel(ylabel or basic.get_label('B', m0=' ', m1=' ',
                                             logbase=logbase))
    ax1.yaxis.tick_left()
    ax1.grid(True)
    if ylim:
        ymin, ymax = ylim
        ax1.set_ylim(ymin, ymax)
    else:
        ymin, ymax = ax1.get_ylim()
    ax2 = fig.add_subplot(111, sharex=ax1, frameon=False)
    if spectrum is not None:
        # Fit spectrum
        spectrum_key = spectrum.kind.upper()
        ylabel1 = basic.get_label(spectrum_key)
        if spec_slope and spec_offset:
            slope, offset = (spec_slope, spec_offset)
        else:
            slope, offset = spectrum.fit(fgws, means_lst[fit_spec_to],
                                         err=stds_avg_lst[fit_spec_to],
                                         intercept=spec_intercept)
        left_to_right = lambda y: (y - offset)/slope
        right_to_left = lambda spec: slope*spec + offset
        # Plot spectrum
        ax1.plot(spectrum.freq, right_to_left(spectrum), color=spec_color,
                 alpha=spec_alpha, zorder=1, linewidth=spec_lw,
                 label=spectrum_key)
        # To check scale uncomment following line,
        # this should overlap with the above:
        # ax2.plot(spectrum.freq, spectrum, color='magenta', alpha=0.4,
        #          linewidth=2)
        # Reset limits to make sure spectrum doesn't affect range
        ax1.set_ylim(ymin, ymax)
        ax2.set_yscale(yscale)
        ylims_right = spec_ylim or (left_to_right(ymin), left_to_right(ymax))
        ax2.set_ylim(min(ylims_right), max(ylims_right))
        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        ax2.set_ylabel(ylabel1)
    else:
        ax2.yaxis.set_major_locator(plt.NullLocator())
    ax1.legend(numpoints=1, loc=legend_loc, fontsize=legend_size)
    # Set limits
    for ax in [ax1, ax2]:
        ax.set_xscale('log')
        ax.set_xlim(flim[0], flim[1])
    return fig, ax1, ax2


def hist_grid(lnbs_lsts, idxs_lsts=None, ylabel=r"$\log {\cal O}^m_{\rm N}$",
              grid_format='box', figsize=(20, 5), bw_method='silverman',
              showmeans=False, showmedians=True, widths=None, whis='range'):
    if idxs_lsts is None:
        idxs_lsts = [None] * len(lnbs_lsts)
    fig = plt.figure(figsize=figsize)
    n_subplots = len(lnbs_lsts)
    n_frames = sum([len(lnbs_lst) for lnbs_lst in lnbs_lsts])
    width_ratios = [float(len(lnbs_lst))/n_frames for lnbs_lst in lnbs_lsts]
    gs = gridspec.GridSpec(1, n_subplots, width_ratios=width_ratios)
    gs.update(wspace=0.05)
    axes = [plt.subplot(g) for g in gs]
    axes[0].set_ylabel(ylabel)
    ymin = min([min(np.ravel(lnbs_lst)) for lnbs_lst in lnbs_lsts]) - 0.1
    ymax = max([max(np.ravel(lnbs_lst)) for lnbs_lst in lnbs_lsts]) + 0.1
    for ax, lnbs_lst, idxs in zip(axes, lnbs_lsts, idxs_lsts):
        locations = range(len(idxs))
        if grid_format == 'box':
            widths = widths or 0.5
            plot_parts = ax.boxplot(lnbs_lst, positions=locations, vert=True,
                                    widths=widths, patch_artist=True, 
                                    whis=whis)
            format_box(plot_parts)
        elif grid_format == 'violin':
            widths = widths or 0.5
            plot_parts = ax.violinplot(lnbs_lst, locations, vert=True,
                                       showmeans=showmeans, widths=widths,
                                       showmedians=showmedians,
                                       #showextrema=False,
                                       bw_method=bw_method)
            format_violin(plot_parts, fc='0.7')
        ax.grid(True)
        ax.set_xticks(locations)
        ax.set_xlim(locations[0] - 0.5, locations[-1] + 0.5)
        ax.set_ylim(ymin, ymax)
        if idxs is not None:
            ax.set_xticklabels(idxs, size=20)
        if ax != axes[0]:
            ax.set_yticklabels([])
    return fig, axes


def bar_grid(lnbs_lsts, idxs_lsts=None, width=0.2, figsize=(20, 5),
             color='gray', ylabel=r"$\log {\cal O}^m_{\rm N}$"):
    if idxs_lsts is None:
        idxs_lsts = [None] * len(lnbs_lsts)
    fig = plt.figure(figsize=figsize)
    n_subplots = len(lnbs_lsts)
    n_frames = sum([len(lnbs_lst) for lnbs_lst in lnbs_lsts])
    width_ratios = [float(len(lnbs_lst))/n_frames for lnbs_lst in lnbs_lsts]
    gs = gridspec.GridSpec(1, n_subplots, width_ratios=width_ratios)
    gs.update(wspace=0.05)
    axes = [plt.subplot(g) for g in gs]
    axes[0].set_ylabel(ylabel)
    ymin = min([min(np.ravel(lnbs_lst)) for lnbs_lst in lnbs_lsts]) - 0.1
    ymax = max([max(np.ravel(lnbs_lst)) for lnbs_lst in lnbs_lsts]) + 0.1
    for ax, lnbs_lst, idxs in zip(axes, lnbs_lsts, idxs_lsts):
        locations = np.arange(len(idxs))*2*width
        lnbs_lst = [lnbs[0] for lnbs in lnbs_lst]
        ax.bar(locations, lnbs_lst, width=width, color=color)
        ax.grid(True)
        xlocs = locations + width/2.
        ax.set_xticks(xlocs)
        margin = width
        ax.set_xlim(xlocs[0] - margin, xlocs[-1] + margin)
        ax.set_ylim(ymin, ymax)
        if idxs is not None:
            ax.set_xticklabels(idxs, size=20)
        if ax != axes[0]:
            ax.set_yticklabels([])
    return fig, axes


def format_violin(violinparts, fc='0.4', lw=0, alpha=1, ec='k', lc='k'):
    # http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.violinplot
    for body in violinparts['bodies']:
        body.set_facecolor(fc)
        #body.set_hatch('\\')
        body.set_linewidths(lw)
        body.set_edgecolor(ec)
        body.set_alpha(alpha)
    line_keys = ['cmins', 'cmaxes', 'cbars', 'cmedians']
    for patch in [violinparts[k] for k in line_keys]:
        patch.set_edgecolor(lc)


def format_box(plot_parts, linecolor='k', facecolor='lightgray'):
    for patch in plot_parts['boxes'] + plot_parts['whiskers']:
        patch.set_color(linecolor)
    for patch in plot_parts['boxes']:
        patch.set_facecolor(facecolor)
