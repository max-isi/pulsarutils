import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
try:
    matplotlib.style.use('classic')
except AttributeError:
    pass


mplparams = {
    'text.usetex': True,  # use LaTeX for all text
    'axes.linewidth': 1,  # set axes linewidths to 0.5
    'axes.grid': False,  # add a grid
    'axes.labelweight': 'normal',
    'font.family': 'serif',
    'font.size': 24,
    'font.serif': 'Computer Modern Roman'
}
matplotlib.rcParams.update(mplparams)
