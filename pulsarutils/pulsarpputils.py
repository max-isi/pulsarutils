# -*- coding: utf-8 -*-
#
#       pulsarpputils.py
#
#       Copyright 2012
#       Matthew Pitkin <matthew.pitkin@ligo.org>
#
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

# import sys
# import math
# import cmath
import os
import numpy as np
# import struct
# import re
# import h5py
#
# from scipy.integrate import cumtrapz
# from scipy.interpolate import interp1d
# from scipy.stats import hmean
# from scipy.misc import logsumexp
#
# from types import StringType, FloatType


def pulsar_nest_to_posterior(postfile, nestedsamples=False, removeuntrig=True):
  """
  This function will import a posterior sample file created by `lalapps_nest2pos` (or a nested
  sample file). It will be returned as a Posterior class object from `bayespputils`. The
  signal evidence and noise evidence are also returned.

  Parameters
  ----------
  postfile : str, required
      The file name of the posterior or nested sample file. In general this should be a HDF5 file with the extension
      '.hdf' or '.h5', although older format ascii files are still supported at the moment.
  nestedsamples : bool, default: False
      If the file being input contains nested samples, rather than posterior samples, then this flag must be set to
      True
  removeuntrig : bool, default: True
      If this is True then any parameters that are sines or cosines of a value will have the value removed if present
      e.g. if cosiota and iota exist then iota will be removed.
  """

  from pylal import bayespputils as bppu
  from pylal.bayespputils import replace_column

  fe = os.path.splitext(postfile)[-1].lower() # file extension

  # combine multiple nested sample files for an IFO into a single posterior (copied from lalapps_nest2pos)
  if fe in ['.h5', '.hdf', '.hdf5']:
    if nestedsamples:
      # use functions from lalapps_nest2pos to read values from nested sample files i.e. not a posterior file created by lalapps_nest2pos
      try:
        from lalinference.io import read_samples
        import lalinference

        samples = read_samples(postfile, tablename=lalinference.LALInferenceHDF5NestedSamplesDatasetName)
        params = samples.colnames

        # make everything a float, since that's what's excected of a CommonResultsObj
        for param in params:
          replace_column(samples, param, samples[param].astype(float))

        nsResultsObject = (samples.colnames, samples.as_array().view(float).reshape(-1, len(params)))
      except:
        raise IOError, "Could not import nested samples"
    else: # assume posterior file has been created with lalapps_nest2pos
      peparser = bppu.PEOutputParser('hdf5')
      nsResultsObject = peparser.parse(postfile)
  elif fe == '.gz': # gzipped file
    import gzip
    peparser = bppu.PEOutputParser('common')
    nsResultsObject = peparser.parse(gzip.open(postfile, 'r'))
  else: # assume an ascii text file
    peparser = bppu.PEOutputParser('common')
    nsResultsObject = peparser.parse(open(postfile, 'r'))

  pos = bppu.Posterior( nsResultsObject, SimInspiralTableEntry=None, votfile=None )

  # remove any unchanging variables and randomly shuffle the rest
  pnames = pos.names
  nsamps = len(pos[pnames[0]].samples)
  permarr = np.arange(nsamps)
  np.random.shuffle(permarr)
  for pname in pnames:
    # check first and last samples are the same
    if pos[pname].samples[0] - pos[pname].samples[-1] == 0.:
      pos.pop(pname)
    else:
      # shuffle
      shufpos = bppu.PosteriorOneDPDF(pname, pos[pname].samples[permarr])
      pos.pop(pname)
      pos.append(shufpos)

  # # check whether iota has been used
  # try:
  #   posIota = pos['iota'].samples
  # except:
  #   posIota = None
  #
  # if posIota is not None:
  #   cipos = None
  #   cipos = bppu.PosteriorOneDPDF('cosiota', np.cos(posIota))
  #   pos.append(cipos)
  #   if removeuntrig:
  #     pos.pop('iota')
  #
  # # check whether sin(i) binary parameter has been used
  # try:
  #   posI = pos['i'].samples
  # except:
  #   posI = None
  #
  # if posI is not None:
  #   sinipos = None
  #   sinipos = bppu.PosteriorOneDPDF('sini', np.sin(posI))
  #   pos.append(sinipos)
  #   if removeuntrig:
  #     pos.pop('i')
  #
  # # convert C22 back into h0, and phi22 back into phi0 if required
  # try:
  #   posC21 = pos['c21'].samples
  # except:
  #   posC21 = None
  #
  # try:
  #   posC22 = pos['c22'].samples
  # except:
  #   posC22 = None
  #
  # try:
  #   posphi22 = pos['phi22'].samples
  # except:
  #   posphi22 = None
  #
  # # convert C22 back into h0, and phi22 back into phi0 if required
  # if posC22 is not None and posC21 is None:
  #   h0pos = None
  #   h0pos = bppu.PosteriorOneDPDF('h0', 2.*pos['c22'].samples)
  #
  #   pos.append(h0pos)
  #   pos.pop('c22')
  #
  #   if posphi22 is not None:
  #     phi0pos = None
  #     phi0pos = bppu.PosteriorOneDPDF('phi0', np.fmod(pos['phi22'].samples + math.pi, 2.*math.pi))
  #
  #     pos.append(phi0pos)
  #     pos.pop('phi22')
  #
  # # get evidence (as in lalapps_nest2pos)
  # if fe == '.h5' or fe == '.hdf':
  #   # read evidence from HDF5 file
  #   hdf = h5py.File(postfile, 'r')
  #   a = hdf['lalinference']['lalinference_nest']
  #   sigev = a.attrs['log_evidence']
  #   noiseev = a.attrs['log_noise_evidence']
  #   hdf.close()
  # else:
  #   B = np.loadtxt(postfile.replace('.gz', '')+'_B.txt')
  #   sigev = B[1]
  #   noiseev = B[2]

  # return posterior samples, signal evidence (B[1]) and noise evidence (B[2])
  return pos  #, sigev, noiseev