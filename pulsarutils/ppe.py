import numpy as np
import random
from copy import copy, deepcopy
import os
import glob
from warnings import warn
from ConfigParser import SafeConfigParser
from collections import OrderedDict
from scipy.stats import gaussian_kde
from scipy.misc import logsumexp
from . import basic
from . import stats
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


###############################################################################
# Functions

def subtractb(ba1, ba2):
    """Subtraction operation for LogBayesArray that keeps track of models.

    :rtype : LogBayesArray
    :param ba1: first array (BayesArray).
    :param ba2: second array (BayesArray).
    :return:
    """
    output = ba1 - ba2
    if isinstance(ba1, LogBayesArray) and isinstance(ba2, LogBayesArray):
        output.model_keys = (ba1.model_keys[0], ba2.model_keys[0])
    return output


def addb(bayes_arrays, axis=0, check_models=False):
    """Adds BayesArrays, checking all models are the same.
    """
    if check_models:
        keys = []
        for ba in bayes_arrays:
            if isinstance(ba, bayes_arrays):
                keys.append(ba.model_keys)
        if len(set(keys)) > 1:
            warn("not all models agree.")
    return np.sum(bayes_arrays, axis=axis)


def logavgexp_lnb(bayes_arrays):
    """Combines the natural log of Bayes factors from different hypotheses.

    This uses the numpy.logaddexp() function to average the Bayes factors in
    both arrays without exponentiating directly.

    :param bayes_arrays: array-like object with BayesArray elements.
    :return: BayesArray.
    """
    try:
        base = bayes_arrays[0].logbase
    except AttributeError:
        base = bayes_arrays.logbase
    # make sure array is in base e
    scaled_arrays = [ba * np.log(base) for ba in bayes_arrays]
    new_logb = logsumexp(scaled_arrays, axis=0, b=1.0/len(scaled_arrays))
    #new_logb = logsumexp(bayes_arrays, axis=0) - np.log(len(bayes_arrays))
    new_logb /= np.log(base)
    try:
        mk = bayes_arrays[0].model_keys
    except AttributeError:
        mk = bayes_arrays.model_keys
    return LogBayesArray(new_logb, model_keys=mk, logbase=base)


def compute_bci(coh_results_dict, inc_results_dict_list, simple=False):
    """Computes BCI from coherent and incoherent `ResultsDict` objects.

    If `simple`, the incoherent Bayes factor is given by a simple product:
        lnBI = lnB1 + lnB2
    otherwise, the "generic noise" formula is used:
        lnBI = ln(B1 + 1) + ln(B2 + 1).

    Arguments
    ---------
    coh_results_dict: ppe.ResultsDict
        coherent results, with keys corresponding to pulsars.
    inc_results_dict_list: list
        list of `ResultsDict` objects with incoherent results and keys
        corresponding to pulsars.
    simple: bool
        whether to compute incoherent odds as `lnBI = lnB1 + lnB2` (def: False).
    """
    lnbcis = []
    keys = []
    for key, results in coh_results_dict.iteritems():
        keys.append(key)
        coh_lnb = results.bayes[0]
        inc_lnb = 0
        for results_dict in inc_results_dict_list:
            if simple:
                inc_lnb += results_dict[key].bayes[0]
            else:
                logbase = results_dict[key].bayes.logbase
                bayes = results_dict[key].bayes.bayes()[0]
                inc_lnb += np.log1p(bayes)/np.log(logbase)
        lnbcis.append(coh_lnb - inc_lnb)
    return LogBayesArray(lnbcis), keys


def compute_bci_from_bs(coh_lnb, inc_lnb_list, simple=False):
    """Computes BCI from coherent and incoherent `ResultsDict` objects.

    If `simple`, the incoherent Bayes factor is given by a simple product:
        lnBI = lnB1 + lnB2
    otherwise, the "generic noise" formula is used:
        lnBI = ln(B1 + 1) + ln(B2 + 1).

    Arguments
    ---------
    coh_lnb: float, LogBayesArray
        float or array of coherent log-odds.
    inc_lnb_list: list
        list of LogBayesArrays; each containts the incoherent log-odds for each
        single detector.
    simple: bool
        whether to compute incoherent odds as `lnBI = lnB1 + lnB2` (def: False).
    """
    inc_lnb = 0
    for inc_lnb_ifo in inc_lnb_list:
        if simple:
            inc_lnb += inc_lnb_ifo
        else:
            logbase = inc_lnb_ifo.logbase
            bayes = inc_lnb_ifo.bayes()
            inc_lnb += np.log1p(bayes)/np.log(logbase)
    lnbcis = coh_lnb - inc_lnb
    return LogBayesArray(lnbcis)


def read_nested_from_ascii(nested_path_list, headers_path=None):
    """From `libexec/lalapps/lalapps_nest2pos.py`
    LALsuite # 067222ca7389f209ed54add618efbdc0775d14b4
    """
    header_path = headers_path or nested_path_list[0] + '_params.txt'
    with open(header_path, 'r') as f:
        header = f.readline()
    headers = header.split()
    input_arrays = map(np.loadtxt, nested_path_list)
    log_noise_evidences = []
    log_max_likelihoods = []
    for path in nested_path_list:
        path = path.replace('.gz',  '') + '_B.txt'
        if os.access(path, os.R_OK):
            content = np.loadtxt(path)
            log_noise_evidences.append(content[2])
            log_max_likelihoods.append(content[3])
    if log_noise_evidences:
        log_noise_evidence = reduce(np.logaddexp, log_noise_evidences)
        log_max_likelihood = max(log_max_likelihoods)
    else:
        log_noise_evidence = 0
        log_max_likelihood = 0
    return headers, input_arrays, log_noise_evidence, log_max_likelihood


def bootstrap_cl_error(samples, nboots, quantiles=[0.95], nbins=100,
                       confidence=90):
    """
    Computes uncertainties over credible levels using a Dirichlet distribution
    to approximate the posterior PDF obtained by histogramming a set of samples
    The basic math is the following:

    imagine we have a histogram (n1,...,nk), we want to estimate the
    probability associated with each bin. Call the set of probabiities
    (q1,...,qk), with the condition that \sum_i q_i = 1 We want

    p(q1,...,qk|n1,..,nk) \propto p(q1,...,qk)p(n1,..,nk|q1,...,qk)

    an histogram is a realisation of a multinomial distribution with given
    probabilities in each bin q1,...,qk.

    If we choose for the prior p(q1,...,qk() its conjugate prior, we know what
    the posterior is straightway.  For a multinomial distribution it is a
    Dirichlet distribution Dir(\alpha_1,...,\alpha_k).

    By default, we are going to take \alpha_1=...=\alpha_k=2 which assigns
    equal weights to each bin.

    Thus the posterior on the probabilities is also a Dirichlet distribution
    Dir(\alpha_1+n1+1,...,\alpha_k+nk+1)

    We generate samples from Dir(\alpha_1+n1+1,...,\alpha_k+nk+1) and we
    average over them to compute median and 90%CL on the input quantiles.

    NOTE: this script operates by fixing the number of bins (or equivalently a
    finite bin size). Generalisations are possible, (number of bins -> \infty)
    in which case speak of a Dirichlet Process. Marginalisation over the number
    of bins can be approximated by averaging the CLs over different bin sizes.

    (From Walter del Pozzo, 2017)

    Arguments
    ---------
    samples: array
        posterior samples
    nboots: int
        number of posterior-PDF realisations to average on
    quantiles: array
        list of quantiles for which to compute uncertainty (default: [95])
    nbins: int
        number of histogram bins (default: 100)
    confidence: int
        confidence level for error un credible interval (default: 90)

    Returns
    -------
    output: dict
        dictionary of median and 90% CL over the quantiles
    """
    # generate the observations (the histogram)
    hist, edges = np.histogram(samples, bins=nbins)
    centres = [0.5*(low+high) for low, high in zip(edges[:-1], edges[1:])]
    
    # choose alpha = 2 for dirichlet prior, corresponding to a uniform
    # distribution over the probabilities
    alphas = np.zeros(nbins) + 2.

    # for saving quantiles from each Nboot
    marginalised_quantile = {cl: np.zeros(nboots) for cl in quantiles}

    # now we want to infer the posterior pdf over the probabilities in each
    # bin using a Dirichlet priorupdate the concentration parameters
    updated_alphas = alphas + hist + 1.
    # generate nboots samples of the pdf
    pdfs = np.random.dirichlet(updated_alphas, size=nboots)
    
    # loop over the PDF realisations
    for b in range(nboots):
        cs = np.cumsum(pdfs[b, :])  # cumulative distribution
        for cl in marginalised_quantile.keys():
            binid = np.abs(cs - cl).argmin()
            marginalised_quantile[cl][b] = centres[binid]
    output = {cl: (np.percentile(marginalised_quantile[cl], 100-confidence),
                   np.median(marginalised_quantile[cl]),
                   np.percentile(marginalised_quantile[cl], confidence))
              for cl in marginalised_quantile.keys()}
    # print output
    # cs = np.cumsum(hist/float(len(samples)))  # cumulative distribution
    # cl = 0.95
    # binid = np.abs(cs - cl).argmin()
    # print centres[binid]
    return output


###############################################################################
# Classes

class LogBayesFile(object):
    """ Hold and manipulate Bayes factor from file output by lalapps_ppe_nested.

    These are ASCII files containing the Bayesian odds ratio comparing whether
    the data contained a signal or just noise. They include four values:
      (0) natural logarithm of the odds ratio,
      (1) log of the Bayesian evidence that the data contains a signal,
      (2) log of the Bayesian evidence that the data contains noise,
      (3) maximum log likelihood value from all the points.
    Note that the first value is just the second value minus the third value.
    """
    def __init__(self, logB=0, Zsignal=0, Znoise=0, logLmax=0, info=0, nlive=0,
                 model_keys=(None, None)):
        self.logB = logB  # log odds ratio
        self.Zsignal = Zsignal  # log signal evidence
        self.Znoise = Znoise  # log noise evidence
        self.logLmax = logLmax  # max log likelihood
        self.model_key = model_keys  # model tested
        self.filepath = None  # path of file loaded
        self.info = info
        self.nlive = nlive

    @classmethod
    def load(cls, filepath, **kwargs):
        """
        Load file from disk.
        :param filepath: path to file (str).
        """
        basename, extension = os.path.splitext(filepath)
        if extension in ['.hf', '.hdf', '.hdf5']:
            import h5py
            hdf = h5py.File(filepath, 'r')
            a = hdf['lalinference']['lalinference_nest']
            bayes = a.attrs['log_bayes_factor']
            sigev = a.attrs['log_evidence']
            noiseev = a.attrs['log_noise_evidence']
            loglmax = a.attrs['log_max_likelihood']
            info = a.attrs['information_nats']
            nlive = a.attrs['number_live_points']
            hdf.close()
        else:
            data = np.loadtxt(filepath)
            try:
                bayes = data[0]
                sigev = data[1]
                noiseev = data[2]
                loglmax = data[3]
                info = None
                nlive = None
            except IndexError:
                raise IOError("invalid Bayes file %r" % filepath)
        new = cls(logB=bayes, Zsignal=sigev, Znoise=noiseev,
                  logLmax=loglmax, info=info, nlive=nlive, **kwargs)
        new.filepath = filepath
        return new

    @classmethod
    def load_nongr(cls, masterpath, **kwargs):
        """ Produce an odds ratio by averaging Bayes factors corresponding
        to independent non-GR hypotheses.
        """
        if '(M)' not in masterpath:
            raise ValueError("path mask lacks (M) wildcard: %r" % masterpath)
        non_gr_keys = basic.Model(None)._nongr_keys()
        lnbs = []
        for mkey in non_gr_keys:
            bpath = basic.tokenize(masterpath, m=mkey)
            basename, extension = os.path.splitext(bpath)
            if extension in ['.hf', '.hdf', '.hdf5']:
                import h5py
                hdf = h5py.File(bpath, 'r')
                a = hdf['lalinference']['lalinference_nest']
                lnbs.append(a.attrs['log_bayes_factor'])
            else:
                data = np.loadtxt(bpath)
                lnbs.append(data[0])
        # produce an odds ratio by averaging Bayes factors
        lnodds = lnbs[0]
        for lnb in lnbs[1:]:
            lnodds = np.logaddexp(lnodds, lnb) - np.log(2)
        new = cls(logB=np.array(lnodds))
        new.filepath = masterpath
        return new


class LogBayesArray(np.ndarray):
    """
    Array of logarithms of Bayes factors (a subclass of np.ndarray).
    """
    _defaults = {
        'model_keys': (None, None),
        'origin': None,
        'logbase': np.exp(1)
    }

    def __new__(cls, input_data, **kwargs):
        if isinstance(input_data, LogBayesArray):
            return input_data
        obj = np.asarray(input_data).view(cls)
        # set metadata using kwargs or default value specified above:
        for key, value in kwargs.iteritems():
            if key not in cls._defaults.keys():
                raise TypeError("BayesArray() got an unexpected argument %r"
                                % key)
            elif key == 'model_keys' and (isinstance(value, basestring) or
                                                  len(value) != 2):
                raise ValueError("invalid model keys value %r" % value)
        for key, defvalue in cls._defaults.iteritems():
            setattr(obj, key, kwargs.get(key, defvalue))
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        # set metadata using kwargs or default value specified above:
        for key, value in self._defaults.iteritems():
            setattr(self, key, getattr(obj, key, value))

    @classmethod
    def collect_n(cls, masterpath, nf=None, n0=0, model_keys=(None, None)):
        """
        Collect Bayes factors from a path matching :masterpath:.

        Attempts to load Bayes factor files output from ppe_nested, assuming
        they contain a comparison between some model and noise. If two models
        are provided, will independently load files corresponding to m1 vs
        noise, then m2 vs noise and will combine them to produce m1 vs m2

        Argument :masterpath: is a path pointing to the location of the files.
        Note it must point to filesi not just a directory. The string should
        contain a "wildcard" (N) to be replaced by an integer; this will be
        used to iterate over a range from :n0: to :nf:-1.

        An optional wildcard, (M), can be use to represent the model name. This
        is useful if files corresponding to several models vs noise are stored
        in the same directory.

        :param masterpath: path "mask" to Bayes factors (str).
        :param nf: maximum value of (N) to load (int).
        :param n0: minimum value of (N) to load (int).
        :param model_keys: models keys M or (M0, M1) to load (str or tuple).
        """
        if '(N)' not in masterpath:
            raise ValueError("path mask lacks (N) wildcard: %r" % masterpath)
        masterpath = os.path.abspath(masterpath)

        if isinstance(model_keys, basestring):
            model_keys = (model_keys, None)
        elif len(model_keys) == 1 and isinstance(model_keys[0], basestring):
            model_keys = (model_keys[0], None)
        elif len(model_keys) == 2 and isinstance(model_keys[0], basestring)\
                and isinstance(model_keys[1], basestring):
            pass
        else:
            raise ValueError("Invalid models: %r" % model_keys)

        models_given = [m not in (None, "noise", "n") for m in model_keys]

        if nf is None:
            warn("Guessing number of instantiations from directory contents.")
            # use first model key to guess number of injections
            guesspath = basic.tokenize(masterpath.replace('(N)', '*'),
                                       m=model_keys[0])
            # determine the number of files matching :guesspath:
            nf = len(glob.glob(guesspath))
            if nf == 0:
                raise IOError("no files found matching %r" % guesspath)
        d = []
        for n in range(n0, nf+1):
            path = masterpath.replace('(N)', str(n))
            try:
                if all(models_given):
                    bfs = []
                    for mk in model_keys:
                        if mk.lower() in ['ngr', 'nongr']:
                            bf_i = LogBayesFile.load_nongr(path)
                        else:
                            bf_i = LogBayesFile.load(path.replace('(M)', mk))
                        bfs.append(bf_i)
                    bf = LogBayesFile(logB=bfs[0].logB-bfs[1].logB)
                elif models_given[0]:
                    mk = model_keys[0]
                    if mk.lower() in ['ngr', 'nongr']:
                        bf = LogBayesFile.load_nongr(path)
                    else:
                        bf = LogBayesFile.load(path.replace('(M)', mk))
                elif models_given[1]:
                    # logB for noise vs model (rather than model vs noise)
                    # should this be allowed?
                    mk = model_keys[1]
                    if mk.lower() in ['ngr', 'nongr']:
                        bf = LogBayesFile.load_nongr(path)
                    else:
                        bf = LogBayesFile.load(path.replace('(M)', mk))
                    bf.logB = - bf.logB
                else:
                    raise ValueError("must provide at least one model key.")
                d.append(bf.logB)
            except IOError, e:
                warn("Did not find %r. Error: %s" % (path, e))
        return cls(d, origin=masterpath, model_keys=model_keys)

    @classmethod
    def collect_global(cls, globalpath, wc='*', wc_values=None,
                       reference=None):
        """
        Collect Bayes factors from paths matching :globalpath:.

        Attempts to load Bayes factor files output from ppe_nested. This will
        generally be a Bayes factor with respect to noise. The :reference:
        argument can be be a path pointing to a particular Bayes file; if this
        is provided, that file is loaded and subtracted from all values of
        self.logB, such that the stored Bayes factors are with respect to the
        reference: B^{some model}_{reference model}.


        Argument :globalpath: is a path pointing to the location of the files.
        Note it must point to files not just a directory. The string should
        contain a wildcard :wc:. If :wc_values: is provided, only paths with
        that match the global expression with a wildcard value in the list will
        be loaded.

        :param globalpath: path regexp to Bayes factors (str).
        :param wc: wildcard (str).
        :param wc_values: optional wildcard values (list).
        :param reference: optional path to reference Bayes file (str).
        """
        if not isinstance(globalpath, basestring):
            raise TypeError("globalpath must be a string, not %r"
                            % type(globalpath))
        if reference is None:
            logb_ref = 0.
        else:
            if os.path.exists(reference):
                logb_ref = LogBayesFile.load(reference).logB
            else:
                raise ValueError("Reference points to non-existing file: %r"
                                 % reference)
        bf_list = []
        if wc_values is not None:
            # wc_values is list of strings
            for value in wc_values:
                bf = LogBayesFile.load(globalpath.replace(wc, str(value)))
                bf_list.append(bf.logB - logb_ref)
        else:
            paths = glob.glob(globalpath.replace(wc, '*'))
            for path in paths:
                bf = LogBayesFile.load(path)
                bf_list.append(bf.logB - logb_ref)
        if len(bf_list) == 0:
            raise IOError("Did not find any Bayes files matching %r"
                          % globalpath)
        return cls(bf_list, origin=globalpath)

    def export(self, path):
        """Save list of Bayes factors to disk.

        Format determined from extension (currently only supports ASCII).
        :param: path: path to destination file (str).
        """
        filename, extension = os.path.splitext(path)
        if '.hdf' in extension:
            raise NotImplementedError("no HDF5 support not yet.")
        else:
            np.savetxt(path, self, fmt="%.6f")

    def hist(self, *args, **kwargs):
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        n, bins, _ = ax.hist(self, *args, **kwargs)
        return fig, ax, n, bins

    def logaddexp(self):
        """Natural logarithm of the sum of exp(self), taking care of log base.
        """
        return np.log(np.sum(self.bayes()))/np.log(self.logbase)

    def logavgexp(self):
        """Logarithm of the average of Bayes factors.

        Natural logarithm of (1/N) time the sum of exp(self), where N is the
        number of elements (length of the array.
        """
        avgexp = np.average(self.bayes())
        return np.log(avgexp)/np.log(self.logbase)

    def bayes(self):
        return self.logbase**self

    def logprodexp(self):
        raise NotImplementedError()

    def append(self, value):
        """Appends :value: to array (not in-place).

        :param value number or array (array_like)
        """
        return LogBayesArray(np.append(self, value))

    def append_from_file(self, path, **kwargs):
        if os.path.exists(path):
            value = LogBayesFile.load(path, **kwargs)
        else:
            value = LogBayesArray.collect_global(path, **kwargs)
        return self.append(value)

    def gkde(self, value, *args, **kwargs):
        """Returns the gkd interpolation of the value.

        This assumes tha logB values in the array represent some sort of PDF
        distribution which is approximated via gkd to return the inreporlated
        (or extrapolated) value

        :returns float
        """
        return gaussian_kde(self, *args, **kwargs)(value)

    def pvalue_kde(self, logb, lolim=-np.inf, uplim=np.inf):
        """ Returns the pvalue of :logB: treating the values in array as
        background. Computed using Gaussian KDE approximation of the
        distribution.

        Numerically integrates pdf estimated via gkd from :lolim: up to the
        indicated value and returns 1.0 minus that quantity.

        :return pvalue (float)
        """
        from scipy.integrate import quad
        # the norm of the pdf should always be unity, but just in case:
        norm = quad(self.gkde, lolim, uplim)[0]
        pvalue = 1.0 - quad(self.gkde, lolim, logb)[0] / norm
        print pvalue
        return pvalue

    def pvalue(self, logb):
        """Computes p-value computed from simple ordering of the samples.
        (Right handed tail).
        """
        sorted_samples = np.sort(self)
        logb_placement = float(sorted_samples.searchsorted(logb))
        pvalue = 1. - logb_placement/len(self)
        if pvalue <= 0:
            pvalue = 1./len(self)
        return pvalue

    def newbase(self, newbase):
        newarray = deepcopy(self) / np.log(newbase)
        newarray.logbase = newbase
        return newarray

    def fit_dist(self, name, *args, **kwargs):
        dist = stats.distribution(name)
        param = dist.fit(self, *args, **kwargs)
        return param

    def plot_fit_dist(self, fit_dist, *args, **kwargs):
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        fit_kwargs = kwargs.pop('fit_kwargs', {})
        x = kwargs.pop('x', np.linspace(min(self), max(self), 100))
        dist = stats.distribution(fit_dist)
        fitpar = self.fit_dist(fit_dist, **fit_kwargs)
        fitpdf = dist.pdf(x, *fitpar[:-2], loc=fitpar[-2], scale=fitpar[-1])
        scale = kwargs.pop('scale', False)
        if scale:
            fitpdf *= float(len(self))
        ax.plot(x, fitpdf, *args, **kwargs)
        return fig, ax


class InfoArray(np.ndarray):
    """
    Array of info H in nats (a subclass of np.ndarray).
    """
    _defaults = {
        'model_keys': (None, None),
        'origin': None
    }

    def __new__(cls, input_data, **kwargs):
        if isinstance(input_data, LogBayesArray):
            return input_data
        obj = np.asarray(input_data).view(cls)
        # set metadata using kwargs or default value specified above:
        for key, value in kwargs.iteritems():
            if key not in cls._defaults.keys():
                raise TypeError("BayesArray() got an unexpected argument %r"
                                % key)
            elif key == 'model_keys' and (isinstance(value, basestring) or
                                                  len(value) != 2):
                raise ValueError("invalid model keys value %r" % value)
        for key, defvalue in cls._defaults.iteritems():
            setattr(obj, key, kwargs.get(key, defvalue))
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        # set metadata using kwargs or default value specified above:
        for key, value in self._defaults.iteritems():
            setattr(self, key, getattr(obj, key, value))

    @classmethod
    def collect_n(cls, masterpath, nf=None, n0=0, model_keys=(None, None)):
        """
        Collect Bayes factors from a path matching :masterpath:.

        Attempts to load Bayes factor files output from ppe_nested, assuming
        they contain a comparison between some model and noise. If two models
        are provided, will independently load files corresponding to m1 vs
        noise, then m2 vs noise and will combine them to produce m1 vs m2

        Argument :masterpath: is a path pointing to the location of the files.
        Note it must point to filesi not just a directory. The string should
        contain a "wildcard" (N) to be replaced by an integer; this will be
        used to iterate over a range from :n0: to :nf:-1.

        An optional wildcard, (M), can be use to represent the model name. This
        is useful if files corresponding to several models vs noise are stored
        in the same directory.

        :param masterpath: path "mask" to Bayes factors (str).
        :param nf: maximum value of (N) to load (int).
        :param n0: minimum value of (N) to load (int).
        :param model_keys: models keys M or (M0, M1) to load (str or tuple).
        """
        if '(N)' not in masterpath:
            raise ValueError("path mask lacks (N) wildcard: %r" % masterpath)
        masterpath = os.path.abspath(masterpath)

        if isinstance(model_keys, basestring):
            model_keys = (model_keys, None)
        elif len(model_keys) == 1 and isinstance(model_keys[0], basestring):
            model_keys = (model_keys[0], None)
        elif len(model_keys) == 2 and isinstance(model_keys[0], basestring)\
                and isinstance(model_keys[1], basestring):
            pass
        else:
            raise ValueError("Invalid models: %r" % model_keys)

        models_given = [m not in (None, "noise", "n") for m in model_keys]

        if nf is None:
            warn("Guessing number of instantiations from directory contents.")
            # use first model key to guess number of injections
            guesspath = basic.tokenize(masterpath.replace('(N)', '*'),
                                       m=model_keys[0])
            # determine the number of files matching :guesspath:
            nf = len(glob.glob(guesspath)) - 1
            if nf < 1:
                raise IOError("no files found matching %r" % guesspath)
        d = []
        for n in range(n0, nf+1):
            path = masterpath.replace('(N)', str(n))
            try:
                if all(models_given):
                    bfs = []
                    for mk in model_keys:
                        if mk.lower() in ['ngr', 'nongr']:
                            bf_i = LogBayesFile.load_nongr(path)
                        else:
                            bf_i = LogBayesFile.load(path.replace('(M)', mk))
                        bfs.append(bf_i)
                    bf = LogBayesFile(logB=bfs[0].logB-bfs[1].logB)
                elif models_given[0]:
                    mk = model_keys[0]
                    if mk.lower() in ['ngr', 'nongr']:
                        bf = LogBayesFile.load_nongr(path)
                    else:
                        bf = LogBayesFile.load(path.replace('(M)', mk))
                elif models_given[1]:
                    # logB for noise vs model (rather than model vs noise)
                    # should this be allowed?
                    mk = model_keys[1]
                    if mk.lower() in ['ngr', 'nongr']:
                        bf = LogBayesFile.load_nongr(path)
                    else:
                        bf = LogBayesFile.load(path.replace('(M)', mk))
                    bf.logB = - bf.logB
                else:
                    raise ValueError("must provide at least one model key.")
                d.append(bf.info)
            except IOError, e:
                warn("Did not find %r. Error: %s" % (path, e))
        return cls(d, origin=masterpath, model_keys=model_keys)

    @classmethod
    def collect_global(cls, globalpath, wc='*', wc_values=None):
        """
        Collect Bayes factors from paths matching :globalpath:.

        Attempts to load Bayes factor files output from ppe_nested. This will
        generally be a Bayes factor with respect to noise. The :reference:
        argument can be be a path pointing to a particular Bayes file; if this
        is provided, that file is loaded and subtracted from all values of
        self.logB, such that the stored Bayes factors are with respect to the
        reference: B^{some model}_{reference model}.


        Argument :globalpath: is a path pointing to the location of the files.
        Note it must point to files not just a directory. The string should
        contain a wildcard :wc:. If :wc_values: is provided, only paths with
        that match the global expression with a wildcard value in the list will
        be loaded.

        :param globalpath: path regexp to Bayes factors (str).
        :param wc: wildcard (str).
        :param wc_values: optional wildcard values (list).
        :param reference: optional path to reference Bayes file (str).
        """
        if not isinstance(globalpath, basestring):
            raise TypeError("globalpath must be a string, not %r"
                            % type(globalpath))
        info_list = []
        if wc_values is not None:
            # wc_values is list of strings
            for value in wc_values:
                bf = LogBayesFile.load(globalpath.replace(wc, str(value)))
                info_list.append(bf.logB)
        else:
            paths = glob.glob(globalpath.replace(wc, '*'))
            for path in paths:
                bf = LogBayesFile.load(path)
                info_list.append(bf.info)
        if len(info_list) == 0:
            raise IOError("Did not find any output files matching %r"
                          % globalpath)
        return cls(info_list, origin=globalpath)

    def export(self, path):
        """Save list of Bayes factors to disk.

        Format determined from extension (currently only supports ASCII).
        :param: path: path to destination file (str).
        """
        filename, extension = os.path.splitext(path)
        if '.hdf' in extension:
            raise NotImplementedError("no HDF5 support not yet.")
        else:
            np.savetxt(path, self, fmt="%.6f")

    def hist(self, *args, **kwargs):
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        n, bins, _ = ax.hist(self, *args, **kwargs)
        return fig, ax, n, bins

    def append(self, value):
        """Appends :value: to array (not in-place).

        :param value number or array (array_like)
        """
        return InfoArray(np.append(self, value))

    def append_from_file(self, path, **kwargs):
        if os.path.exists(path):
            value = LogBayesFile.load(path, **kwargs).info
        else:
            value = LogBayesArray.collect_global(path, **kwargs).info
        return self.append(value)

    def gkde(self, value, *args, **kwargs):
        """Returns the gkd interpolation of the value.

        This assumes tha logB values in the array represent some sort of PDF
        distribution which is approximated via gkd to return the inreporlated
        (or extrapolated) value

        :returns float
        """
        return gaussian_kde(self, *args, **kwargs)(value)

    def fit_dist(self, name, *args, **kwargs):
        dist = stats.distribution(name)
        param = dist.fit(self, *args, **kwargs)
        return param

    def plot_fit_dist(self, fit_dist, *args, **kwargs):
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        fit_kwargs = kwargs.pop('fit_kwargs', {})
        x = kwargs.pop('x', np.linspace(min(self), max(self), 100))
        dist = stats.distribution(fit_dist)
        fitpar = self.fit_dist(fit_dist, **fit_kwargs)
        fitpdf = dist.pdf(x, *fitpar[:-2], loc=fitpar[-2], scale=fitpar[-1])
        scale = kwargs.pop('scale', False)
        if scale:
            fitpdf *= float(len(self))
        ax.plot(x, fitpdf, *args, **kwargs)
        return fig, ax


# cf: lscsoft/src/lalsuite/lalapps/src/pulsar/HeterodyneSearch/pulsarpputils.py
class Prior(object):
    """ Read and write ppe prior files.

    As an example, the contents of a prior file would look like:
        C22 uniform 0 1e-21
        psi uniform -0.785398163397448 0.785398163397448
        phi22 uniform 0 6.283185307179586
        cosiota uniform -1 1
    """

    def __init__(self, model_key="GR"):
        self.model = basic.Model(model_key)
        self.params = {}
        for param in self.model.params:
            self.params[param] = None
        if "IOTA" in self.params:
            self.params["COSIOTA"] = None
        if "COSIOTA" in self.params:
            self.params["IOTA"] = None

    def add(self, name, prior, arg1, arg2):
        """Add prior for a parameter.

        :param name: parameter name (str).
        :param prior: prior type, e.g. 'uniform' (str).
        :param arg1: parameter lower bound (float).
        :param arg2: parameter upper bound (float).
        """
        if name.upper() not in self.params.keys():
            raise KeyError("invalid parameter %r for model %s" %
                           (name, self.model.key))
        if not isinstance(prior, basestring):
            raise TypeError("prior type must be a string (e.g. 'uniform').")
        if prior == 'fermidirac_cdf':
            # assume :arg1: is CDF value (usually 0.95) and :arg2: the
            # corresponding value of the variable named :name:.
            # Note that currently cannot pass mufrac argument.
            sigma, r = stats.fermidirac_sigma_r(arg2, cdf=float(arg1))
            self.params[name.upper()] = ('fermidirac', float(sigma), float(r))
        elif prior == 'gmm':
            self.params[name.upper()] = (prior, int(arg1), arg2)
        else:
            self.params[name.upper()] = (prior, float(arg1), float(arg2))

    def add_fermidirac_from_asd(self, name, f, paths, frompsd=False, **kwargs):
        """Add fermidirac prior based on h95 computed from spectra in disk.

        Duration must be indicated somehow in kwargs, either through duration
        or runs/ifos arguments (see implementation of basic.SpectrumList).
        """
        if not basic.Parameter(name, 0).isamplitude:
            raise ValueError("Upper limit can only be estimated from ASD for "
                             "amplitude parameters, not %r." % name)
        spectra = basic.SpectrumList.load(paths, **kwargs)
        h95 = spectra.h95ul(f, ispsd=frompsd)
        if name.upper() == 'C22':
            h95 = 0.5*h95
        self.add(name, 'fermidirac_cdf', 0.95, h95)

    def write(self, path, params=None):
        """Write prior file to disk.

        :param path: destination path (str).
        :param params: optional list of parameters to write (list of strings).
        """
        params = params or self.params.keys()
        with open(path, 'w') as f:
            for k in params:
                if k in self.params:
                    v = self.params[k]
                    if v is not None:
                        line = "%s %s %r %r\n" % (k, v[0], v[1], v[2])
                        line = line.replace("'", "")
                        f.write(line)

    def __getitem__(self, item):
        return self.params[item]

    def keys(self):
        return self.params.keys()

    @classmethod
    def read(cls, path, model_key="GR"):
        """Read prior file from disk.

        :param path: input path (str).
        """
        new = cls(model_key=model_key)
        paramsadded = 0
        with open(path, 'r') as f:
            for line in f:
                if line[0] in ['#', ';', '/']:
                    break
                contents = line.split(' ')
                if len(contents) != 4:
                    break
                elif contents[0] in new.params.keys():
                    new.add(contents[0], contents[1], float(contents[2]),
                            float(contents[3]))
                    paramsadded += 1
        if paramsadded == 0:
            raise IOError("invalid prior file.")
        else:
            return new


# cf: lscsoft/src/lalsuite/lalapps/src/pulsar/HeterodyneSearch/pulsarpputils.py
class PulsarPar(object):
    """ Contains data from TEMPO-style parameter file for a source.

    Example:
        RAJ 10:11:12.5643
        DECJ -21:45:54.7855
        F0 100.0
        F1 1e-15
        PEPOCH 54000.0

    May be created by providing parameters as kwargs or by loading an existing
    file.
    """

    def __init__(self, *args, **kwargs):
        self.params = {}
        self.filepath = kwargs.pop('filepath', None)
        if len(args) == 1:
            # assume input is istelf a `PulsarPar` object
            input_par = args[0]
            for key in input_par.keys():
                self.add(key, input_par[key])
        for key, value in kwargs.iteritems():
            self.add(key, value)

    def add(self, key, value):
        if isinstance(value, basic.Parameter):
            param = value
        else:
            param = basic.Parameter(key, value)
        self.params[param.key] = param

    def __getitem__(self, item):
        return self.params[item]

    def keys(self):
        return self.params.keys()

    @classmethod
    def read(cls, path):
        """Load TEMPO-style pulsar parameter ASCII file.

        :rtype new: PulsarPar
        :param path: path to input file (str).
        """
        new = cls()
        paramsadded = 0
        with open(path, 'r') as f:
            for line in f:
                if line[0] not in ['#', ';', '/']:
                    contents = line.split()
                    if not (len(contents) > 4 or len(contents) < 2):
                        try:
                            new.add(contents[0], contents[1])
                            if len(contents) > 2:
                                new.add("%s_ERR" % contents[0], contents[-1])
                            paramsadded += 1
                        except ValueError:
                            warn("ignoring invalid parameter %r" % contents[0])
        if paramsadded == 0:
            raise IOError("invalid prior file.")
        else:
            new.filepath = path
            return new

    def write(self, path, params=('RAJ', 'DECJ', 'F0', 'F1', 'PEPOCH')):
        """Write TEMPO-style pulsar parameter ASCII file.

        :param path: path to destination file (str).
        :param params: parameters to be included in file (str or tuple).
        """
        if basic.ismodel(params):
            params = basic.Model(params).params
        elif params == 'all':
            params = self.keys()
        elif params == 'not_err':
            params = [k for k in self.keys() if 'ERR' not in k]
        else:
            pass

        with open(path, 'w') as f:
            for par in params:
                par = par.upper()
                if par in self.params:
                    valuestr = str(self[par])
                    f.write("%s %s\n" % (par, str(valuestr)))


class PulsarParDict(OrderedDict):
    """Contains several `PulsarPar` objects labeled by PSRJ."""
    def __init__(self, input_data, **kwargs):
        if isinstance(input_data, dict):
            # check contents are Results objects
            for item in input_data.items():
                if not isinstance(item, PulsarPar):
                    warn("not all contents are PulsarPar objects!")
        super(PulsarParDict, self).__init__(input_data)

    @classmethod
    def collect_global(cls, globalpath, wc='*', wc_values=None):
        """ Collect PARs from paths matching :globalpath:.

        Argument `globalpath` is a path pointing to the location of the files.
        Note it must point to files not just a directory. The string should
        contain a wildcard `wc`. If `wc_values` is provided, only paths with
        that match the global expression with a wildcard value in the list will
        be loaded.

        Arguments
        ---------
        globalpath: str
            path regexp to Bayes factors.
        wc: str
            wildcard (string to be replaced).
        wc_values: list
            optional wildcard values.
        """
        if not isinstance(globalpath, basestring):
            raise TypeError("globalpath must be a string, not %r"
                            % type(globalpath))
        par_dict = {}
        if wc_values is not None:
            # wc_values is list of strings
            for value in wc_values:
                par = PulsarPar.read(globalpath.replace(wc, str(value)))
                par_dict[str(par['PSRJ'])] = par
        else:
            paths = glob.glob(globalpath.replace(wc, '*'))
            for path in paths:
                par = PulsarPar.read(path)
                par_dict[str(par['PSRJ'])] = par
        if not par_dict:
            raise IOError("Did not find any PAR files matching %r"
                          % globalpath)
        return cls(par_dict)


class PulsarParArray(object):
    def __init__(self, contents=None, model_key=None):
        """Array of PulsarPar objects.

        Includes import, export and manipulation methods.
        :param contents: list or array of :PulsarPar: objects
        """
        if isinstance(contents, PulsarParArray):
            self.contents = deepcopy(contents.contents)
            model_key = model_key or contents.model.key
        else:
            # check input type before adding
            if isinstance(contents, basestring):
                raise ValueError("invalid parameter type %r" % type(contents))
            else:
                try:
                    contents[0]
                    if isinstance(contents, type(self)):
                        self.contents = np.array(contents.contents)
                    else:
                        self.contents = np.array(contents)
                except TypeError:
                    raise ValueError("invalid parameter type %r" %
                                     type(contents))
        self.model = basic.Model(model_key)

    def __len__(self):
        return len(self.contents)

    def __getitem__(self, item):
        return self.contents[item]

    def _get_heff(self):
        """Returns an array with the effective injection strength as defined
        by self.model. Designed for internal use, cf. self.getparam().
        """
        paramlist = []
        for parfile in self.contents:
            heff_args = [float(parfile.params[p]) for p in
                         self.model.heff_args]
            paramlist.append(self.model.heff(*heff_args))
        return np.array(paramlist)

    @classmethod
    def collect(cls, masterpath, nf=None, n0=0, model_key=None):
        """Load series of pulsar parameter ASCII files from disk.

        The argument :masterpath: is path pointing to the location of the files
        Note it must point to files, not just a directory. The string should
        contain a "wildcard" (N) to be replaced by an integer; this will be
        used to iterate over a range from :n0: to :nf:-1.

        :param masterpath: path mask to input files (str).
        :param nf: maximum value of (N) to load (int).
        :param n0: minimum value of (N) to load (int).
        :param model_key: value to replace (M) wildcard (str).
        """
        if '(N)' not in masterpath:
            raise ValueError("path mask lacks (N) wildcard: %r" % masterpath)
        masterpath = os.path.abspath(masterpath)

        if isinstance(model_key, basestring):
            masterpath = masterpath.replace('(M)', model_key)
        elif model_key is not None:
            raise ValueError("invalid model %r." % model_key)

        if not nf:
            warn("guessing number of instantiations from directory.")
            path = masterpath.replace('(N)', '*')
            nf = len(glob.glob(path))
            if nf == 0:
                raise IOError("no files found matching %r." % path)

        pars = []
        for n in range(n0, nf + 1):
            path = masterpath.replace('(N)', str(n))
            pars.append(PulsarPar.read(path))
        return cls(contents=pars, model_key=model_key)

    @classmethod
    def collect_global(cls, globalpath, wc='*', wc_values=None,
                       model_key=None):
        """ Collect PARs from paths matching :globalpath:.

        Argument :globalpath: is a path pointing to the location of the files.
        Note it must point to files not just a directory. The string should
        contain a wildcard :wc:. If :wc_values: is provided, only paths with
        that match the global expression with a wildcard value in the list will
        be loaded.

        Arguments
        ---------
        globalpath: str
            path regexp to Bayes factors.
        wc: str
            wildcard (string to be replaced).
        wc_values: list
            optional wildcard values.
        model_key: str
            key of model for injection PAR files if applicable (optional).
        """
        if not isinstance(globalpath, basestring):
            raise TypeError("globalpath must be a string, not %r"
                            % type(globalpath))
        par_list = []
        if wc_values is not None:
            # wc_values is list of strings
            for value in wc_values:
                par = PulsarPar.read(globalpath.replace(wc, str(value)))
                par_list.append(par)
        else:
            paths = glob.glob(globalpath.replace(wc, '*'))
            for path in paths:
                par = PulsarPar.read(path)
                par_list.append(par)
        if len(par_list) == 0:
            raise IOError("Did not find any PAR files matching %r"
                          % globalpath)
        return cls(contents=par_list, model_key=model_key)

    def getparam(self, key, sort_by=None, return_object=False):
        """Return array with requested parameter for each file in contents.

        By default, returns array with actual values (float or str) of the
        requested parameter. If :return_object: is True, instead returns an a
        array of Parameter objects, rather than the actual values of the
        parameter.

        If a key is passed to the :sort_by: option, the output will be sorted
        by this second parameter and a second array :sortlist: will be returned
        containing the array of parameters used to sort the first. By default,
        however, the array will be in the same order as the Parameter objects
        are stored in the :self.contents: array.

        The special key 'heff' can be used to request the effective signal
        strength for a given model. This uses the amplitude and orientation
        parameters to compute the "effective SNR" (or absolute strain
        amplitude), as defined for a particular model in basic.Model.

        :param key: parameter key (str).
        :param sort_by: parameter key to sort the first array by (str).
        :param return_object: return Parameter object, default False (bool).
        :return paramlist: np.array.
        """
        key = key.upper()
        keys = self.contents[0].keys()
        if key in keys:
            if return_object:
                paramlist = np.array([parfile[key] for parfile
                                      in self.contents])
            else:
                paramlist = np.array([parfile.params[key].value for parfile
                                      in self.contents])
        elif key == 'HEFF':
            paramlist = self._get_heff()
        else:
            # cover cases like 'HVECTOR' or 'HGR'
            argkeys, func = basic.translate_parameter(key, origin=keys)
            args = [np.array([parfile.params[k].value for parfile in
                              self.contents]) for k in argkeys]
            paramlist = func(*args)

        if isinstance(sort_by, basestring):
            # note the following will fail if the sort_by key corresponds to
            # a non-sortable parameter, like "PSR", because it lacks a float
            # representation.
            if key in keys:
                if return_object:
                    sortlist = np.array([parfile[key] for parfile
                                          in self.contents])
                else:
                    sortlist = np.array([parfile.params[key].value for parfile
                                          in self.contents])
            elif key == 'HEFF':
                sortlist = self._get_heff()
            else:
                # cover cases like 'HVECTOR' or 'HGR'
                argkeys, func = basic.translate_parameter(key, origin=keys)
                args = [np.array([parfile.params[k].value for parfile in
                                  self.contents]) for k in argkeys]
                sortlist = func(*args)
            sorted_bundle = zip(*sorted(zip(sortlist, paramlist)))
            sortlist = np.array(sorted_bundle[0])
            paramlist = np.array(sorted_bundle[1])

            return paramlist, sortlist
        else:
            return paramlist

    def setparam(self, key, value):
        """Set parameter for all PulsarPar objects.
        """
        if isinstance(value, (list, tuple)):
            if len(value) != len(self.contents):
                raise ValueError("not enough values for all PAR files.")
        else:
            value = [value for n in self.contents]
        for n in range(len(value)):
            self.contents[n].add(key, value[n])

    def export(self, masterpath, params='all'):
        """Save injection PAR files to disk.

        The argument :masterpath: must include the wildcard (N) to be replaced
        by the instantiation number. It may also include the wildcard (M) to be
        replaced by the model name, but this is not required.

        The argument :params: indicates which parameters to include in each
        file. It can be a list of strings with parameter keys. Otherwise, it
        can be a key string for one of the models (e.g. GR) to include only the
        associated parameters, as defined in the :basic.Models: object. If this
        is instead 'all', all parameters will be included (default).

        :param masterpath: path mask for output files (str).
        :param params: parameters to include in file (str or list of strings).
        """
        masterpath = os.path.abspath(masterpath.replace('(M)', self.model.key))
        for n in range(len(self.contents)):
            path = masterpath.replace('(N)', str(n))
            self.contents[n].write(path, params=params)

    def get_psr(self, psrj):
        """ Parameter files that correspond to a given pulsar."""
        parfiles = []
        for parfile in self:
            if str(parfile['PSRJ']) == psrj.upper():
                parfiles.append(parfile)
        if len(parfiles) == 1:
            return parfiles[0]
        else:
            return PulsarParArray(contents=parfiles)

    def get_constraint_bool(self, constraint_dict):
        """ Returns boolean array marking the contents that satisfy all of
        the constraints passed in `constraint_dict`.

        Parameters
        ----------
        constraint_dict: dict
            dictionary containing constraints on injection parameters

        Returns
        -------
        constraint: bool array
            boolean index array
        """
        import pandas as pd
        param_df = pd.DataFrame({})
        pmin_df = pd.DataFrame({})
        pmax_df = pd.DataFrame({})
        for pkey, plim in constraint_dict.iteritems():
            plims = []
            for pl in plim:
                if isinstance(pl, basestring):
                    lkey_scale = pl.split('_times_')
                    lkey = lkey_scale[0]
                    if len(lkey_scale) == 1:
                        scale = 1
                    elif len(lkey_scale) == 2:
                        scale = float(lkey_scale[1])
                    else:
                        raise ValueError("invalid constraint "
                                         " format: %r" % pl)
                    plims.append(scale * np.array(self.getparam(lkey)))
                else:
                    plims.append([pl] * len(self))
            pmin_df[pkey] = plims[0]
            pmax_df[pkey] = plims[1]
            param_df[pkey] = self.getparam(pkey)
        # produce boolean Series marking values that satisfy all constraints
        if not param_df.empty:
            cnstr_df = (pmin_df < param_df) & (param_df < pmax_df)
            constraint = cnstr_df.apply(all, axis=1).values
        else:
            constraint = np.array([True]*len(self))
        return constraint


class InjectionParArray(PulsarParArray):
    @classmethod
    def from_model(cls, model_key, random_seed=2, pulsarpar=None,
                   parameters=None, **kwargs):
        """Creates a PulsarParArray object based on model key and parameters.

        Parameters can be provided inside a PulsarPar object by using the
        keyword argument 'pulsarpar'. Otherwise, they can be given as
        independent keyword arguments. The minimum parameters required
        are defined in the basic.Model class, together with other template
        properties. Note that if both methods are used, parameters in the
        PulsarPar object take precedence.

        Free parameters can be provided as keyword arguments in the format:

            param_name = (sampler_name, min_value, max_value)

        this will determine the range used for injection. The default range,
        as defined in the :basic.Parameter: object, can be used as well:

            param_name = "default"

        The first element in the tuple should be a string indicating how to
        sample the range for this parameter. Valid values are those accepted
        by `basic.ParameterArray.collect()`

        Arguments
        ---------
        model_key: str
            template model name, e.g. 'GR'.
        random_seed: float
            seed for random number generator.
        pulsarpar: PulsarPar
            object containing fixed parameters for injection.
        parameters: dict
            optional dictionary containing pulsar parameter ranges
            (note that, if this is used, kwargs are ignored).
        kwargs: dict
            parameters to range over in the format:
            param_name = (min_value, max_value)
            alternative

        Returns
        -------
        pararray: InjectionParArray
        """
        model = basic.Model(model_key)

        # check fixed parameters
        if isinstance(pulsarpar, PulsarPar):
            templatepar = pulsarpar
        elif pulsarpar is None:
            templatepar = PulsarPar()
        else:
            raise ValueError("invalid pulsarpar type: %r" % type(pulsarpar))

        # check parameters
        if parameters is not None:
            input_params = parameters
            if not isinstance(input_params, dict):
                raise ValueError("'parameters' must be dict, not %r"
                                 % type(input_params))
        else:
            input_params = kwargs

        param_ranges = {}
        sampler_names = {}
        for p in model.params:
            if p in templatepar.keys():
                # parameter in PulsarPar already
                pass
            else:
                # parameter not in PulsarPar, check keyword arguments
                if p in input_params:
                    param = input_params.pop(p)
                    if isinstance(param, tuple) or isinstance(param, list) \
                            and len(param) == 3:
                        # a range was given, e.g. ('linspace', '0', '6')
                        sampler_names[p] = str(param[0])
                        param_ranges[p] = (float(param[1]), float(param[2]))
                    elif param == 'default':
                        default_range = basic.Parameter(p, 0).range
                        if default_range is None:
                            raise ValueError("no default range for %r" % p)
                        else:
                            param_ranges[p] = default_range
                    else:
                        # assume a definite value was given
                        # if not, following line will fail
                        templatepar.add(p, param)
                elif p:
                    raise ValueError("must provide %r" % p)

        # check injection options (namely, number of injections)
        if "options" in kwargs:
            input_options = kwargs.pop("options")
            if not isinstance(input_options, dict):
                raise ValueError("'options' must be dict, not %r"
                                 % type(input_options))
        else:
            input_options = kwargs
        inj_options = {}
        for op in model.inj_numbers:
            if op in input_options:
                inj_options[op] = input_options.pop(op)
            elif 'n' in input_options:
                inj_options['n'] = int(input_options['n'])
            else:
                raise ValueError("must provide injection option %r" % op)
        # remove 'n' if it was present
        input_options.pop('n', None)

        # check if there are any extra parameters
        for key, value in kwargs.iteritems():
            if key in templatepar.keys():
                warn("%r specified twice, using PulsarPar value" % key)
            else:
                if isinstance(value, tuple) and len(value) == 2:
                    basic.Parameter(key, 0)  # makes sure parameter is valid
                    param_ranges[key] = value
                else:
                    templatepar.add(key, value)

        # -- CREATE INJECTION PARAMETERS --
        # The following is designed to work with all models based on the
        # _INJECTION_NUMBERS_AMPLITUDES dictionary defined in basic.Model.
        # The total number of injections is given by the number assigned to
        # each amplitude parameter, e.g. for scalar-tensor, then ntot = n0*nb:
        # >>> from . import basic
        # >>> model = basic.Model('ST')
        # >>> model.inject_numbers_dict
        # {'H0': 'n0', 'HSCALARB': 'nb'}
        random.seed(random_seed)
        ntot = model.total_injection_number(inj_options)
        param_values = OrderedDict()
        # The structure of param_values will be:
        #  param_values = {
        #       'H0': basic.ParameterArray([H0[0], H0[1], ...]),
        #       'PHI0': basic.ParameterArray([PHI0[0], PHI0[1],...], ... ]),
        #       ...
        # }
        flat_amplitudes = 'n' in inj_options
        for p in model.params:
            if p in model.amplitudes:
                if model.inj_numbers_dict[p] in inj_options:
                    # e.g. size = n0
                    size = int(inj_options[model.inj_numbers_dict[p]])
                elif flat_amplitudes:
                    # assume same number for all amplitudes
                    # (and no tensor product)
                    size = int(inj_options['n'])
                else:
                    raise ValueError('Cannot determine injection numbers.')
            elif all([ni in inj_options for ni in model.inj_numbers]):
                # e.g. size = [n0, nb]
                size = [int(inj_options[ni]) for ni in model.inj_numbers]
            elif flat_amplitudes:
                size = int(inj_options['n'])
            else:
                raise ValueError('Cannot determine injection numbers.')
            if p in templatepar.keys():
                # same value of this parameter for all injections
                param_values[p] = np.ones(size) * templatepar[p].value
            else:
                # array of values of this parameter built from sampler
                param_values[p] = basic.ParameterArray.construct(
                    sampler_names[p], param_ranges[p][0],
                    param_ranges[p][1], size, key=p, seed=None)
        # amp_matrices contains a list of np.ndarrays with shapes:
        # [ [ H0[0] ]*nb, ..., [ H0[n0-1] ]*nb]
        # and
        # [ [ HB[0], ..., HB[nb-1] ]*n0 ]
        if not flat_amplitudes:
            amp_matrices = basic.meshgrid(*[param_values[amp] for amp in
                                            model.amplitudes], indexing='ij')
            for amp, matrix in zip(model.amplitudes, amp_matrices):
                param_values[amp] = matrix
        pulsarpars = []
        # Stack np.ndarrays and loop over sets of values for all parameters
        stack = basic.stack(param_values.values(), axis=-1)
        for values_instance in stack.reshape(ntot, len(model.params)):
            # values_instance = [H0[0], HSCALARB[0], PHI0[0]]
            par = deepcopy(templatepar)
            for p, value in zip(param_values.keys(), values_instance):
                # p = 'H0', value = H0[0]
                if p not in par.keys():
                    par.add(p, value)
            pulsarpars.append(par)
        return cls(contents=pulsarpars, model_key=model.key)


class AnalysisJob(object):
    """Holds arguments to run a single pulsar_parameter_estimation_nested job.

    Internally stores the different options to be passed to the lalapps command
    and can produce a command string to be executed.

    Sample output command:
        lalapps_pulsar_parameter_estimation_nested --par-file pulsar.par
        --input-files data.txt --outfile output.txt --non-fixed-only
        --prior-file prior.txt --Nlive 1000 --kDTree 0 --diffev 0
        --covariance 0 --ensembleStretch 1 --ensembleWalk 1 --detectors H1
    """
    # flags obtained from ppe_nested --help
    _validkeys = [
        "verbose",
        "detectors",
        "par-file",
        "cor-file",
        "input-files",
        "sample-interval",
        "outfile",
        "non-fixed-only",
        "gzip",
        "outXML",
        "chunk-min",
        "chunk-max",
        "time-bins",
        "prior-file",
        "ephem-earth",
        "ephem-sun",
        "ephem-timecorr",
        "harmonics",
        "biaxial",
        "gaussian-like",
        "randomise",
        # Nested sampling parameters:
        "Nlive",
        "Nmcmc",
        "Nmcmcinitial",
        "Nruns",
        "tolerance",
        "randomseed",
        # MCMC proposal parameters:
        "covariance",
        "temperature",
        "kDTree",
        "kDNCell",
        "kDUpdateFactor",
        "diffev",
        "freqBinJump",
        "ensembleStretch",
        "ensembleWalk",
        # Reduced order quadrature parameters:
        "roq",
        "ntraining",
        "roq-tolerance",
        "test-basis",
        "output-weights",
        "input-weights",
        # Signal injection parameters:
        "inject-file",
        "inject-output",
        "fake-data",
        "fake-psd",
        "fake-starts",
        "fake-lengths",
        "fake-dt",
        "scale-snr",
        # Flags for using a Nested sampling file as a prior:
        "sample-files",
        "sample-nlives",
        "prior-cell",
        "Npost",
        # Legacy code flags:
        "oldChunks",
        "jones-model",
        # Benchmarking:
        "time-it",
        "sampleprior",
        # Non-GR:
        "nonGR",
        "inject-nonGR",
        "reheterodyne"
    ]

    _executable = "lalapps_pulsar_parameter_estimation_nested"

    def __init__(self, defaults=True):
        if not defaults:
            self.arguments = {}
        else:
            # set default options
            self.arguments = {
                "par-file": "",
                "prior-file": "",
                "outfile": "",
                "non-fixed-only": "",
                "Nlive": "1000",
                #"kDTree": "0",
                #"diffev": "0",
                #"covariance": "0",
                #"ensembleStretch": "1",
                #"ensembleWalk": "1"
            }
        # intermediate container for input paths for different harmonics
        self.input_paths_harm_dict = {'1': {}, '2': {}}

    def add(self, key, value=''):
        """Add a property to arguments registry.

        :param key: property name (str).
        :param value: property value (str).
        """
        key = key.replace('_', '-')
        lower_dict = {}
        # create dictionary to recognize option regardless of capitalization
        for k in self._validkeys:
            lower_dict[k.lower()] = k

        if key.lower() not in lower_dict:
            raise ValueError("invalid PPE argument %r" % key)
        else:
            key = lower_dict[key.lower()]

        if key in ("detectors", "input-files", "fake-starts", "fake-lengths", "fake-data", "fake-psd", "fake-dt", "harmonics"):
            try:
                self.arguments[key] += ",%s" % str(value)
            except KeyError:
                self.arguments[key] = str(value)
        else:
            self.arguments[key] = str(value)
        self.arguments[key] = self.arguments[key].strip().strip(',')
    
    def add_input(self, path, ifo, harmonic):
        """ Dedicated function to add input paths for different harmonics.

        :param path: path to data
        :param ifo: detector key (e.g. 'H1')
        :param harmonic: rotational frequency harmonic (1 or 2)
        """
        self.input_paths_harm_dict[str(harmonic)][ifo.upper()] = path

    @classmethod
    def from_model(cls, search='GR', inject='GR', **kwargs):
        """Create PPEjob from model defaults.

        :param search: search model (str).
        :param inject: inject model (str).
        """
        job = cls(**kwargs)
        if search != "GR":
            if search == "nonGR":
                # will search over all polarizations
                search = ''
            job.add("nonGR", search)
        if inject != "GR":
            if inject == "nonGR":
                # will inject arbitrary combination of polarizations
                inject = ''
            job.add("inject-nonGR", inject)
        return job

    def _get_harmonic_input_data(self):
        """ Form 'input-files' option from harmonics dict."""
        value = ''
        if 'harmonics' in self.arguments:
            harmonics = self['harmonics'].split(',')
            detectors = self['detectors'].split(',')
            for d in detectors:
                for h in harmonics:
                    value += ',%s' % self.input_paths_harm_dict[h][d]
        return value.strip(',')

    def __getitem__(self, item):
        return self.arguments[item]

    def __str__(self):
        """Return option string.
        """
        # get correct input files
        input_files = self._get_harmonic_input_data()
        if input_files:
            self.arguments['input-files'] = input_files
        # form string
        command = ''
        for key, value in self.arguments.iteritems():
            s = "--%s %s " % (key, value)
            command += s.replace('  ', ' ')
        return command.strip()

    def command(self):
        """Return command string, including all options.
        """
        return "%s %s" % (self._executable, str(self))


class HeterodyneJob(object):
    """Holds arguments to run a single `lalapps_heterodyne_pulsar` job.

    Internally stores the different options to be passed to the lalapps command
    and can produce a command string to be executed.

    Sample output command:
    """
    # flags obtained from `heterodyne_pulsar --help`
    _validkeys = [
        "verbose",
        "ifo",
        "pulsar",
        "heterodyne-flag",
        "param-file",
        "param-file-update",
        "manual-epoch",
        "ephem-earth-file",
        "ephem-sun-file",
        "ephem-time-file",
        "filter-knee",
        "sample-rate",
        "resample-rate",
        "data-file",
        "channel",
        "output-file",
        "seg-file",
        "calibrate",
        "response-file",
        "coefficient-file",
        "sensing-function",
        "open-loop-gain",
        "stddev-thresh",
        "freq-factor",
        "scale-factor",
        "high-pass-freq",
        "binary-input",
        "binary-output",
        "gzip-output"
    ]

    _executable = "lalapps_heterodyne_pulsar"

    def __init__(self, defaults=True):
        if not defaults:
            self.arguments = {}
        else:
            # set default options
            self.arguments = {
                'heterodyne-flag': '1',
                'filter-knee': '0.25',
                'sample-rate': '1',
                'resample-rate': '1/60',
                'binary-input': '',
                'ephem-earth-file': '(LALPULSAR_DATADIR)/'
                                    'earth00-19-DE405.dat.gz',
                'ephem-sun-file': '(LALPULSAR_DATADIR)/sun00-19-DE405.dat.gz',
                'ephem-time-file': '(LALPULSAR_DATADIR)/te405_2000-2019.dat.gz'
            }
        data_dir = os.getenv('LALPULSAR_DATADIR', '')
        for key in [k for k in self.arguments.keys() if 'ephem' in k]:
            self.arguments[key] = self.arguments[key].replace(
                '(LALPULSAR_DATADIR)', data_dir)

    def add(self, key, value=''):
        """Add a property to arguments registry.

        :param key: property name (str).
        :param value: property value (str).
        """
        key = key.replace('_', '-')
        lower_dict = {}
        # create dictionary to recognize option regardless of capitalization
        for k in self._validkeys:
            lower_dict[k.lower()] = k

        if key.lower() not in lower_dict:
            raise ValueError("invalid `heterodyne_pulsar` argument %r" % key)
        else:
            key = lower_dict[key.lower()]
        self.arguments[key] = str(value)
        self.arguments[key] = self.arguments[key].strip().strip(',')

    def __getitem__(self, item):
        return self.arguments[item]

    def __str__(self):
        """Return option string.
        """
        command = ''
        for key, value in self.arguments.iteritems():
            s = "--%s %s " % (key, value)
            command += s.replace('  ', ' ')
        return command.strip()

    def command(self):
        """Return command string, including all options.
        """
        return "%s %s" % (self._executable, str(self))


class Results(object):
    """Contains results of a PPE run produced with seveal injections (B vs h0).

    The main attributes are: self.bayes (a BayesArray containing the Bayes
    factors for the requested models) and self.injections (a list of PulsarPar
    files containing the loaded injection information).
    """
    def __init__(self, bayes=None, injections=None, posteriors=None, par=None,
                 search_keys=(None, None), inject_key=None, info=None):
        if bayes is not None:
            self.bayes = LogBayesArray(bayes)
            skeys = [search_keys[0] or self.bayes.model_keys[0],
                     search_keys[1] or self.bayes.model_keys[1]]
        else:
            self.bayes = None
            skeys = search_keys
        self.search_models = basic.ModelList(skeys)
        if injections is not None:
            self.injections = PulsarParArray(injections)
            ikey = inject_key or self.injections.model.key
            self.inject_model = basic.Model(ikey)
        else:
            self.injections = None
            self.inject_model = basic.Model(inject_key)
        if posteriors is not None:
            self.posteriors = PosteriorSamplesDictArray(posteriors)
        else:
            self.posteriors = None
        if par is not None:
            self.pulsarpar = PulsarPar(par)
        else:
            self.pulsarpar = None
        if info is not None:
            self.info = InfoArray(info)
        else:
            self.info = None

    def __len__(self):
        if self.bayes is None:
            return 0
        else:
            return len(self.bayes)

    @classmethod
    def collect(cls, bpath, injpath=None, pospath=None, parpath=None, nf=None,
                n0=0, search_keys=(None, None), inject_key=None, nlive=None,
                posparams=None, load_info=False):
        """Collect Bayes factor results and injection files from disk.

        Will create a BayesArray using its own collect method with the path
        mask `bpath`. If `injpath` is provided, will also create a
        PulsarParArray using its own collect method with that path mask.

        The argument :search_models: must be a string or a string tuple/list,
        e.g. "GR", ("GR") or ("GR", "ST"). The string must correspond to a
        recognized model (cf. `basic.models`).

        The argument `inject_model` is metadata for record-keeping only.

        Arguments
        ---------
        bpath: str
            input path mask for Bayes factor ppe output files.
        injpath: str
            input path mask for injection parameter files.
        pospath: str
            path pointing to posterior files (ASCII).
        parpath: str
            path pointing to pulsar PAR file.
        nf: int
            maximum run index to be loaded.
        n0: int
            minimum run index to be loaded, default 0.
        search_keys: str, tuple
            models compared in search.
        inject_key: str
            model of injected signals, optional.
        nlive: int
            number of live points (only needed if loading posteriors).
        """
        # check model names
        if search_keys != (None, None):
            if isinstance(search_keys, basestring) and \
                    not basic.ismodel(search_keys):
                raise ValueError('invalid model %r' % search_keys)
            elif 0 < len(search_keys) < 3:
                for m in search_keys:
                    if not (basic.ismodel(m) or m.lower() in ['n', 'noise',
                                                              'ngr', 'nongr']):
                        raise ValueError('invalid model %r' % m)
            else:
                raise ValueError('invalid model keys %r' % search_keys)
        # load Bayes factors
        bayes = LogBayesArray.collect_n(bpath, nf=nf, n0=n0,
                                        model_keys=search_keys)
        # load info
        if load_info:
            info = InfoArray.collect_n(bpath, nf=nf, n0=n0,
                                       model_keys=search_keys)
        else:
            info = None
        # load injections
        if injpath:
            injpars = PulsarParArray.collect(injpath, nf=nf, n0=n0,
                                             model_key=inject_key)
        else:
            injpars = None
        # load posteriors
        if pospath:
            params = posparams or basic.Model(search_keys[0]).params
            try:
                posteriors = PosteriorSamplesDictArray.load_n(
                    pospath, nmin=n0, nmax=nf, params=params)
            except IOError:
                warn("Failed to load posteriors, attempting nested samples.")
                posteriors = PosteriorSamplesDictArray.load_nest_n(
                    pospath, nmin=n0, nmax=nf, nlive=nlive)
            if not posteriors:
                warn("Failed to load posteriors, attempting nested samples.")
                posteriors = PosteriorSamplesDictArray.load_nest_n(
                    pospath, nmin=n0, nmax=nf, nlive=nlive)
        else:
            posteriors = None
        # load PAR files
        if parpath:
            par = PulsarPar.read(parpath)
        else:
            par = None
        return cls(bayes=bayes, injections=injpars, search_keys=search_keys,
                   inject_key=inject_key, posteriors=posteriors, par=par,
                   info=info)

    def update_models(self, search_keys=(None, None), inject_key=None):
        """ Update models (useful when objected created from ResultsDict"""
        if search_keys != (None, None):
            self.search_models = basic.ModelList(search_keys)
        if inject_key is not None:
            self.inject_model = basic.Model(inject_key)

    def load_posteriors(self, pospath, nlive=None, nf=None, n0=0, params=[]):
        """Read posterior samples from disk.
        If fails, attempts to load nested samples."""
        nf = nf or len(self.bayes) - 1
        if params == 'all':
            params = []
        else:
            params = params or self.search_models[0].params
        try:
            self.posteriors = PosteriorSamplesDictArray.load_n(
                pospath, nmin=n0, nmax=nf, params=params)
        except IOError:
            warn("Failed to load posteriors, attempting nested samples.")
            self.posteriors = PosteriorSamplesDictArray.load_nest_n(
                pospath, nmin=n0, nmax=nf, nlive=nlive)
        if not self.posteriors:
            warn("Failed to load posteriors, attempting nested samples.")
            self.posteriors = PosteriorSamplesDictArray.load_nest_n(
                pospath, nmin=n0, nmax=nf, nlive=nlive)

    def _sqrtb(self):
        """Returns an array containing the square root of log Bayes factors.
        For negative values, the opposite of the square root of the absolute
        value is returned: \sg(logB) * \sqrt(\abs(logB))
        """
        signs = np.sign(self.bayes)
        sqrtb = signs * np.sqrt(np.abs(self.bayes))
        return sqrtb

    def newbase(self, base):
        """Update base of log Bayes array."""
        self.bayes = self.bayes.newbase(base)

    def getparam(self, paramkey):
        """Returns array of the requested injection parameter or log Bayes.

        Besides regular parameter keys, also accepts:
            'B', 'BAYES', 'LOGB'
            'SQRTB', 'SQRTBAYES', 'SQRTLOGB'
        :param paramkey: parameter name (str).
        """
        paramkey = paramkey.upper()
        hasinjections = self.inject_model.params is not None
        if (hasinjections and paramkey in self.inject_model.params)\
                or paramkey.upper() == 'HEFF':
            x = self.injections.getparam(paramkey)
        elif paramkey in ['B', 'BAYES', 'LOGB']:
            x = self.bayes
        elif paramkey in ['SQRTB', 'SQRTBAYES', 'SQRTLOGB']:
            x = self._sqrtb()
        elif paramkey == 'FGW':
            if hasinjections:
                x = 2.0 * self.injections.getparam('F0')
            else:
                x = 2.0 * float(self.pulsarpar['F0'])
        else:
            if hasinjections:
                injparams = self.inject_model.params
                if paramkey in injparams:
                    x = self.injections.getparam(paramkey)
                else:
                    # catch composite parameters like 'HVECTOR' or 'HGR'
                    argkeys, func = basic.translate_parameter(paramkey,
                                                              origin=injparams)
                    args = [self.injections.getparam(k) for k in argkeys]
                    x = func(*args)
            else:
                params = self.pulsarpar.keys()
                if paramkey in params:
                    x = float(self.pulsarpar[paramkey])
                else:
                    # catch composite parameters like 'HVECTOR' or 'HGR'
                    argkeys, func = basic.translate_parameter(paramkey,
                                                              origin=params)
                    args = [float(self.pulsarpar[k]) for k in argkeys]
                    x = func(*args)
        return np.array(x)

    def fold(self):
        """Combine Bayes factors assuming independent observations.

        Returns joint log Bayes factor produced by multiplynh Bayes factors
        stored in self.bayes (add the logs).

        :returns: BayesArray of single element.
        """
        return addb(self.bayes)

    def fit(self, xkey, ykey, deg=None, zero_intersect=False, **kwargs):
        """Polynomial fit of ykey vs xkey data.

        The argument :deg: determines the degree of the polynomial fit and is
        set to 1 if plotting sqrtb and 2 if plotting b by default.

        The argument :zero_intersect: indicates whether to force the
        y-intersect to be zero. If this is the case, numpy.linalg.lstsq is used
        instead of numpy.polyfit. This option is only available if deg=1.
        """
        if ykey.upper() in ['SQRTB', 'SQRTLOGB']:
            deg = deg or 1
        else:
            deg = deg or 2
        x = self.getparam(xkey)
        y = self.getparam(ykey)
        if zero_intersect is False:
            coeff = np.polyfit(x, y, deg, **kwargs)
        elif deg == 1:
            x_vertical = np.reshape(x, (len(x), 1))
            y_vertical = np.reshape(y, (len(y), 1))
            coeff, _, _, _ = np.linalg.lstsq(x_vertical, y_vertical)
            coeff = coeff[0][0]
        else:
            raise ValueError("unable to fix y-intersect except for deg=1")
        return coeff

    def plot(self, xkey, ykey, *args, **kwargs):
        """Produces 2D matplotlib plot of the quantities x and y.

        Accepts the kwarg 'default_style'. If true, it uses the standard LaTeX
        rendering. Extra arguments are passed to pyplot.plot().

        :param x: key of x-axis data (str).
        :param y: key of y-axis data (str).
        :returns fig: Matplotlib figure object.
        :returns ax: Matplotlib axis object.
        """
        default_style = kwargs.pop('default_style', True)
        # obtain data
        x = self.getparam(xkey)
        y = self.getparam(ykey)
        # plot
        if default_style:
            matplotlib.rcParams.update(basic.MPLPARAMS)
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        scatter = kwargs.pop('scatter', False)
        if scatter:
            im = ax.scatter(x, y, *args, **kwargs)
        else:
            ax.plot(x, y, *args, **kwargs)
        ax.set_xlabel(xkey)
        ax.set_ylabel(ykey)
        returns = [fig, ax]
        if scatter:
            returns.append(im)
        return returns

    def imshow(self, xkey, ykey, zkey, nrows=0, ncols=0, ratio=False,
               drop_zeros='x', **kwargs):
        # obtain data
        x = self.getparam(xkey)
        y = self.getparam(ykey)
        z = self.getparam(zkey)
        if drop_zeros:
            x_tmp = []
            y_tmp = []
            z_tmp = []
            for xi, yi, zi in zip(x, y, z):
                if ('x' in drop_zeros and xi != 0) or \
                        ('y' in drop_zeros and yi != 0):
                    x_tmp.append(xi)
                    y_tmp.append(yi)
                    z_tmp.append(zi)
            x = np.array(x_tmp)
            y = np.array(y_tmp)
            z = np.array(z_tmp)
        if ratio:
            y = y/x
        # reshape
        if not ncols:
            warn("guessing number of columns from x data.")
            ncols = len(set(x))
        if not nrows:
            warn("guessing number of rows from y data.")
            nrows = 0
            for yi in y:
                if yi == y[0]:
                    nrows += 1
        zgrid = z.reshape((nrows, ncols))
        # format defaults
        interp = kwargs.pop('interpolation', 'nearest')
        cmap = kwargs.pop('cmap', matplotlib.cm.cubehelix)
        # plot
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        ax.imshow(zgrid, extent=(x.min(), x.max(), y.max(), y.min()),
                  interpolation=interp, cmap=cmap, **kwargs)
        ax.set_xlabel(xkey)
        ax.set_ylabel(ykey)
        return fig, ax

    def scatter3d(self, xkey, ykey, zkey, ratio=False, drop_zeros='x',
                  **kwargs):
        elev = kwargs.pop('elev', None)
        rotn = kwargs.pop('rotn', None)
        default_style = kwargs.pop('default_style', True)
        if default_style:
            matplotlib.rcParams.update(basic.MPLPARAMS)
        x = self.getparam(xkey)
        y = self.getparam(ykey)
        z = self.getparam(zkey)
        if drop_zeros:
            x_tmp = []
            y_tmp = []
            z_tmp = []
            for xi, yi, zi in zip(x, y, z):
                if ('x' in drop_zeros and xi != 0) or \
                        ('y' in drop_zeros and yi != 0):
                    x_tmp.append(xi)
                    y_tmp.append(yi)
                    z_tmp.append(zi)
            x = np.array(x_tmp)
            y = np.array(y_tmp)
            z = np.array(z_tmp)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        if ratio:
            y = y/x
        ax.scatter(x, y, z, c=z)
        # specifc view
        if elev is not None and rotn is not None:
            ax.view_init(elev, rotn)
        return fig, ax

    def search_keys(self):
        return [model.key for model in self.search_models]

    def inject_key(self):
        return self.inject_model.key

    def posterior_percentiles(self, paramkey, *args, **kwargs):
        """Returns array of percentiles from posterior of parameter `paramkey`.
        """
        compute_error = kwargs.pop('compute_error', False)
        percentiles = []
        if not compute_error:
            nbins = kwargs.pop('nbins', 100)
            for posterior_dict in self.posteriors:
                posterior = posterior_dict[paramkey]
                q = posterior.percentile(*args, **kwargs)
                percentiles.append(q)
                # # TEST (alt way of computing UL)
                # hist, edges = np.histogram(posterior, nbins)
                # centres = [0.5*(low+high) for low, high in zip(edges[:-1], edges[1:])]
                # cs = np.cumsum(hist/float(len(posterior)))  # cumulative distribution
                # cl = 0.95
                # binid = np.abs(cs - cl).argmin()
            return np.array(percentiles)
        else:
            ul_mins = []
            ul_maxs = []
            for posterior_dict in self.posteriors:
                posterior = posterior_dict[paramkey]
                error = posterior.percentile_error(*args, **kwargs)
                ul_mins.append(error[0])
                percentiles.append(error[1])
                ul_maxs.append(error[2])
            return np.array(ul_mins), np.array(percentiles), np.array(ul_maxs)

    def apply_constraints(self, constraint_dict, bool_constraint=None):
        """ Returns Results object containing instantiations that satisfiy the
        constraints provided in dictionary. If no constraints, returns copy
        of self. Info and posteriors are lost. Uses Pandas DataFrame.

        Arguments
        ---------
        constraint_dict : dict
            dictionary containing constraints on injection parameters
        bool_constraint : boolean array
            custom boolean indexing to be applied on top of conditions from
            `constraint_dict`

        Returns
        -------
        results : Results
            results containing only instantiations satisfying all constraints.
        """
        if isinstance(self.injections, PulsarParArray):
            constraint = self.injections.get_constraint_bool(constraint_dict)
        else:
            # there are no injections
            constraint = np.array([True] * len(self.bayes))

        # combine with user defined boolean constraint
        if bool_constraint is not None:
            if len(bool_constraint) == len(self.bayes):
                constraint = np.logical_and(constraint, bool_constraint)
            else:
                raise ValueError("Invalid boolean constraint.")

        if not any(constraint):
            raise ValueError('no injections satisfying all constraints')

        value_dict = {k: deepcopy(getattr(self, k))
                      for k in ['bayes', 'posteriors', 'info']}
        for key, item in value_dict.iteritems():
            if item is not None and len(item)>0:
                value_dict[key] = item[constraint]
        value_dict['injections'] = self.injections.contents[constraint] if \
                                   self.injections is not None else None

        results = Results(search_keys=self.search_keys(), par=self.pulsarpar,
                          inject_key=self.inject_key(), **value_dict)
        return results

    def _apply_constraints(self, constraint_dict, bool_constraint=None):
        """ [DEPRECATED]
        Returns Results object containing instantiations that satisfiy the
        constraints provided in dictionary. If no constraints, returns copy
        of self. Info and posteriors are lost.

        DEPRECATED!

        Arguments
        ---------
        constraint_dict : dict
            dictionary containing constraints on injection parameters

        Returns
        -------
        results : Results
        """
        if bool_constraint is not None:
            warn("Boolean constraint not implemented for non-Pandas version of"
                 " this function. Ignoring argument.")
        results = self # deepcopy(self)
        valid_lnbs = deepcopy(results.bayes)
        valid_inj = deepcopy(results.injections)
        for pkey, plim in constraint_dict.iteritems():
            plims = []
            for pl in plim:
                if isinstance(pl, basestring):
                    lkey_scale = pl.split('_times_')
                    lkey = lkey_scale[0]
                    if len(lkey_scale) == 1:
                        scale = 1
                    elif len(lkey_scale) == 2:
                        scale = float(lkey_scale[1])
                    else:
                        raise ValueError("invalid constraint "
                                         " format: %r" % pl)
                    plims.append(scale * np.array(valid_inj.getparam(lkey)))
                else:
                    plims.append([pl] * len(valid_lnbs))
            pmins, pmaxs = plims
            ps = valid_inj.getparam(pkey)
            valid_lnbs = LogBayesArray([b for b, pmin, p, pmax
                                        in zip(valid_lnbs, pmins, ps, pmaxs)
                                        if pmin <= p <= pmax],
                                       model_keys=results.search_keys())
            if len(valid_lnbs) == 0:
                raise ValueError('no injections satisfying %s '
                                 'constraint.' % pkey)
            valid_inj = InjectionParArray([i for i, pmin, p, pmax
                                           in zip(valid_inj, pmins, ps, pmaxs)
                                           if pmin <= p <= pmax],
                                          model_key=results.inject_key())
        new_results = Results(bayes=valid_lnbs, injections=valid_inj,
                              par=results.pulsarpar)
        # results.bayes = valid_lnbs
        # results.injections = valid_inj
        # results.info = None
        # results.posteriors = None
        return new_results


class ResultsDict(OrderedDict):
    """OrderedDict containing series of results objects.
    """
    def __init__(self, input_data, **kwargs):
        sm_k = kwargs.pop("search_keys", (None, None))
        im_k = kwargs.pop("inject_key", None)
        if isinstance(input_data, ResultsDict):
            skeys = [sm_k[0] or input_data.search_keys[0],
                     sm_k[1] or input_data.search_keys[1]]
            im = basic.Model(im_k or input_data.inject_key)
            # check contents are Results objects
            for item in input_data.items():
                if not isinstance(item, Results):
                    warn("not all contents are Results objects!")
        else:
            skeys = sm_k
            im = basic.Model(im_k)
        self.search_models = basic.ModelList(skeys)
        self.search_keys = self.search_models.keys
        self.inject_model = im
        self.inject_key = im.key
        # initialize dict to store combined gkde estimators
        # (see self.combine_posteriors_gkde)
        self._gkde_dict = {}
        super(ResultsDict, self).__init__(input_data)

    @classmethod
    def collect(cls, bpath, wc="psrj", wc_values=None, inject_key=None,
                search_keys=(None, None), indep_inject=None, **kwargs):
        """Load set of PPE results from disk using wildcards (wc).

        The bayes factor and injection paths are expected to be of the form:
            bpath:   /some/dir/bdir/(wc)/b_filename_(M)_(N).ext
            injpath: /some/dir/injdir/(wc)/inj_filename_(M)_(N).ext

        The :bpath: argument may contain wildcards (M) for model and (N) for
        injection index as used by the ppe.Results.collect() method, plus a
        new optional wild card defined by the option :wc:. This could be PSRJ.

        :param bpath: path to out.txt_B.txt files (str).
        :param injpath: path to inject.par files (str).
        :param wc: wildcard string to be replaced in bpath and injpath (str).
        :param wc_values: optional values for :wc: (list of str).
        :param nf: maximum value of (N) to be loaded (int).
        :param n0: minimum value of (N) to be loaded (int).
        :param search_keys: tuple of strings.
        :param inject_key: str.
        """
        injpars = None
        master_bpath = os.path.abspath(bpath)
        master_optpaths = {pn: os.path.abspath(kwargs[pn]) for pn in
                           ['injpath', 'parpath', 'pospath'] if pn in kwargs
                           and kwargs[pn] is not None}
        if wc_values is None:
            warn("guessing wc values from directory names.")
            bp = basic.tokenize(master_bpath).split('(%s)' % wc.upper())[0]
            if os.path.isdir(bp):
                wc_values = [r for r in os.listdir(bp) if os.path.isdir(bp)]
            else:
                raise IOError("unable to access directory %r" % bp)
        if indep_inject is None:
            # set default behavior such that, if wildcard not in injection
            # name, assume all elements have same injections and load upfront
            injpath = kwargs.get('injpath', None)
            wc_kwargs = {wc: 'PlAcEhOlDeR'}
            indep_inject = (basic.tokenize(injpath, **wc_kwargs) != injpath)
        if not indep_inject:
            # assume all results have same injections and load upfront to
            # reduce IO operations
            injpath = master_optpaths.pop('injpath', None)
            kwargs['injpath'] = None
            nf = kwargs.get('nf', None)
            n0 = kwargs.get('n0', 0)
            if injpath is not None:
                injpars = PulsarParArray.collect(injpath, nf=nf, n0=n0,
                                                 model_key=inject_key)
        contents = {}
        for wc_value in wc_values:
            wc_kwargs = {wc: wc_value}
            bpath = basic.tokenize(master_bpath, **wc_kwargs)
            optpaths = {pn: basic.tokenize(p, **wc_kwargs) for pn, p in
                        master_optpaths.iteritems()}
            local_kwargs = kwargs.copy()
            local_kwargs.update(optpaths)
            results = Results.collect(bpath, search_keys=search_keys,
                                      inject_key=inject_key, **local_kwargs)
            if not indep_inject:
                results.injections = injpars
            contents[wc_value] = results
        return cls(contents, search_keys=search_keys, inject_key=inject_key)

    def newbase(self, base):
        """Modify base of log Bayes array
        """
        for results in self.itervalues():
            results.newbase(base)

    def add_from_disk(self, *args, **kwargs):
        """Adds elements to dictionary by calling self.collect()

        :param bpath: path to out.txt_B.txt files (str).
        :param injpath: path to inject.par files (str).
        :param wc: string to be replaced in bpath and injpath (str).
        :param wc_values: optional values for :wc: (list of str).
        :param nf: maximum value of (N) to be loaded (int).
        :param n0: minimum value of (N) to be loaded (int).
        :param search_keys: tuple of strings.
        :param inject_key: str.
        """
        new_res_dict = self.collect(*args, **kwargs)
        for key, results in new_res_dict.iteritems():
            self[key] = results

    def add(self, key, results, combine=False, preserve_injections=False,
            preserve_par=True):
        """Adds Results object to dictionary.

        The :combine: argument determines action for a preexisting key. If its
        value is 'add', the log Bayes factors of the old and new results
        objects will be added. If it is 'average', the Bayes factors will be
        combined using combineb(). Otherwise, the old element is replaced by
        the new one (default).

        If the entry already exists and `preserve_injections` is True, the
        updated entry will have the same injection array as the original one.
        Otherwise, a new injection array is created that only preserves signal
        amplitude or phase parameters, as well as those needed to recreate the
        effective strength.

        :param key: entry name (str).
        :param results: Results object to be stored (Results).
        :param combine: determines what to do if entry already exists (str).
        :param preserve_injections: whether to keep injection info (bool).
        """
        if preserve_par:
            pulsarpar = deepcopy(self[key].pulsarpar)
        else:
            pulsarpar = None
        if key in self.keys() and (combine in ['add', 'sum']):
            # proceeding as in self.fold()
            bayes_list = [self[key].bayes, results.bayes]
            new_b = addb(bayes_list, axis=0)
            injections = deepcopy(self[key].injections)
            if not preserve_injections:
                for parkey in self.inject_model.params:
                    par = basic.Parameter(parkey, 0)
                    inheff = parkey in self.inject_model.heff_args
                    if not (par.isamplitude or par.isphase or inheff):
                        injections.setparam(parkey, 0)
            new_results = Results(bayes=new_b, injections=injections,
                                  par=pulsarpar)
        elif key in self.keys() and (combine in ['average', 'avg']):
            bayes_list = [self[key].bayes, results.bayes]
            new_b = logavgexp_lnb(bayes_list)
            injections = deepcopy(self[key].injections)
            if not preserve_injections:
                for parkey in self.inject_model.params:
                    par = basic.Parameter(parkey, 0)
                    inheff = parkey in self.inject_model.heff_args
                    if not (par.isamplitude or par.isphase or inheff):
                        injections.setparam(parkey, 0)
            new_results = Results(bayes=new_b, injections=injections,
                                  par=pulsarpar)
        else:
            new_results = results
        self[key] = new_results

    def fold(self, full=False, blank_inj=True, combine='add', sub_keys=(),
             preserve_par=None):
        """Combine Bayes factors of all Results objects accross injections.

        WARNING: source information will not be inherited by output; signal
        amplitude and phase coefficients (including those required to
        reconstruct the effective strength) are inherited from the first
        Results() object.

        The above behavior is explained by the assumption that the different
        Results() objects might correspond to different sources. However, the
        injection parameters which are marginalized over are assumed to be the
        same throughout.

        If :full: is True, the Results object resulting from the folding across
        keys will itself be folded so that the Bayes factors it contains are
        combined into one and a float is returned. This is the same as:
            folded = self.fold()
            folded.fold()

        :param full: whether to combine Bayes factors across injections (bool).
        """
        # create list of BayesArrays objects to pass to np.sum()
        bayes_list = []
        for key, results in self.iteritems():
            bayes_list.append(results.bayes)
        if combine in ['add', 'sum']:
            new_b = addb(bayes_list, axis=0)
        elif combine == 'logavgexp':
            new_b = logavgexp_lnb(bayes_list)
        elif combine == 'avg':
            new_b = np.average(bayes_list, axis=0)
        elif combine == 'std':
            new_b = np.std(bayes_list, axis=0)
        elif combine in ['sub', 'subtract']:
            # this is a roundabout way of subtracting lnBs that preserves
            # injection information
            new_b = self[sub_keys[0]].bayes - self[sub_keys[1]].bayes
        else:
            raise ValueError("`combine` must be 'add', 'sub' or 'logavgexp'.")
        # preserve PAR files if explicitely requested or if option not given
        # but preserving injections
        if preserve_par:
            pulsarpar = self[self.keys()[0]].pulsarpar
        elif preserve_par is None and not blank_inj:
            pulsarpar = self[self.keys()[0]].pulsarpar
        else:
            pulsarpar = None
        # create new InjectionArray preserving only HEFF info
        # (this is because, we assume we are combining accross pulsars and
        # source-dependent information becomes meaningless)
        # [but this is not always true! sometimes combine accross models!]
        if self.inject_key not in ['none', 'noise', None, '']:
            injections = InjectionParArray(self[self.keys()[0]].injections)
            for parkey in self.inject_model.params:
                par = basic.Parameter(parkey, 0)
                in_heff = parkey in self.inject_model.heff_args
                if blank_inj and \
                        not (par.isamplitude or par.isphase or in_heff):
                    injections.setparam(parkey, 0)
        else:
            injections = None
        results = Results(bayes=new_b, injections=injections, par=pulsarpar)
        if full:
            return results.fold()
        else:
            return results

    def plot(self, xkey, ykey, *args, **kwargs):
        """Produce scatter plot of all results objects.

        Special usage (only valid if different entries correspond to different
        pulsars): if one of the keys is 'fit', will produce a fit for each
        of the Results objects and will plot the coefficient vs an parameter
        intrinsic to the source. For example, this can be used to produce a
        scatter plot of the slope of the best-fit-line of SQRTB vs HEFF for
        each of the results vs. the pulsar frequency F0. In this case, the
        parameters to fit can be passed as keyword arguments starting with
        'fit_'. The defaults are:
            fit_x = 'sqrtb'
            fit_y = 'heff'
            fit_zero_intersect = True
        The degree of the fit must be 1 for fit() to return a single
        coefficient, so that it can be plotted, so 'fit_deg' is not accepted.

        Extra arguments are passed to the plotting function.

        :param xkey: key of parameter to plot on the x axis (str).
        :param ykey: key of parameter to plot on the y axis (str).
        """
        # by default, plot all results
        plot_keys = kwargs.pop('keys', 'all')
        if plot_keys == 'all':
            plot_keys = self.keys()
        # by default, use keys as legends
        labels = kwargs.pop('labels', 'default')
        if labels == 'default':
            labels = {}
            for key in plot_keys:
                labels[key] = key
        elif not labels:
            labels = {}
            for key in plot_keys:
                labels[key] = None
        elif isinstance(labels, list):
            new_labels = {}
            for ix in range(len(plot_keys)):
                new_labels[plot_keys[ix]] = labels[ix]
            labels = new_labels
        elif isinstance(labels, dict):
            labels_hold = deepcopy(labels)
            for key in plot_keys:
                labels[key] = key
            for key in labels_hold:
                labels[key] = key
        else:
            raise ValueError("invalid labels.")
        # accept option 'legend' to determine whether to include legend
        legend = kwargs.pop('legend', False)
        # obtain fig and ax if passed
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        kwargs['fig'] = fig
        kwargs['ax'] = ax
        if 'fit' in [xkey.lower(), ykey.lower()]:
            fit_x = kwargs.pop('fit_x', 'heff')
            fit_y = kwargs.pop('fit_y', 'sqrtb')
            fit_zero_intersect = kwargs.pop('fit_zero_intersect', True)
            if xkey == 'fit':
                x = self.fit(fit_x, fit_y, zero_intersect=fit_zero_intersect)
                # note next step fails if key doesn't correspond to intrinsic
                # source parameter (e.g. 'F0' is valid, 'H0' is not).
                y = self.get_psr_param(ykey)
            else:
                y = self.fit(fit_x, fit_y, zero_intersect=fit_zero_intersect)
                # note next step fails if key doesn't correspond to intrinsic
                # source parameter (e.g. 'F0' is valid, 'H0' is not).
                x = self.get_psr_param(xkey)
            fig, ax = plt.subplots(1)
            fig = kwargs.pop('fig', fig)
            ax = kwargs.pop('ax', ax)
            ax.plot(x, y, *args, **kwargs)
        else:
            for key in plot_keys:
                kwargs['label'] = labels[key]
                fig, ax = self[key].plot(xkey, ykey, *args, **kwargs)
                kwargs['fig'] = fig
                kwargs['ax'] = ax
            if legend:
                ax.legend()
        return fig, ax

    def hist(self, *args, **kwargs):
        """Histograms combined Bayes factors from Results() objects.
        """
        # check whether to fold
        fold = kwargs.pop('fold', True)
        # obtain fig and ax if passed
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        kwargs['fig'] = fig
        kwargs['ax'] = ax
        # plot
        if fold:
            b_list = []
            for r in self.itervalues():
                b_list.append(float(r.fold()))
            b_array = LogBayesArray(b_list)
            fig, ax = b_array.hist(*args, **kwargs)
            ax.set_xlim(min(b_array), max(b_array))
        else:
            # by default, plot all results
            plot_keys = kwargs.pop('keys', 'all')
            if plot_keys == 'all':
                plot_keys = self.keys()
            # by default, use keys as legends
            labels = kwargs.pop('labels', 'default')
            if labels == 'default':
                labels = {}
                for key in plot_keys:
                    labels[key] = key
            elif not labels:
                labels = {}
                for key in plot_keys:
                    labels[key] = None
            elif isinstance(labels, list):
                labels = {}
                for ix in range(len(plot_keys)):
                    labels[plot_keys[ix]] = labels[ix]
            elif isinstance(labels, dict):
                labels_hold = deepcopy(labels)
                for key in plot_keys:
                    labels[key] = key
                for key in labels_hold:
                    labels[key] = key
            else:
                raise ValueError("invalid labels.")
            # accept option 'legend' to determine whether to include legend
            legend = kwargs.pop('legend', False)
            # find xlims
            bmin = np.inf
            bmax = - np.inf
            for key, r in self.iteritems():
                fig, ax = r.bayes.hist(*args, **kwargs)
                kwargs['fig'] = fig
                kwargs['ax'] = ax
                bmin = min(bmin, min(r.bayes))
                bmax = max(bmax, max(r.bayes))
            ax.set_xlim(bmin, bmax)
        return fig, ax

    def violinplot(self, locations='FGW', *args, **kwargs):
        """Violin plots for each of the component results in one axis."""
        if any([res.pulsarpar is None for res in self.itervalues()]):
            raise ValueError("Results object lacks PAR files.")
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        if isinstance(locations, basestring):
            if locations.upper() == 'FGW':
                locations = [2.0 * float(results.pulsarpar['F0']) for results
                             in self.itervalues()]
            else:
                locations = [float(results.pulsarpar[locations]) for results
                             in self.itervalues()]
        lnbs_lst = [results.bayes for results in self.itervalues()]
        violin_parts = ax.violinplot(lnbs_lst, locations, *args, **kwargs)
        return fig, ax, violin_parts

    def fit(self, *args, **kwargs):
        """Returns an array of the best-fit coefficients obtained by calling
        the fit() function of each stored Results() object.

        All parameters are passed directly to the Results.fit() function.
        """
        coeffs = []
        for key, result in self.iteritems():
            coeffs.append(result.fit(*args, **kwargs))
        return np.array(coeffs)

    def get_psr_param(self, param_key):
        """Returns an array of the values of an intrinsic source parameter.

        This is obtained by looking at the first injection in each of the
        Results objects contained. Thus, this cannot be used to obtain signal
        parameters, like H0 (because those may vary between injections).

        :param param_key: name of pulsar parameter.
        """
        param_key = param_key.upper()
        values = []
        for key, result in self.iteritems():
            try:
                values.append(result.getparam(param_key)[0])
            except IndexError:
                # parameter is single value, not array
                values.append(result.getparam(param_key))
        return np.array(values)

    def load_posteriors(self, pospath, nlive, nf=None, n0=0, wc="psrj",
                        wc_values=None, params=[]):
        """Load posterior samples for each results object."""
        wc_values = wc_values or self.keys()
        for wc_val, result in zip(wc_values, self.itervalues()):
            wc_kwargs = {wc: wc_val}
            pospath = os.path.abspath(basic.tokenize(pospath, **wc_kwargs))
            params = params or self.search_models[0].params
            result.load_posteriors(pospath, nlive, nf=nf, n0=n0, params=params)

    def combine_posteriors_gkde(self, param, index, value, *args, **kwargs):
        """ Uses GKDE to combine posteriors accross Result objects.

        Arguments
        ---------
        param: str
            name of parameter, e.g. 'C22'.
        index: int
            index of instantiation to be combined.
        value: float
            value of parameter at which interpolation is to be done.

        Returns
        -------
        pdf: float
            PDF of parameter at desired value from GKDE-combined distribution
            for a given instantiation.
        """
        # pick posterior samples of parameter 'param' for all Ninst for
        # each Results object stored in self; parray is a
        # PosteriorSamplesArray object
        parray = [r.posteriors.collect(param) for k, r in self.iteritems()]
        # pair up PosteriorSamples objects by Ninst index; each element
        # of psamples_set is a PosteriorSamples object and gkde combine
        psamples_array = PosteriorSamplesArray(zip(*parray)[index])
        return psamples_array.combined_gkde(value, *args, **kwargs)

    def random_set(self, nsets=1, setlen=None, seed=None, full_fold=True,
                   constraint_dict=None, keys=None):
        """Pick one instantiation randomly.from each Results object.

        Arguments
        ---------
        nsets: int
            number of sets to produce, default nsets=1.
        setlen: int
            number of items in each set.
        constraint_dict: dict
            dictionary indicating criteria for valid injections. Format:
            `{'param_key_str': (min_val, max_val), ...}`
        keys: array
            list of keys to be included (default: all).
        seed: float
            random seed, unset if `None` (default: `None`).
        full_fold: bool
            whether to fold runs and PSRs, cf. self.fold.

        """
        if seed is not None:
            random.seed(seed)
        if constraint_dict:
            for pkey, plim in constraint_dict.iteritems():
                if not (isinstance(pkey, basestring) and len(plim) == 2):
                    raise ValueError("Invalid `constraint_dict`. Valid format:"
                                     "\n`{'key_str': (min_val, max_val)}`")
        else:
            constraint_dict = {}
        if keys is None:
            keys = self.keys()
        results_dict = ResultsDict({}, search_keys=self.search_keys,
                                   inject_key=self.inject_key)
        for n in range(nsets):
            # pick `setlen` random Results
            if setlen:
                if setlen <= len(keys):
                    keys = np.random.choice(keys, setlen)
                else:
                    warn("Set-length must be less than number of items."
                         "Sets will have all elements.")
            # for each Results, pick a random instantiation
            injection_list = []
            b_list = []
            for key in keys:
                # check parameters satisfy constraint
                results = self[key].apply_constraints(constraint_dict)
                valid_lnbs = results.bayes
                valid_inj = results.injections
                # pick a random instantiation
                ix = np.random.randint(len(valid_lnbs))
                b_list.append(valid_lnbs[ix])
                if valid_inj:
                    injection_list.append(valid_inj[ix])
                else:
                    # no injections
                    injection_list.append(None)
            if self[keys[0]].injections:
                inj_array = InjectionParArray(contents=injection_list,
                                              model_key=self.inject_key)
            else:
                inj_array = None
            b_array = LogBayesArray(b_list, model_keys=self.search_keys)
            results_dict[n] = Results(bayes=b_array, injections=inj_array)
        return results_dict

    def pool(self, preserve_posteriors=True):
        """Combines results into a single object, loosing distinction between
        instantiations. Useful for histogramming across pulsars, for instance.
        """
        pooled_bayes = []
        ms = [[], []]
        post_dict_lst = []
        for results in self.itervalues():
            [ms[i].append(results.search_keys()[i]) for i in range(2)]
            if preserve_posteriors and results.posteriors is not None:
                post_dict_lst.append(results.posteriors[0])
            for b in results.bayes:
                pooled_bayes.append(b)
        for i in range(2):
            ms[i] = list(set(ms[i]))
        if any([len(m) > 1 for m in ms]):
            sk = (None, None)
        else:
            sk = [m[0] for m in ms]
        out_res = Results(bayes=pooled_bayes, search_keys=sk,
                          posteriors=None)#post_dict_lst)
        return out_res


class ResultsMatrix(OrderedDict):
    """Contains several Results() objects (2D).

    Dictionary of dictionaries such that Result objects must be accessed
    through an outer and inner keys: self[key1][key2]. This is thought for
    RUN and PSRJ as outer and inner keys respectively.
    """
    def __init__(self, input_data, ninst=0, **kwargs):
        sm_k = kwargs.pop("search_keys", (None, None))
        im_k = kwargs.pop("inject_key", None)
        if isinstance(input_data, ResultsMatrix):
            skeys = [sm_k[0] or input_data.search_keys[0],
                     sm_k[1] or input_data.search_keys[1]]
            im = basic.Model(im_k or input_data.inject_key)
        else:
            skeys = sm_k
            im = basic.Model(im_k)
        self.search_models = basic.ModelList(skeys)
        self.search_keys = self.search_models.keys
        self.inject_model = im
        self.inject_key = im.key
        self.ninst = ninst
        super(ResultsMatrix, self).__init__(input_data)
        # check contents are of right type
        for key, element in self.iteritems():
            if not isinstance(element, ResultsDict):
                self[key] = ResultsDict(element)

    @classmethod
    def collect(cls, bpath, wc1="run", wc2="psrj", wc1_values=None,
                wc2_values=None, search_keys=(None, None), inject_key=None,
                indep_inject=None, **kwargs):
        """Load from disk logBayes PPE results, using two wildcards in the path.

        The bayes factor and injection paths are expected to be of the form:
            bpath:   /some/dir/bdir/(RUN)/(PSRJ)/b_filename_(M)_(N).ext
            injpath: /some/dir/injdir/(RUN)/(PSRJ)/inj_filename_(M)_(N).ext

        The :bpath: argument may contain wildcards (M) for model and (N) for
        injection index as used by the ppe.Results.collect() method, plus the
        optional (RUN) and (PSRJ).
        """
        injpars = None
        master_bpath = os.path.abspath(bpath)
        master_optpaths = {pn: os.path.abspath(kwargs[pn]) for pn in
                           ['injpath', 'parpath', 'pospath'] if pn in kwargs
                           and kwargs[pn] is not None}
        if wc1_values is None:
            warn("guessing wc1 (%r) values from directory names." % wc1)
            bp = master_bpath.split('(%s)' % wc1.upper())[0]
            if os.path.isdir(bp):
                wc1_values = [r for r in os.listdir(bp)
                              if os.path.isdir(bp)]
            else:
                raise IOError("unable to access directory %r" % bp)
        if wc2_values is None:
            warn("guessing wc1 (%r) values from directory names." % wc2)
            wc2_values = {}
            for wc1_value in wc1_values:
                local_kwargs = {wc1: wc1_value}
                bp = basic.tokenize(master_bpath, **local_kwargs).split(
                    '(%s)' % wc2.upper())[0]
                if os.path.isdir(bp):
                    wc2_values[wc1_value] = [r for r in os.listdir(bp)
                                             if os.path.isdir(bp)]
                else:
                    raise IOError("unable to access directory %r" % bp)
        if indep_inject is None:
            # set default behavior such that, if wildcard not in injection
            # name, assume all elements have same injections and load upfront
            injpath = kwargs.get('injpath', None)
            wc_kwargs = {wc2: 'PlAcEhOlDeR'}
            indep_inject = (basic.tokenize(injpath, **wc_kwargs) != injpath)
        if not indep_inject:
            # assume all results have same injections and load upfront to
            # reduce IO operations
            injpath = kwargs.pop('injpath', None)
            nf = kwargs.get('nf', None)
            n0 = kwargs.get('n0', 0)
            if injpath is not None:
                injpars = PulsarParArray.collect(injpath, nf=nf, n0=n0,
                                                 model_key=inject_key)
        contents = {}
        for wc1_value in wc1_values:
            wc_kwargs = {wc1: wc1_value}
            bp = basic.tokenize(master_bpath, **wc_kwargs)
            optpaths = {pn: basic.tokenize(p, **wc_kwargs) for pn, p in
                        master_optpaths.iteritems()}
            local_kwargs = kwargs.copy()
            local_kwargs.update(optpaths)
            rd = ResultsDict.collect(bp, search_keys=search_keys,
                                     inject_key=inject_key,
                                     wc=wc2, wc_values=wc2_values[wc1_value],
                                     **local_kwargs)
            if not indep_inject:
                for result in rd.itervalues():
                    result.injections = injpars
            contents[wc1_value] = rd
        if indep_inject:
            n0 = kwargs.pop('n0', 0)
            nf = kwargs.pop('nf', -1)
        return cls(contents,  search_keys=search_keys, inject_key=inject_key,
                   ninst=nf-n0+1)

    def __getitem__(self, item):
        if isinstance(item, tuple) and len(item) == 2:
            key1, key2 = item
            if key1 == 'all' and key2 == 'all':
                return self
            if key1 == 'all':
                rdict_out = ResultsDict({}, search_keys=self.search_keys,
                                          inject_key=self.inject_key)
                for key, rdict in self.iteritems():
                    try:
                        rdict_out[key] = rdict[key2]
                    except KeyError:
                        pass
            elif key2 == 'all':
                rdict_out = super(ResultsMatrix, self).__getitem__(key1)
            else:
                rdict_out = super(ResultsMatrix, self).__getitem__(key1)[key2]
        else:
            rdict_out = super(ResultsMatrix, self).__getitem__(item)
        return rdict_out

    def pool(self, level=1, preserve_posteriors=True):
        """Combines results into a single object, loosing distinction between
        instantiations. Useful for histogramming across pulsars, for instance.
        """
        if float(level) == 1:
            out_res_dict = ResultsDict({})
            for key1, results_dict in self.iteritems():
                pooled_bayes = []
                ms = [[], []]
                post_dict_lst = []
                for key2, results in results_dict.iteritems():
                    [ms[i].append(results.search_keys()[i]) for i in range(2)]
                    if preserve_posteriors and results.posteriors is not None:
                        post_dict_lst.append(results.posteriors[0])
                    for b in results.bayes:
                        pooled_bayes.append(b)
                for i in range(2):
                    ms[i] = list(set(ms[i]))
                if any([len(m) > 1 for m in ms]):
                    sk = (None, None)
                else:
                    sk = [m[0] for m in ms]
                out_res_dict[key1] = Results(bayes=pooled_bayes,
                                             search_keys=sk,
                                             posteriors=None)#post_dict_lst)
            return out_res_dict
        else:
            raise NotImplementedError("Level %r pooling not implemented"
                                      % level)

    def fold(self, level=1, full=False, preserve_injections=False,
             preserve_par=True, combine='add'):
        """Add logBs corresponding to different runs and/or pulsars.

        This is achieved by addingB (not logB!). The result is the natural
        log of the combined Bayes factor. If only one dimension (ie RUN or
        PSRJ) is folded, return another ResultsDict object. If both are
        combined, returns a Results object.

        Importantly, it is assumed that all source-independent signal
        parameters are the same for all injections accross pulsars and runs.

        :param level: determines what keys to fold.
        """
        if level in [1, '1']:
            if combine == 'run':
                out_res_dict = {}
                for key2 in self._all_inner_keys():
                    rdict = self['all', key2]
                    lnb_arrays = [res.bayes for key1, res in rdict.iteritems()]
                    invodds_plus_one = [1./np.exp(lnbarr) + 1. for lnbarr in
                                        lnb_arrays]
                    if np.any(np.array(invodds_plus_one)==1.):
                        try:
                            import bigfloat
                            invodds_plus_one = [1./bigfloat.exp(lnbarr) + 1.
                                                for lnbarr in lnb_arrays]
                        except ImportError:
                            warn("Too large lnBs! Overflow error.")
                    prod_invodds_plus_one = np.ones(self.ninst)
                    for iopo in invodds_plus_one:
                        prod_invodds_plus_one *= iopo
                    final_odds = 1.0/(prod_invodds_plus_one - 1)
                    # assume same injections for all runs
                    r = Results(bayes=np.log(final_odds),
                                par=rdict[self.keys()[0]].pulsarpar,
                                injections=rdict[self.keys()[0]].injections,
                                search_keys=self.search_keys,
                                inject_key=self.inject_key)
                    out_res_dict[key2] = r
                out = ResultsDict(out_res_dict, search_keys=self.search_keys,
                                  inject_key=self.inject_key)
            else:
                out = ResultsDict({}, search_keys=self.search_keys,
                                  inject_key=self.inject_key)
                for key1, rdict in self.iteritems():
                    for key2, results in rdict.iteritems():
                        out.add(key2, results, combine=combine,
                                preserve_injections=preserve_injections,
                                preserve_par=preserve_par)
        elif level in [2, '2']:
            out = ResultsDict({}, search_keys=self.search_keys,
                                inject_key=self.inject_key)
            for key1, rdict in self.iteritems():
                out[key1] = rdict.fold(full=full)
        elif level == 'both':
            rdict_out = ResultsDict({}, search_keys=self.search_keys,
                                      inject_key=self.inject_key)
            for rdict in self.items():
                for key2, results in rdict.iteritems():
                    rdict_out.add(key2, results, combine=combine,
                                  preserve_injections=preserve_injections,
                                  preserve_par=preserve_par)
            out = rdict_out.fold(full=full)
        else:
            raise ValueError("invalid injection level %r" % level)
        return out

    def _all_inner_keys(self):
        key_list = []
        for item in self.itervalues():
            key_list += item.keys()
        return list(set(key_list))

    def _key1_by_key2(self):
        key1_by_key2 = {}
        for key2 in self._all_inner_keys():
            key1_by_key2[key2] = []
        for key1, rdict in self.iteritems():
            for key2 in rdict.keys():
                key1_by_key2[key2].append(key1)
        return key1_by_key2

    def load_posteriors(self, pospath, nlive, nf=None, n0=0, wc1="run",
                        wc2="psrj", wc1_values=None, wc2_values=None,
                        params=[]):
        """Load posterior samples for each results object."""
        wc1_values = wc1_values or self.keys()
        for wc1_val, result in zip(wc1_values, self.itervalues()):
            wc1_kwargs = {wc1: wc1_val}
            pospath = os.path.abspath(basic.tokenize(pospath, **wc1_kwargs))
            params = params or self.search_models[0].params
            result.load_posteriors(pospath, nlive, nf=nf, n0=n0, wc=wc2,
                                   wc_values=wc2_values, params=params)

    def random_set(self, nsets=1, setlen=None, seed=None, full_fold=False,
                   constraint_dict=None, bool_constraint_dict=None, key1s=None,
                   key2s=None):
        """For each key1, for each key2, pick one instantiation randomly.

        If full_fold is True, returns a BayesArray object containing the Bayes
        factors obtained by: folding over key1, picking one instantiation per
        key2, folding over pulsars, repeating nsets-times.

        If full_fold is False, returns a dictionary with key1 as keys and
        Results objects as values. Each element in the Results.bayes BayesArray
        corresponds to the chosen instantiation for a certain pulsar. Similar
        for Results.injections.

        In the former case, information about the pulsar identity and
        injection strength is lost. In the latter case, those data are
        contained in the Results.injections object. Note that if, for a given
        pulsar, there is data for multiple runs, the same instatiation is
        chosen for all runs.

        Arguments
        ---------
        nsets: int
            number of sets to produce, default nsets=1.
        setlen: int
            number of items in each set.
        constraint_dict: dict
            dictionary indicating criteria for valid injections. Format:
            `{'param_key_str': (min_val, max_val), ...}`
        key1s: list
            list of outer keys to be included (default: all).
        key2s: list
            list of inner keys to be included (default: all).
        seed: float
            random seed, unset if `None` (default: `None`).
        full_fold: bool
            whether to fold runs and PSRs, cf. self.fold.

        """
        if seed is not None:
            random.seed(seed)
        if constraint_dict:
            for pkey, plim in constraint_dict.iteritems():
                if not (isinstance(pkey, basestring) and len(plim)==2):
                    raise ValueError("Invalid `constraint_dict`. Valid format:"
                                     "\n`{'key_str': (min_val, max_val)}`")
        else:
            constraint_dict = {}
        if key1s is None:
            key1s = self.keys()
        if key2s is None:
            key2s = self._all_inner_keys()
        if bool_constraint_dict is None:
            bool_constraint_dict = {}

        if full_fold:
            # fold over the outer key
            results_dict = self.fold(1)
            bayes_final_list = []
            for n in range(nsets):
                # one logB chosen per pulsar
                bayes_set_list = []
                for key, res in results_dict.iteritems():
                    if key in key2s:
                        # check injection parameter satisfies constraint
                        bc = bool_constraint_dict.get(key)
                        new_res = res.apply_constraints(constraint_dict,
                                                        bool_constraint=bc)
                        bayes_set_list.append(random.choice(new_res.bayes))
                bayes_final_list.append(addb(bayes_set_list))
            return LogBayesArray(bayes_final_list, model_keys=self.search_keys)
        else:
            results_list = []
            for n in range(nsets):
                # initialize index dictionary
                ixdict = {key2: None for key2 in key2s}
                # pick `setlen` random pulsars
                key2_list = key2s
                if setlen:
                    if setlen <= len(key2s):
                        key2_idxs = [random.randint(0, len(key2s)-1)
                                     for _ in range(setlen)]
                        key2_list = [key2s[k2idx] for k2idx in key2_idxs]
                    else:
                        warn("Set-length must be less than number of items."
                             "Sets will have all elements.")
                # for each key1, for all key2s, pick a random instantiation
                results_dict = {}
                for key1 in key1s:
                    resdict = self[key1]
                    injection_list = []
                    b_list = []
                    par_list = []
                    for key2 in key2_list:
                        old_res = resdict[key2]
                        bc = bool_constraint_dict.get(key2)
                        results = old_res.apply_constraints(constraint_dict,
                                                            bool_constraint=bc)
                        # check injection parameter satisfies constraint
                        valid_lnbs = results.bayes
                        valid_inj = results.injections
                        # pick a random instantiation, if there isn't one
                        ix = ixdict[key2] or np.random.randint(len(valid_lnbs))
                        ixdict[key2] = ix
                        b_list.append(valid_lnbs[ix])
                        if valid_inj is not None:
                            injection_list.append(valid_inj[ix])
                        par_list.append(results.pulsarpar)
                    b_array = LogBayesArray(b_list)
                    if len(injection_list) == 0:
                        inj_array = InjectionParArray(par_list)
                        # do this to preserve pulsar iD if no injections
                    else:
                        inj_array = InjectionParArray(contents=injection_list,
                                                      model_key=self.inject_key)
                    results_dict[key1] = Results(bayes=b_array,
                                                 injections=inj_array)
                results_list.append(results_dict)
            return results_list


class ResultsModelMatrix(ResultsMatrix):
    """ Subclass of ResultsMatrix that assumes key1 is models, while key 2 is
    pulsars.
    """

    def __init__(self, input_data, *args, **kwargs):
        injections = kwargs.pop('injections', None)
        inject_key = kwargs.pop('inject_key', None)
        ninst = kwargs.pop('ninst', None)
        if input_data and injections is None:
            try:
                key1 = input_data.keys()[0]
                key2 = input_data[key1].keys()[0]
                results = input_data[key1][key2]
                injections = results.injections
                inject_key = results.inject_key
                ninst = len(results.bayes)
            except:
                warn("Error finding injections.")
        super(ResultsModelMatrix, self).__init__(input_data, *args, **kwargs)
        self.injections = injections
        self.inject_key = inject_key
        self.ninst = ninst
        self.inner_keys = self._all_inner_keys()

    def _load_properties(self):
        """Obtain instantiation properties from contents."""
        key1 = self.keys()[0]
        key2 = self.inner_keys()[0]
        results = self[key1][key2]
        if self.injections is None:
            self.injections = results.injections
        if self.inject_key is None:
            self.inject_key = results.inject_key
        if self.ninst in [None, 0]:
            self.ninst = len(results.bayes)

    def random_inst(self, nsets=1, setlen=None, seed=None, psrs=None,
                    constraint_dict=None):
        # obtain properties from contents
        self._load_properties()
        # setup random generator
        if seed is not None:
            random.seed(seed)
        # check user-defined constraints
        if constraint_dict:
            for pkey, plim in constraint_dict.iteritems():
                if not (isinstance(pkey, basestring) and len(plim) == 2):
                    raise ValueError("Invalid `constraint_dict`. Valid format:"
                                     "\n`{'key_str': (min_val, max_val)}`")
        else:
            constraint_dict = {}
        # get pulsar names
        if psrs is None:
            psrs = self.inner_keys()
        # loop over sets
        index_dict_list = []
        for n in range(nsets):
            # pick `setlen` random pulsars
            if setlen:
                if setlen <= len(psrs):
                    psrs = np.random.choice(psrs, setlen)
                else:
                    warn("Set-length must be less than number of items."
                         "Sets will have all elements.")
            # for each pulsar, pick a random instantiation
            index_dict = {}
            for key in psrs:
                # check parameters satisfy constraint
                valid_inj = self.injections
                valid_ixs = range(self.ninst)
                for pkey, plim in constraint_dict.iteritems():
                    plims = []
                    for pl in plim:
                        if isinstance(pl, basestring):
                            lkey_scale = pl.split('_times_')
                            lkey = lkey_scale[0]
                            if len(lkey_scale) == 1:
                                scale = 1
                            elif len(lkey_scale) == 2:
                                scale = float(lkey_scale[1])
                            else:
                                raise ValueError("invalid constraint "
                                                 " format: %r" % pl)
                            plims.append(scale*np.array(valid_inj.getparam(lkey)))
                        else:
                            plims.append([pl] * len(valid_ixs))
                    pmins, pmaxs = plims
                    ps = valid_inj.getparam(pkey)
                    valid_ixs = [ix for ix, pmin, p, pmax in zip(valid_ixs, pmins, ps, pmaxs)
                                 if pmin <= p <= pmax]
                    if not valid_ixs:
                        raise ValueError('no injections satisfying %s '
                                         'constraint.' % pkey)
                    valid_inj = InjectionParArray([i for i, pmin, p, pmax
                                                   in zip(valid_inj, pmins, ps, pmaxs)
                                                   if pmin <= p <= pmax])
                # pick a random instantiation
                ix = np.random.randint(len(valid_ixs))
                index_dict[key] = valid_ixs[ix]
            index_dict_list.append(index_dict)
        return index_dict_list

    def ensemble_and(self, psrjs=None, models=None, nongr_gr=False,
                     series=False, **kwargs):
        if 'nsets' in kwargs:
            warn("Ignoring NSETS argument.")
            kwargs['nsets'] = 1
        # pick `setlen` random pulsarsars; repeat `NITER` times to obtain
        # `NITER` sets of `setlen` randomly drawn sources each:
        psrjs = psrjs or self._all_inner_keys()
        models = models or self.keys()
        random_set = self.random_set(key1s=models, key2s=psrjs, **kwargs)[0]
        # redefine `psrs` in case the random set length is less than the
        # original pulsar set
        psrjs = random_set[models[0]].getparam('PSRJ')
        np.random.shuffle(psrjs)
        # `random_set = {model1: Results([PSR1, ...], [ix1, ...] ...}`
        partial_sets = []
        log_base = np.exp(1)
        if series:
            partial_key_list = []
            for psrj in psrjs:
                partial_key_list.append(psrj)
                model_dict = {m: [] for m in models}
                for m in models:
                    results = random_set[m]
                    local_psrj = results.getparam('PSRJ')
                    local_lnbs = results.bayes
                    log_base = local_lnbs.logbase
                    for loc_psrj, lnb in zip(local_psrj, local_lnbs):
                        if loc_psrj in partial_key_list:
                            model_dict[m].append(lnb)
                partial_sets.append(model_dict)
        else:
            model_dict = {m: [] for m in models}
            for m in models:
                results = random_set[m]
                local_psrj = results.getparam('PSRJ')
                local_lnbs = results.bayes
                log_base = local_lnbs.logbase
                for loc_psrj, lnb in zip(local_psrj, local_lnbs):
                    model_dict[m].append(lnb)
                partial_sets.append(model_dict)
        # `partial_sets = [{model1: [lnb_psr1, lnb_psr2, ...], ...}]`
        for partial_set in partial_sets:
            for m in partial_set.keys():
                partial_set[m] = LogBayesArray(partial_set[m], logbase=log_base)
        if nongr_gr:
            # Requested non-GR vs GR, rather than signal vs noise
            if 'GR' in models:
                new_partial_sets = []
                for partial_set in partial_sets:
                    gr_lnbs = partial_set['GR']
                    model_dict = {m: partial_set[m]-gr_lnbs for m
                                  in partial_set.keys() if m != 'GR'}
                    new_partial_sets.append(model_dict)
                partial_sets = new_partial_sets
                models = [m for m in models if m != 'GR']
            else:
                raise ValueError('Non-GR vs GR requested but GR not in models')
        ensemble_logodds_list = []
        for partial_set in partial_sets:
            # implemente Eq. (47) in methods paper
            # first produce sum of lnbs over pulsars for each model
            cum_logbs = LogBayesArray([np.sum(partial_set[m]) for m in models])
            # then produce the log of the average of the cumulative logbs
            ensemble_logodds_list.append(cum_logbs.logavgexp())
        if len(ensemble_logodds_list) == 1:
            return ensemble_logodds_list[0]
        else:
            return LogBayesArray(ensemble_logodds_list)

    def get_nongr_gr(self, gr_key='GR'):
        """ Combine GR and non-GR results to produce nGR vs GR odds. Uses
        `logavgexp` to combine non-GR sub-hypotheses and then takes difference
        with GR results to obtain non-GR vs GR odds with equal priors for the
        GR and non-GR hypotheses.

        Parameters
        ----------
        gr_key : str
            key of the GR model ('GR' or 'tensor').

        Returns
        -------
        ngr_dict : ResultsDict
            object containing the non-GR vs GR odds, with PSRJ as keys.

        """
        # obtain keys from sub-hypotheses making up the NONGR model
        nongr_keys = set(self.keys()) & set(basic.NONGR_KEYS) - {gr_key}
        # initialize output dictionary and loop over pulsars
        ngr_dict = ResultsDict({}, search_keys=('NGR', gr_key),
                               inject_key=self.inject_key)
        # update inner keys
        self.inner_keys = self._all_inner_keys()
        for psrj in self.inner_keys:
            # obtain GR vs noise results
            results_gr = self[gr_key][psrj]
            # obtain non-GR vs noise results
            dummy_dict = ResultsDict({m: self[m][psrj] for m in nongr_keys},
                                     inject_key=self.inject_key)
            results_ngr = dummy_dict.fold(blank_inj=False, combine='logavgexp')
            # combine to obtain non-GR vs GR
            dummy_dict = ResultsDict({gr_key: results_gr, 'NGR': results_ngr},
                                     inject_key=self.inject_key)
            results_ngr_gr = dummy_dict.fold(sub_keys=('NGR', gr_key),
                                             blank_inj=False, combine='sub')
            results_ngr_gr.update_models(search_keys=('NGR', gr_key))
            ngr_dict[psrj] = results_ngr_gr
        return ngr_dict

    def get_signal_noise(self, keys=None):
        """ Combine results to produce signal vs noise odds.

        Uses `logavgexp` to combine nsub-hypotheses to obtain any-signal vs
        noise odds with equal prior weight for all sub-hypotheses.

        Parameters
        ----------
        keys : list
            optional model keys to construct signal hypothesis (default: all).

        Returns
        -------
        sig_dict : ResultsDict
            object containing the signal vs noise odds, with PSRJ as keys.

        """
        # obtain keys from sub-hypotheses making up the signal model
        # (computed in this way to make sure extraneous model keys, like 'S'
        # or 'NGR', are not included in this computation)
        keys = keys or set(self.keys()) & (set(basic.NONGR_KEYS) | {'GR'})
        # initialize output dictionary and loop over pulsars
        sig_dict = ResultsDict({}, search_keys=('S', 'N'),
                               inject_key=self.inject_key)
        # update inner keys
        self.inner_keys = self._all_inner_keys()
        for psrj in self.inner_keys:
            # obtain non-GR vs noise results
            dummy_dict = ResultsDict({m: self[m][psrj] for m in keys},
                                     inject_key=self.inject_key)
            results_sig = dummy_dict.fold(blank_inj=False, combine='logavgexp')
            results_sig.update_models(search_keys=('S', 'N'))
            sig_dict[psrj] = results_sig
        return sig_dict


class PosteriorSamples(np.ndarray):
    """ Subclass of numpy.ndarry designed to contain posterior samples for a
    given parameter.

    Contains class methods to produce an array of posterior samples for nested
    samples. Also provides plotting and fitting methods.
    """
    _attributes = ['label']

    _gkde = None

    def __new__(cls, input_data, label=''):
        if isinstance(input_data, PosteriorSamples):
            return input_data
        obj = np.asarray(input_data).view(cls)
        if isinstance(label, basestring):
            obj.label = label.upper()
        else:
            raise TypeError("label argument must be str not %r" % type(label))
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        # set metadata using kwargs or default value specified above:
        for attr in self._attributes:
            setattr(self, attr, getattr(obj, attr, None))

    @classmethod
    def load(cls, path, column=0, label=''):
        """ Loads posterior samples from ASCII file.
        TODO: add HDF5 compatibility.

        Parameters
        ----------
        path: basestring
            path to text file with posterior samples.
        column: int
            column number to load (optional).
        label: basestring
            parameter name (optional).

        Returns
        -------
        newclass: PosteriorSamples
        """
        if isinstance(path, basestring):
            if os.path.exists(path):
                origin = os.path.abspath(path)
                try:
                    data = np.loadtxt(origin, comments='%')
                except ValueError:
                    data = np.loadtxt(origin, comments='#')
                if column == 0:
                    input_data = data
                else:
                    input_data = data[:, column]
            else:
                raise IOError("ASD file does not exist: %r" % path)
        else:
            raise TypeError("Path must be string, not %r." % type(path))
        return cls(input_data, label=label)

    # @classmethod
    # def from_nest(cls, input_data, log_wts, label=''):
    #     """ Creates posterior array from nested samples.
    #
    #     Wrapper for lalapps.nest2pos.draw_posterior.
    #
    #     Parameters
    #     ----------
    #     input_data: np.array
    #         Posterior samples.
    #     log_wts: np.array
    #         Array of log(weight)'s.
    #     label: str
    #         parameter name (optional).
    #
    #     Returns
    #     -------
    #     newclass: PosteriorSamples
    #     """
    #     # TODO: broken because lalapps.nest2pos broken, fix!
    #     from lalapps.nest2pos import draw_posterior
    #     return cls(draw_posterior(input_data, log_wts), label=label)

    # @classmethod
    # def load_nest(cls, path, nlive, label='', headers_path=None, headers=None):
    #     """ BROKEN!
    #     Loads nested samples from ASCII file and turn into posterior.
    #
    #     Based on lalapps_nest2pos executable.
    #
    #     Parameters
    #     ----------
    #     path: str
    #         path to text file with nested samples.
    #     nlive: int
    #         number of live points.
    #     label: str
    #         parameter name (optional).
    #     headers_path: str
    #         path to header file.
    #     headers: list of strings
    #         list of header names (optional alternative to headers_path).
    #     Returns
    #     -------
    #     newclass: PosteriorSamples
    #     """
    #     # TODO: broken because lalapps.nest2pos broken, fix!
    #     from lalapps.nest2pos import draw_posterior_many
    #     if headers is None and headers_path is None:
    #         # deafault headers path
    #         headers_path = "%s_params.txt" % path
    #     if headers_path is not None:
    #         if headers is None:
    #             # taken from lalapps_nest2pos:83
    #             headerfile = open(headers_path, 'r')
    #             headerstr = headerfile.readline()
    #             headerfile.close()
    #             headers = headerstr.lower().split()
    #         else:
    #             warn("Ignoring headers file because headers were passed "
    #                  "direcly.")
    #     else:
    #         headers = [header.lower() for header in headers]
    #     # find column numbers for data and log likelihoods
    #     loglikecol = headers.index('logl')
    #     if label.lower() in headers:
    #         poscol = headers.index(label.lower())
    #     else:
    #         raise ValueError("Parameter name not in provided headers.")
    #     posteriors = draw_posterior_many([np.loadtxt(path)], [nlive],
    #                                      logLcols=[loglikecol])
    #     return cls(np.array(posteriors)[:, poscol], label=label)

    def hist(self, *args, **kwargs):
        """pyploy.histogram wrapper.
        """
        fig, ax = plt.subplots(1)
        fig = kwargs.pop('fig', fig)
        ax = kwargs.pop('ax', ax)
        n, bins, _ = ax.hist(self, *args, **kwargs)
        return fig, ax, n, bins

    def gkde(self, value, reset=False, *args, **kwargs):
        """Returns the gkd interpolation of the value.

        Uses scipy.stats.gaussian_kde to estimate PDF in a non-parameteric way.
        Saves the estimator in the internal variable self._gkde for future
        use.

        Arguments
        ---------
        value: float
            value of parameter at which to compute PDF; this can be set to None
            to update self._gkde without returning anything.
        reset: bool
            indicates whether to overwrite gkde estimator already saved in
            self._gkde (optional, default False).

        Returns
        -------
        gkde: float
            PDF value for parameter at value from GKDE estimator.
        """
        if reset or self._gkde is None:
            self._gkde = gaussian_kde(self, *args, **kwargs)
        if value is not None:
            return self._gkde(value)
        else:
            return None

    def percentile(self, percent, **kwargs):
        """ Returns the value of the parameter that is greater than `percent`%
        of the posterior samples. (Wrapper for `np.percentile`.)

        Can be used to compute the `percent`%-confidence upper-limit for this
        parameter."""
        return np.percentile(self, percent, **kwargs)

    def percentile_error(self, percent, nboots=100, **kwargs):
        quant = percent/100.
        edict = bootstrap_cl_error(self, nboots, quantiles=[quant], **kwargs)
        return edict[quant]

    def dirichlet_pdfs(self, nboots, nbins=100):
        """ Produces multiple PDFs drawn from the same Dirichlet family as
        the posterior samples. See `bootstrap_cl_error`

        Arguments
        ---------
        nboots: int
            Number of PDFs instantiations ("bootstraps") to draw.
        nbins: int
            Number of histogram bins (default: 100).

        Returns
        -------
        pdfs: array
            Array of PDFs (`nboots` x `nbins`).
        hist: array
            Histogram of values in `self` (1 x `nbins`).
        edges: array
            Array of bin values from histogram.
        """
        # generate the observations (the histogram)
        hist, edges = np.histogram(self, bins=nbins)
        # choose alpha = 2 for dirichlet prior, corresponding to a uniform
        # distribution over the probabilities
        alphas = np.zeros(nbins) + 2.
        # now we want to infer the posterior pdf over the probabilities in each
        # bin using a Dirichlet priorupdate the concentration parameters
        updated_alphas = alphas + hist + 1.
        # generate nboots samples of the pdf
        pdfs = np.random.dirichlet(updated_alphas, size=nboots)
        return pdfs, hist, edges


class PosteriorSamplesArray(list):
    """ Subclass of list designed to contain multiple PosteriorSamples objects
    for the *same* parameter.

    The main use case for this class is as output of collecting the data from
    multiple runs of the PPE code. Provides methods to combine different
    posteriors using GKDE.
    """
    def __init__(self, input_data):
        # check contents are of right type
        new_data = []
        for element in input_data:
            new_data.append(PosteriorSamples(element))
        super(PosteriorSamplesArray, self).__init__(new_data)

    def combined_gkde(self, value, *args, **kwargs):
        """Returns gkde estimated function from combination of all posteriors.

        Produces an analytical PDF from the GKDE fit to each posterior array
        object and then multiplies them together.

        Arguments
        ---------
        value: float
            point at which to evaluate the combined PDF.
        """
        # first make sure that all posteriors in list correspond to the same
        # parameter; otherwise, it makes no sense to multiply their PDFs:
        headers = [posarray.label for posarray in self]
        if len(set(headers)) != 1:
            warn("Multiplying PDFs from different parameters!")
        return np.prod([posarray.gkde(value, *args, **kwargs) for posarray in
                        self])

    def percentiles(self, *arg, **kwargs):
        return np.array([ps.percentile(*arg, **kwargs) for ps in self])


class PosteriorSamplesDict(OrderedDict):
    """ Stores posterior distriburions for multiple parameters.

    Main intended use case is holding posterior samples corresponding to a
    single PPE run, i.e. what results from loading a given out.txt file.
    """
    def __init__(self, input_data):
        if isinstance(input_data, dict):
            # check contents are of right type
            for key, element in input_data.iteritems():
                if not isinstance(element, PosteriorSamples):
                    self[key] = PosteriorSamples(element, label=key)
            super(PosteriorSamplesDict, self).__init__(input_data)
        else:
            # assume input_data is a list of PosteriorArray objects
            post_dict = {}
            for post_array in input_data:
                post_dict[post_array.label] = post_array
            super(PosteriorSamplesDict, self).__init__(post_dict)

    def __getitem__(self, key):
        if key in self:
            item = super(PosteriorSamplesDict, self).__getitem__(key)
        else:
            argkeys, func = basic.translate_parameter(key, list(self))
            args = [super(PosteriorSamplesDict, self).__getitem__(k) for k in
                    argkeys]
            item = func(*args)
        return item

    @classmethod
    def load(cls, path, params=()):
        """ Loads posterior samples from disk.

        Parameters
        ----------
        path: str
            path to output of `lalapps_nest2pos` (HDF5 or ASCII)
        """
        from lalinference.io import read_samples
        try:
            from lalinference import \
                LALInferenceHDF5PosteriorSamplesDatasetName as tablename
        except ImportError:
            tablename = 'posterior_samples'
        # `samples` will be an `astropy.table.Table` object
        samples = read_samples(path, tablename=tablename)
        postarray_list = []
        columns = [str(c) for c in samples.columns]
        paramkeys = [p.upper() for p in params if p in columns]
        paramkeys = paramkeys or columns
        for col in paramkeys:
            postarray_list.append(PosteriorSamples(np.array(samples[col]),
                                                   label=col))
        return cls(postarray_list)

    @classmethod
    def load_nest(cls, path, nlive=None, labels=None):
        """ Loads nested samples from or HDF5 file and turn into posterior.
        WARNING: *very* slow if using HDF5!

        Based on lalapps_nest2pos executable.

        Parameters
        ----------
        path: str
            path to text file with nested samples.
        nlive: int
            number of live points.
        labels: list
            list of parameter names (optional).

        Returns
        -------
        newclass: PosteriorSamplesDict
        """
        from .nest2pos import draw_posterior_many
        fe = os.path.splitext(path)[-1].lower()
        if fe in ['.h5', '.hdf', '.hdf5']:
            from .lalapps_nest2pos import read_nested_from_hdf5
            if not nlive:
                warn("Ignoring user-defined Nlive in favor of HDF5 metadata")
            if not os.path.exists(path):
                raise IOError("no nested samples found in: %r" % path)
            return_values = read_nested_from_hdf5([path])
            (input_arrays, log_noise_evidence, log_max_likelihood,
             metadata, nlive_from_file, run_identifier) = return_values
            if not input_arrays:
                raise IOError("no nested samples found in: %r" % path)
            nlive = map(int, nlive_from_file)
            headers = input_arrays[0].dtype.names
            posterior = draw_posterior_many(input_arrays, nlive)
            if not labels:
                labels = headers
            else:
                labels = [label for label in labels if label.upper() in headers]
            postarray_list = [PosteriorSamples(np.array(posterior[label]), label=label) for label in labels]
        else:
            from .lalapps_nest2pos import read_nested_from_ascii
            return_values = read_nested_from_ascii([path])
            (headers, input_arrays, log_noise_evidence, log_max_likelihood) = \
                return_values
            if not input_arrays:
                raise IOError("no nested samples found in: %r" % path)
            if nlive:
                try:
                    nlive = [int(nlive)]
                except TypeError:
                    pass
            else:
                raise ValueError("must provide `nlive` to load from ASCII")
            loglikecol = headers.index('logL')
            posterior = draw_posterior_many(input_arrays, nlive,
                                            logLcol=loglikecol)
            if not labels:
                labels = headers
            try:
                poscols = [headers.index(label.upper()) for label in labels if
                           label.upper() in headers]
            except ValueError:
                poscols = [headers.index(label.lower()) for label in labels if
                           label.lower() in headers]
            if len(poscols) == 0:
                raise ValueError("Parameter name not in provided headers.")
            postarray_list = []
            for (param, poscol) in zip(labels, poscols):
                postarray_list.append(
                    PosteriorSamples(np.array(posterior)[:, poscol], label=param))
        return cls(postarray_list)

    # # DEPRECATED
    # @classmethod
    # def load_nest_alt(cls, path, nlive=None, labels=None, headers_path=None,
    #                   headers=None):
    #     """ DEPRECATED: Loads nested samples from file and turn into posterior.
    #
    #     Based on lalapps_nest2pos executable.
    #
    #     Parameters
    #     ----------
    #     path: str
    #         path to text file with nested samples.
    #     nlive: int
    #         number of live points.
    #     labels: list
    #         list of parameter names (optional).
    #     headers_path: str
    #         path to header file.
    #     headers: list of strings
    #         list of header names (optional alternative to headers_path).
    #
    #     Returns
    #     -------
    #     newclass: PosteriorSamplesDict
    #     """
    #     from .nest2pos import draw_posterior_many
    #     fe = os.path.splitext(path)[-1].lower()
    #     if fe in ['.h5', '.hdf', '.hdf5']:
    #         from .pulsarpputils import pulsar_nest_to_posterior
    #         posterior = pulsar_nest_to_posterior(path)
    #         if not labels:
    #             labels = headers
    #         else:
    #             labels = [label for label in labels if label.upper() in headers]
    #         postarray_list = [
    #             PosteriorSamples(np.array(posterior[label]), label=label) for label
    #             in labels]
    #     else:
    #         from .lalapps_nest2pos import read_nested_from_ascii
    #         return_values = read_nested_from_ascii([path])
    #         (headers, input_arrays, log_noise_evidence, log_max_likelihood) = \
    #             return_values
    #         if not input_arrays:
    #             raise IOError("no nested samples found in: %r" % path)
    #         if nlive:
    #             try:
    #                 nlive = [int(nlive)]
    #             except TypeError:
    #                 pass
    #         else:
    #             raise ValueError("must provide `nlive` to load from ASCII")
    #         loglikecol = headers.index('logL')
    #         posterior = draw_posterior_many(input_arrays, nlive,
    #                                         logLcol=loglikecol)
    #         if not labels:
    #             labels = headers
    #         try:
    #             poscols = [headers.index(label.upper()) for label in labels if
    #                        label.upper() in headers]
    #         except ValueError:
    #             poscols = [headers.index(label.lower()) for label in labels if
    #                        label.lower() in headers]
    #         if len(poscols) == 0:
    #             raise ValueError("Parameter name not in provided headers.")
    #         postarray_list = []
    #         for (param, poscol) in zip(labels, poscols):
    #             postarray_list.append(
    #                 PosteriorSamples(np.array(posterior)[:, poscol], label=param))
    #     return cls(postarray_list)

    def corner(self, **kwargs):
        import corner
        mkey = kwargs.pop('mkey', None)
        if mkey:
            model = basic.Model(mkey)
            def_keys = [k for k in model.params if k in self.keys() and
                        k not in ['RAJ', 'DECJ', 'PEPOCH', 'F0']]
        else:
            def_keys = self.keys()
        keys = kwargs.pop('params', def_keys)
        params = [basic.Parameter(k) for k in keys]
        ranges = kwargs.pop('ranges', [])
        def_ranges = False
        if ranges:
            if isinstance(ranges, dict):
                ranges_dict = deepcopy(ranges)
            elif ranges == 'default':
                def_ranges = True
                ranges_dict = {}
                ranges = []
            elif len(ranges) == len(keys):
                ranges_dict = dict(zip(keys, ranges))
            else:
                raise ValueError("Invalid ranges.")
        else:
            ranges_dict = {}
        labels = kwargs.pop('labels', [par.label for par in params])
        samples = []
        logscale = kwargs.pop('logscale', False)
        truths = kwargs.get('truths', None)
        if truths:
            if isinstance(truths, dict):
                truths_dict = deepcopy(truths)
            else:
                truths_dict = dict(zip(keys, truths))
        else:
            truths_dict = None
        for par in params:
            key = par.key
            if logscale and par.isamplitude:
                posteriors = np.log10(self[key])
            else:
                posteriors = self[key]
            samples.append(posteriors)
            autorange = (min(self[key]), max(self[key]))
            if autorange[0] >= autorange[1]:
                autorange = par.range
            elif truths_dict and truths_dict[key] is not None:
                autorange = (min(autorange[0], truths_dict[key]),
                            max(autorange[1], truths_dict[key]))
            if not ranges:
                ranges_dict[key] = autorange
            elif def_ranges:
                ranges_dict[key] = par.range
            elif ranges_dict[key] == 'auto':
                ranges_dict[key] = autorange
            elif ranges_dict[key] == 'default':
                ranges_dict[key] = par.range
        kwargs['range'] = [ranges_dict[key] for key in keys]
        if isinstance(truths, dict):
            kwargs['truths'] = [truths_dict[key] for key in keys]
        data = np.array(samples).T
        fig = corner.corner(data, labels=labels, **kwargs)
        return fig


class PosteriorSamplesDictArray(list):
    def __init__(self, input_data):
        # check contents are of right type
        new_data = []
        for element in input_data:
            new_data.append(PosteriorSamplesDict(element))
        super(PosteriorSamplesDictArray, self).__init__(new_data)

    @classmethod
    def load_glob(cls, globalpath, wc='*', wc_values=None, *args, **kwargs):
        """ Collects posterior samples from paths matching :globalpath:.

        Arguments
        ---------
        globalpath: str
            path pointing to the location of the files. Note it must point to
            files not just a directory. The string should contain a wildcard
            :wc:. If :wc_values: is provided, only paths with that match the
            global expression with a wildcard value in the list will be loaded.
        wc: str
            wildcard name (string to be replaced in path).
        wc_values: str, None
            wildcard values; if none, loads all matching files.
        """
        if not isinstance(globalpath, basestring):
            raise TypeError("globalpath must be a string, not %r"
                            % type(globalpath))
        postdict_lst = []
        if wc_values is not None:
            # wc_values is list of strings
            for val in wc_values:
                pd = PosteriorSamplesDict.load(
                    globalpath.replace(wc, str(val)), *args, **kwargs)
                postdict_lst.append(pd)
        else:
            paths = glob.glob(globalpath.replace(wc, '*'))
            for path in paths:
                pd = PosteriorSamplesDict.load(path, *args, **kwargs)
                postdict_lst.append(pd)
        if len(postdict_lst) == 0:
            raise IOError("Did not find any nested files matching %r"
                          % globalpath)
        return cls(postdict_lst)

    @classmethod
    def load_nest_glob(cls, globalpath, wc='*', wc_values=None, *args,
                       **kwargs):
        """ Collects nested samples from paths matching :globalpath:.

        Arguments
        ---------
        globalpath: str
            path pointing to the location of the files. Note it must point to
            files not just a directory. The string should contain a wildcard
            :wc:. If :wc_values: is provided, only paths with that match the
            global expression with a wildcard value in the list will be loaded.
        wc: str
            wildcard name (string to be replaced in path).
        wc_values: str, None
            wildcard values; if none, loads all matching files.
        """
        if not isinstance(globalpath, basestring):
            raise TypeError("globalpath must be a string, not %r"
                            % type(globalpath))
        postdict_lst = []
        if wc_values is not None:
            # wc_values is list of strings
            for val in wc_values:
                pd = PosteriorSamplesDict.load_nest(
                    globalpath.replace(wc, str(val)),
                    *args, **kwargs)
                postdict_lst.append(pd)
        else:
            paths = glob.glob(globalpath.replace(wc, '*'))
            for path in paths:
                pd = PosteriorSamplesDict.load_nest(path, *args, **kwargs)
                postdict_lst.append(pd)
        if len(postdict_lst) == 0:
            raise IOError("Did not find any nested files matching %r"
                          % globalpath)
        return cls(postdict_lst)

    @classmethod
    def load_n(cls, globalpath, nmin=0, nmax=0, *args, **kwargs):
        kwargs['wc'] = '(N)'
        kwargs['wc_values'] = range(nmin, nmax+1)
        return cls.load_glob(globalpath, *args, **kwargs)

    @classmethod
    def load_nest_n(cls, globalpath, nmin=0, nmax=0, *args, **kwargs):
        kwargs['wc'] = '(N)'
        kwargs['wc_values'] = range(nmin, nmax+1)
        return cls.load_nest_glob(globalpath, *args, **kwargs)

    def collect(self, item):
        return PosteriorSamplesArray([postdict[item] for postdict in self])

    def combine_gkde(self, item, value, *args, **kwargs):
        return self.collect(item).combined_gkde(value, *args, **kwargs)


class ConfigFile(SafeConfigParser):

    _GENERAL_OPTIONS = ['ifos', 'runs', 'psrj']
    _PATHS_REQUIRED = ['output', 'par', 'repo']
    _PATHS_OPTIONAL = ['exe', 'logs', 'temp', 'results']

    def __init__(self, *args, **kwargs):
        # old-class style initialization
        SafeConfigParser.__init__(self, *args, **kwargs)
        # initialize custom attributes
        self.ifos = []
        self.runs = []
        self.psrj = []
        self.paths = {}
        self.fake_data = {}
        self.prior = {}
        self.injection = {}
        self.seeds = {}
        self.split_jobs = True
        self.htc = {}
        self.maxjobs = 1000
        self.ppe = {}
        self.inject_numbers = {}
        self.total_n = 1
        self.search_model_keys = []
        self.search_models = basic.ModelList([])
        self.inject_model = basic.Model(None)
        self.inject_model_key = None
        self.name = ''

    def read(self, *args, **kwargs):
        mode = kwargs.pop('mode', 'closed')
        SafeConfigParser.read(self, *args, **kwargs)
        self._parse_contents(mode=mode)

    def _parse_contents(self, mode='closed'):
        self.mode = mode
        # -- PRIMARY ATTRIBUTES
        for opt in self._GENERAL_OPTIONS:
            if self.has_option('general', opt):
                attr = [x.strip() for x in self.get('general', opt).split(',')]
                setattr(self, opt, attr)
        # required paths
        for opt in self._PATHS_REQUIRED:
            self.paths[opt] = self.get('paths', opt)
        # fake-data options
        if self.has_option('ppe', 'fake-data'):
            # NOTE: assumes all IFOs have same fake-starts and fake-lengths;
            # TO DO: generalize this.
            self.fake_data['data'] = self.get('ppe', 'fake-data')
            for opt in ['starts', 'lengths', 'dt']:
                if self.has_option('ppe', 'fake-%s' % opt):
                    self.fake_data[opt] = self.get('ppe', 'fake-%s' % opt)
        else:
            # If not using fake-data and if data path is not provided,
            # should fail.
            self.paths['data'] = self.get('paths', 'data')
        # optional paths
        for path_key in self._PATHS_OPTIONAL:
            if self.has_option('paths', path_key):
                self.paths[path_key] = self.get('paths', path_key)
        # get absolute paths
        for name, path in self.paths.iteritems():
            self.paths[name] = os.path.abspath(path)
        # prior
        # assumes one section named [prior-model] for each search model and
        # that each section named this way contains an option named "model"
        # with the model key.
        # TODO: add option to read prior file from dir
        for section_name in self.sections():
            if 'prior' in section_name:
                options = {opt: self.get(section_name, opt) for opt in
                           self.options(section_name)}
                model = basic.Model(options.pop('model'))
                self.prior[model.key] = options
                self.search_model_keys.append(model.key)
        # Injections
        if self.has_section('injection'):
            for optname in self.options('injection'):
                self.injection[optname] = self.get('injection', optname)
            self.seeds['injection'] = float(self.injection.pop('seed', 2))
            self.inject_model_key = self.injection.pop("model")
            self.inject_model = basic.Model(self.inject_model_key)
        # Condor
        if self.has_option('condor', 'split_jobs'):
            self.split_jobs = self.getboolean('condor', 'split_jobs')
        elif mode == 'closed':
            self.split_jobs = False
        for optname in self.options('condor'):
            if optname != 'split_jobs':
                self.htc[optname] = self.get('condor', optname)
        if 'maxjobs' in self.htc:
            self.maxjobs = int(self.htc['maxjobs'])
        # PPE
        for optname in self.options('ppe'):
            self.ppe[optname] = self.get('ppe', optname)
        # number of injections
        if self.injection:
            for opt in self.inject_model.inj_numbers:
                if opt in self.injection:
                    self.inject_numbers[opt] = int(self.injection.pop(opt))
                elif 'n' in self.injection:
                    self.inject_numbers['n'] = int(self.injection['n'])
                else:
                    raise ValueError("need number of injections %r" % opt)
            self.injection.pop('n', None)
            # remove 'n' from options, if it was there
            self.total_n = self.inject_model.total_injection_number(
                self.inject_numbers)
        elif self.has_option('general', 'n'):
            self.total_n = self.getint('general', 'n')
        # randomseed
        if "randomseed" in self.ppe:
            rseed_arg = self.ppe.pop("randomseed")
            if rseed_arg == 'auto':
                self.seeds['ppe'] = range(self.total_n)
            elif rseed_arg == 'unset':
                self.seeds['ppe'] = []
            else:
                rseed_range = [float(x) for x in rseed_arg.split()]
                if len(rseed_range) == 1:
                    self.seeds['ppe'] = np.array(rseed_range * self.total_n)
                else:
                    self.seeds['ppe'] = np.linspace(rseed_range[0],
                                                    rseed_range[1],
                                                    self.total_n)
        else:
            self.seeds['ppe'] = range(self.total_n)
        # -- PATHS
        # Internal directory structure (default):
        #
        #   BASEDIR/
        #       - condor/
        #       - priors/
        #       - injections/
        #           - (RUN)/
        #               - (PSR)/
        #       - results/
        #           - (RUN)/
        #               - (PSR)/
        #                   - (M)/
        #                       - run.sh
        #       - logs/
        # Check optional paths were provided
        if 'exe' not in self.paths:
            self.paths['exe'] = "lalapps_pulsar_parameter_estimation_nested"
        basedir = self.paths['output']
        basename = os.path.basename(os.path.dirname("%s/" % basedir))

        scratch = basic.Cluster().scratch_dir
        if 'logs' not in self.paths:
            self.paths['logs'] = os.path.abspath(
                os.path.join(scratch, basename, 'logs/'))
        if 'temp' not in self.paths:
            # if cluster is not identified, this becomes
            # BASEDIR/tmp/(RUN)/(PSRJ)/
            self.paths['temp'] = os.path.abspath(
                os.path.join(scratch, basename, 'tmp/(RUN)/(PSRJ)/(M)/'))
        if 'results' not in self.paths:
            self.paths['results'] = os.path.join(basedir,
                                                 'results/(RUN)/(PSRJ)/(M)/',
                                                 'out_(N).hdf5')
        # Add internal directories
        if self.split_jobs:
            self.paths['sub'] = os.path.join(basedir,
                                             'condor/(RUN)_(PSRJ)_(M).sub')
        else:
            self.paths['sub'] = os.path.join(basedir, 'condor/ppe.sub')
        self.paths['dag'] = os.path.join(basedir, 'condor/ppe')
        self.paths['bashscript'] = os.path.join(os.path.dirname(
            self.paths['results']), 'run.sh')
        self.paths['temp_out'] = os.path.join(self.paths['temp'],
                                              os.path.basename(
                                                  self.paths['results']))
        # assuming same prior for all runs
        prior_path_def = os.path.join(basedir, 'priors/(PSRJ)/(M).txt')
        self.paths['prior'] = os.path.abspath(self.prior.pop('path',
                                                             prior_path_def))

        inj_path_def = os.path.join(basedir, 'injections/(RUN)/(PSRJ)/',
                                    'inj_%s_(N).par' % self.inject_model_key)
        self.paths['injections'] = os.path.abspath(
            self.paths.get('path', inj_path_def))

        # -- SEARCHES --
        if 'AP' in self.search_model_keys:
            # AP model naming convention is alphabetical
            # examples: tensor, tensor_vector, scalar_tensor_vector
            spin_comb_keys = basic.Model('AP').SPIN_COMB_KEYS
            self.search_model_keys = list(set(self.search_model_keys)-{'AP'})\
                                     + spin_comb_keys
        if 'AP1F' in self.search_model_keys:
            # AP model (first rotational harmonic)
            spin_comb_keys = basic.Model('AP').SPIN_COMB_KEYS_1F
            self.search_model_keys = list(set(self.search_model_keys)-{'AP1F'})\
                                     + spin_comb_keys
        if 'APALL' in self.search_model_keys:
            # AP model (both harmonics)
            spin_comb_keys = basic.Model('AP').SPIN_COMB_KEYS_ALL
            self.search_model_keys = list(set(self.search_model_keys)
                                          -{'APALL'}) + spin_comb_keys
        # make sure there are no repetitions
        self.search_model_keys = list(set(self.search_model_keys))
        self.search_models = basic.ModelList(self.search_model_keys)

        # -- OVERWRITE PATHS
        # if config has option 'all_paths_used' overwrite `self.paths`
        if self.has_section('all_paths_used'):
            paths = {}
            for optname in self.options('all_paths_used'):
                paths[optname] = self.get('all_paths_used', optname)
            self.paths = paths
        # add default posterior path
        if 'post' not in self.paths:
            res_file, res_ext = os.path.splitext(self.paths['results'])
            self.paths['post'] = "%s_pos%s" % (res_file,res_ext)
        # check that data and par paths are in right format
        test_str = basic.tokenize(self.paths['par'], psrj='#!', run='#!')
        if self.paths['par'] == test_str:
            self.paths['par'] = os.path.join(self.paths['par'], '(RUN)/',
                                             '(PSRJ).par')
        test_str = basic.tokenize(self.paths['data'], psrj='#!', run='#!',
                                  ifo='#!')
        if self.paths['data'] == test_str:
            self.paths['data'] = os.path.join(self.paths['data'], '(RUN)/',
                                              '(IFO)/finehet_(PSRJ)_(IFO)')
        # set analysis name from file name
        self.name = os.path.basename(self.paths['output'].strip('/'))

    def bpath(self):
        res_file, res_ext = os.path.splitext(self.paths['results'])
        if res_ext.lower() in ['.hdf', '.hf', '.hdf5']:
            bpath = self.paths['results']
        else:
            bpath = "%s%s_B%s" % (res_file, res_ext, res_ext)
        return bpath


#############################################################################
# DEPRECATED

class OpenResultsDict(ResultsDict):
    @classmethod
    def collect(cls, *args, **kwargs):
        """Same as ResultsDict.collect, with wc default 'run'.
        """
        kwargs['wc'] = kwargs.pop('wc', 'run')
        return super(OpenResultsDict, cls).collect(*args, **kwargs)


class OpenResultsMatrix(ResultsMatrix):
    """Subclass of ResultsMatrix that includes functions specifc to open box.

    Assuming key1 is RUN and key2 is PSRJ initializes the parent ResultsMatrix
    object such that each of the contained Results has an injection vector that
    contains the corresponding pulsar information. Inject model will still be
    None.

    The results will be an instance of ResultsMatrix in which key1 is RUN,
    key2 is PSRJ and the Results objects contain a single entry: Results.bayes
    will be of length 1 and will contain the open-box Bayes factor, while
    Results.injections will also be of length 1 and will contain a single
    PulsarPar object storing the source information (e.g. F0).

    Note that although PSR info is stored in Results.injections, there are no
    actual injections, since these are unblinded resutls (i.e. signal arguments
    should be absent from the PAR files).

    [DEPRECATED]
    """
    def __init__(self, *args, **kwargs):
        par_master_dir = kwargs.pop('pulsar_par')
        super(OpenResultsMatrix, self).__init__(*args, **kwargs)
        # get pulsar names (assuming key1=run & key2=psrj)
        pulsar_par_dict = {}
        for key1, rdict in self.iteritems():
            par_dir = os.path.join(par_master_dir, "%s/" % key1)
            file_list = [f for f in os.listdir(par_dir)
                         if os.path.isfile(os.path.join(par_dir, f))]
            pulsar_par_dict[key1] = {}
            for key2, result in rdict.iteritems():
                target_name = "%s" % key2.strip('J')
                for file_name in file_list:
                    if target_name in file_name:
                        par_path = os.path.join(par_dir, file_name)
                        pulsar_par_dict[key1][key2] = PulsarPar.read(par_path)
                if key2 in pulsar_par_dict[key1]:
                    if self[key1][key2].bayes:
                        self[key1][key2].injections = InjectionParArray([
                            pulsar_par_dict[key1][key2]])
                    else:
                        warn("no results for %s %s" % (key1, key2))
                        self[key1].pop(key2)
                else:
                    warn("did not find pulsar PAR file for %s in %r" %
                         (key2, par_dir))
                    self[key1].pop(key2)

    def plot_by_run(self, xkey, ykey, *args, **kwargs):
        """Scatter pplot of B or sqrtB vs F0 with different color for pulsars
        in which there is only data for one or both runs.
        """
        psrjs = self._all_inner_keys()
        # create dict with PSRJs as keys and list of runs as entries
        psr_dict = {}
        for psrj in psrjs:
            psr_dict[psrj] = []
            for run in self.keys():
                if psrj in self[run].keys():
                    psr_dict[psrj].append(run)
            run_set = set(psr_dict[psrj])
            run_label = ""
            for run in run_set:
                run_label += "%s, " % run
            run_label = run_label.strip().strip(',')
            psr_dict[psrj] = run_label
        # create dict with set of runs as keys and PSRJs as entries
        run_dict = {}
        for psrj, run_comb in psr_dict.iteritems():
            if run_comb in run_dict:
                run_dict[run_comb].append(psrj)
            else:
                run_dict[run_comb] = [psrj]
        # combine results
        folded_results_dict = self.fold(preserve_injections=True)
        # plot
        number_in_label = kwargs.pop('number_in_label', True)
        fig, ax = plt.subplots(1)
        for run_label, psrjs in run_dict.iteritems():
            x = []
            y = []
            for psrj in psrjs:
                x.append(folded_results_dict[psrj].getparam(xkey)[0])
                y.append(folded_results_dict[psrj].getparam(ykey)[0])
            x = np.array(x)
            y = np.array(y)
            fig = kwargs.pop('fig', fig)
            ax = kwargs.pop('ax', ax)
            if number_in_label:
                kwargs['label'] = "%s (%i)" % (run_label, len(psrjs))
            else:
                kwargs['label'] = run_label
            ax.plot(x, y, *args, **kwargs)
            kwargs['fig'] = fig
            kwargs['ax'] = ax
        return fig, ax
