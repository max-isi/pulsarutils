import numpy as np
from scipy.stats import rv_continuous


def distribution(name, *args, **kwargs):
    """Returns a Scipy distribution object with a given `name`.

    For info about frozen vs unfrozen distributuions, see scipy documentation:
    http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html
    http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_continuous.html

    Arguments
    ---------
    name: str
        name of a distribution defined here or in scipy.stats.

    Returns
    -------
    distr: scipy.stats.rv_continuous, scipy.stats.rv_discrete.
    """
    if isinstance(name, basestring):
        name = name.lower()
        if name in _CUSTOM_DISTRIBUTIONS:
            distr = globals()[name](*args, **kwargs)
        else:
            try:
                if name in ('normal', 'gaussian', 'gauss'):
                    name = 'norm'
                exec("from scipy.stats import %s" % name)
                distr = locals()[name]
            except ImportError:
                raise ValueError('Unknown distribution name: %r' % name)
    else:
        raise ValueError('Name must be a string, not %r' % type(name))
    return distr


##############################################################################
# FERMI-DIRAC

class fermidirac(rv_continuous):
    """ Fermi-Dirac-like distribution with by location (mu) and scale (sigma).

    Usage is the same as any other scipy.stats rv_continuous distribution:
        >>> from pulsarutils.stats import fermidirac
        >>> fd = fermidirac()
        >>> fermidirac.pdf(0)
        0.72134752044448169
        >>> mu, sigma = 10, 1
        >>> fermidirac.pdf(10, loc=mu, scale=sigma)
        0.049999773006534437
    Calling class functions with no location parameters is the same as setting
    loc = 0, scale = 1. The distribution can be frozen to avoid the need to
    pass location arguments at each call:
        >>> fd_frozen = fd(loc=mu, scale=sigma)
        >>> fd_frozen.pdf(10)
        0.049999773006534437

    Note this distribution does not accept domain arguments (a, b), since the
    domain is fixed to be 0 < x < inf.

    Here sigma is a parameter controlling the spread of the distribution, while
    mu is a parameter controlling the center of the distribution, defined by
    r = \mu / \sigma, where \mu is the point at which the PDF equals half
    the value at x=0.

    The properties and definition of this distribution are given in:
    https://www.authorea.com/users/50521/articles/65214/_show_article

    The behaviour of the parent class is explained in the scipy documentation:
    http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_continuous.html
    http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html
    """

    def _pdf(self, x):
        """ Calculates the probability of value x, assuming it follows a Fermi-
        Dirac like PDF with location parameters mu=0 and sigma=1.

        This function is used by the public version self.pdf() after proper
        rescaling by the corresponding location and scale parameters (mu and
        sigma).

        The properties and definition of this distribution are given in:
        https://www.authorea.com/users/50521/articles/65214/_show_article

        Arguments
        ---------
        x: float, np.array
            value at which to evaluate the PDF.

        Returns
        -------
        output: float
            probability of value x assuming FD with sigma=1 and r=0.
        """
        unnormed_pdf = np.power(np.exp(x)+1., -1.)
        norm = np.log(2.)
        output = unnormed_pdf/norm
        return output

    def _cdf(self, x, *args):
        """ Calculates the cumulative probability of value x, assuming it
        follows a Fermi-Dirac PDF with location parameters sigma=1 and r=0.

        The properties and definition of this distribution are given in:
        https://www.authorea.com/users/50521/articles/65214/_show_article

        Arguments
        ---------
        x: float, np.array
            value at which to evaluate the CDF.

        Returns
        -------
        cdf: float
            cumulative probability of value x assuming FD with sigma=1 and r=0.
        """
        unnormed_cdf = x - np.log(1. + np.exp(x))
        return unnormed_cdf

    def _rvs(self, *args, **kwds):
        # Use basic inverse cdf algorithm for RV generation as default.
        U = self._random_state.random_sample(self._size)
        Y = self.ppf(U, *args, **kwds)
        return Y

    def pdf(self, x, *args, **kwds):
        """ Calculates the probability of value x, assuming it follows a
        Fermi-Dirac like PDF with location parameters loc=mu and scale=sigma.

        Usually, this public method should not be overwritten when subclassing
        rv_continuous; however, this is necessary here because the norm of the
        FD distribution does not depend trivially on the location parameters.

        Shifting and scaling of the distribution can be done by using loc and
        scale parameters: gaussian.pdf(x, loc, scale) essentially computes
        y = (x - loc) / scale and gaussian._pdf(y) / scale; this does not
        return the correct FD normalization. This function scales the result
        correctly according to the formulas in:

        https://www.authorea.com/users/50521/articles/65214/_show_article

        The behavior of the parent class is explained in the scipy docs:
        http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_continuous.html
        http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html

        Parameters
        ----------
        x: float, np.array
            random variable values.
        args: list
            Other Parameters.
        loc: float, int
            center of the distribution (mu).
        scale: flat, int
            spead of the distribution (sigma).

        Returns
        -------
        pdf: float, np.array
        """
        args, loc, scale = self._parse_args(*args, **kwds)
        x, loc, scale = map(np.asarray, (x, loc, scale))
        output = super(fermidirac, self).pdf(x, *args, **kwds)
        normalization = scale * np.log(1 + np.exp(loc/scale))/np.log(2.)
        # this function is only has support over non-negative values
        if output.ndim == 0:
            if x < 0:
                output = 0
        else:
            output[x < 0] = 0
        return output/normalization

    def cdf(self, x, *args, **kwds):
        """ Calculates the cumulative probability of value x, assuming it
        follows a Fermi-Dirac-like PDF with location parameters loc=mu and
        scale=sigma.

        Usually, this public method should not be overwritten when subclassing
        rv_continuous; however, this is necessary here because the norm of the
        FD distribution does not depend trivially on the location parameters.

        Shifting and scaling of the distribution can be done by using loc and
        scale parameters: gaussian.pdf(x, loc, scale) essentially computes
        y = (x - loc) / scale and gaussian._pdf(y) / scale; this does not
        return the correct FD normalization. This function scales the result
        correctly according to the formulas in:

        https://www.authorea.com/users/50521/articles/65214/_show_article

        The behavior of the parent class is explained in the scipy docs:
        http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats
        .rv_continuous.html
        http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html

        Parameters
        ----------
        x: float, np.array
            random variable values.
        args: list
            Other Parameters.
        loc: float, int
            center of the distribution (mu).
        scale: flat, int
            spead of the distribution (sigma).

        Returns
        -------
        cdf: float, np.array
        """
        args, loc, scale = self._parse_args(*args, **kwds)
        x, loc, scale = map(np.asarray, (x, loc, scale))
        output = super(fermidirac, self).cdf(x, *args, **kwds)
        offset = loc/scale + np.log(1 + np.exp(1 - loc/scale))
        normalization = np.log(1 + np.exp(loc/scale))
        output = (output + offset)/normalization
        # this function is only has support over non-negative values
        if output.ndim == 0:
            if x < 0:
                output = 0
        else:
            output[x < 0] = 0
        return output

    def ppf(self, cdf, *args, **kwds):
        """ Calculates the percentage point function (inverse CDF), for a
        Fermi-Dirac-like distribution with location parameters loc=mu and
        scale=sigma.

        Usually, this public method should not be overwritten when subclassing
        rv_continuous; however, this is necessary here because the norm of the
        FD distribution does not depend trivially on the location parameters.

        Shifting and scaling of the distribution can be done by using loc and
        scale parameters: gaussian.pdf(x, loc, scale) essentially computes
        y = (x - loc) / scale and gaussian._pdf(y) / scale; this does not
        return the correct FD normalization. This function scales the result
        correctly according to the formulas in:

        https://www.authorea.com/users/50521/articles/65214/_show_article

        The behavior of the parent class is explained in the scipy docs:
        http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats
        .rv_continuous.html
        http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html

        Parameters
        ----------
        cdf: float, np.array
            cumulative probability value.
        args: list
            Other Parameters.
        loc: float, int
            center of the distribution (mu).
        scale: flat, int
            spead of the distribution (sigma).

        Returns
        -------
        cdf: float, np.array
        """
        args, loc, scale = self._parse_args(*args, **kwds)
        cdf, loc, scale = map(np.asarray, (cdf, loc, scale))
        if all([0 <= q < 1 for q in np.array(cdf)]):
            r = loc/scale
            x = - scale*np.log(-np.exp(-r) + (1. + np.exp(r))**(-cdf) +
                               np.exp(1.-r)*(1. + np.exp(r))**(-cdf))
        else:
            raise ValueError("CDF must satisfy 0 <= CDF < 1.")
        return x

    def logpdf(self, x, *args, **kwds):
        return np.log(self.pdf(x, *args, **kwds))

    def sf(self, x, *args, **kwds):
        return 1. - self.cdf(x, *args, **kwds)

    def rvs(self, *args, **kwds):
        """
        Random variates following FD distribution.

        Usually, this public method should not be overwritten when subclassing
        rv_continuous; however, this is necessary here because the norm of the
        FD distribution does not depend trivially on the location parameters.

        Shifting and scaling of the distribution can be done by using loc and
        scale parameters: gaussian.pdf(x, loc, scale) essentially computes
        y = (x - loc) / scale and gaussian._pdf(y) / scale; this does not
        return the correct FD normalization. This function scales the result
        correctly according to the formulas in:

        https://www.authorea.com/users/50521/articles/65214/_show_article

        The behavior of the parent class is explained in the scipy docs:
        http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_continuous.html
        http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html

        Parameters
        ----------
        arg1, arg2, arg3,... : array_like
            The shape parameter(s) for the distribution (see docstring of the
            instance object for more information).
        loc : array_like, optional
            Location parameter (default=0).
        scale : array_like, optional
            Scale parameter (default=1).
        size : int or tuple of ints, optional
            Defining number of random variates (default is 1).
        random_state : None or int or ``np.random.RandomState``, optional
            If int or RandomState, use it for drawing the random variates.
            If None, rely on ``self.random_state``.
            Default is None.

        Returns
        -------
        rvs : ndarray or scalar
            Random variates of given `size`.

        """
        discrete = kwds.pop('discrete', None)
        rndm = kwds.pop('random_state', None)
        args, loc, scale, size = self._parse_args_rvs(*args, **kwds)
        # self._size is total size of all output values
        self._size = np.product(size, axis=0)
        if self._size is not None and self._size > 1:
            size = np.array(size, ndmin=1)
        if np.all(scale == 0):
            return loc*np.ones(size, 'd')
        # extra gymnastics needed for a custom random_state
        if rndm is not None:
            random_state_saved = self._random_state
            self._random_state = check_random_state(rndm)
        vals = self._rvs(*args, loc=loc, scale=scale)
        if self._size is not None:
            vals = np.reshape(vals, size)
        # do not forget to restore the _random_state
        if rndm is not None:
            self._random_state = random_state_saved
        return vals


def fermidirac_sigma_r(ul, mufrac=0.4, cdf=0.95):
    """Calculate the r and sigma parameter of the Fermi-Dirac distribution.

    Based on the definition of the distribution given in
    https://www.authorea.com/users/50521/articles/65214/_show_article
    the distribution will be defined by a mu parameter at which the
    distribution has 50% of it's maximum probability, and mufrac which is the
    fraction of mu defining the range from which the distribution falls from
    97.5% of the maximum down to 2.5%. Using an upper limit defining a given
    cdf of the distribution the parameters r and sigma will be returned.
    """
    # use fsolve, rather than root, to be compatible with older versions of
    # scipy
    from scipy.optimize import fsolve
    # factor that defines the 97.5% -> 2.5% probability attenuation band
    # around mu
    z = 7.33
    r = 0.5*z/mufrac
    # using the Fermi-Dirac CDF to find sigma given a distribution where the
    # cdf value is found at ul
    cdf_funct = lambda s: cdf * np.log(1. + np.exp(r)) -\
                          np.log(1. + np.exp(-r)) - (ul / s) - \
                          np.log(1. + np.exp((ul / s) - r))
    sol = fsolve(cdf_funct, ul)
    sigma = sol[0]
    return sigma, r


##############################################################################
# LOG-UNIFORM (1/h)

class loguniform(rv_continuous):
    """ Scale-invariant probability distribution (uniform in the log).

    Usage is the same as any other scipy.stats rv_continuous distribution:
        >>> from pulsarutils.stats import loguniform
        >>> xmin, xmax = 1e-26, 1e-24
        >>> lu = loguniform(a=xmin, b=xmax)
        >>> lu.pdf(1e-25)
        2.171472409516259e+24
        >>> lu.cdf(1e-25)
        0.50000000000000011
    Note this distribution does not accept any location or scale parametrs; the
    normalization is computed based on the distribution's domain (determined by
    the a and b arguments, which are fixed at the beginning).

    The behaviour of the parent class is explained in the scipy documentation:
    http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.rv_continuous.html
    http://docs.scipy.org/doc/scipy/reference/tutorial/stats.html
    """

    def __init__(self, *args, **kwds):
        super(loguniform, self).__init__(*args, **kwds)
        if 0 < self.a:
            self.norm = np.log(self.b/self.a)
        else:
            self.norm = 1.0

    def _pdf(self, x):
        """ Computes PDF(x)~1/x, normalized to certain range if provided.

        Arguments
        ---------
        x: float, np.ndarray
            value(s) at which to evaluate PDF.

        Returns
        -------
        output: float, np.ndarray
            pdf(x)
        """
        x = np.array(x)
        if x.ndim > 0:
            unnormed_pdf = np.zeros(np.array(x.size))
            unnormed_pdf[x > 0] = 1. / x
        elif x > 0:
            unnormed_pdf = 1./x
        else:
            unnormed_pdf = 0
        output = unnormed_pdf / self.norm
        return output

    def _cdf(self, x):
        """ Computes CDF(x)~log(x) corresponding to PDF(x)~1/x.

        Arguments
        ---------
        x: float, np.ndarray
            value(s) at which to evaluate PDF.

        Returns
        -------
        output: float, np.ndarray
            cdf(x)
        """
        x = np.array(x)
        if x.ndim > 0:
            unnormed_cdf = np.zeros(np.array(x.size))
            unnormed_cdf[x > 0] = np.log(x/self.a)
        elif x > 0:
            unnormed_cdf = np.log(x/self.a)
        else:
            unnormed_cdf = 0
        output = unnormed_cdf / self.norm
        return output

    def _ppf(self, q):
        q = np.array(q)
        output = self.a * np.power(self.b/self.a, q)
        return output

_CUSTOM_DISTRIBUTIONS = {
    'fermidirac': fermidirac,
    'loguniform': loguniform
}


##############################################################################
# SCIPY COMPATIBILITY

# copied from scipy 0.16.0
def check_random_state(seed):
    """Turn seed into a np.random.RandomState instance

    If seed is None (or np.random), return the RandomState singleton used 
    by np.random.
    If seed is an int, return a new RandomState instance seeded with seed.
    If seed is already a RandomState instance, return it.
    Otherwise raise ValueError.
    """
    if seed is None or seed is np.random:
        return np.random.mtrand._rand
    if isinstance(seed, (numbers.Integral, np.integer)):
        return np.random.RandomState(seed)
    if isinstance(seed, np.random.RandomState):
        return seed
    raise ValueError('%r cannot be used to seed a numpy.random.RandomState'
                     ' instance' % seed)
