#! /usr/bin/env python

import os
import sys
import numpy as np
repodir = os.getenv('PULSARUTILS') or os.path.dirname(os.path.dirname(
    os.path.abspath(__file__)))
try:
    sys.path.append(repodir)
    from pulsarutils import ppe, basic
except ImportError, e:
    raise ImportError('ppe module not found. Set $PULSARUTILS to point to'
                      'package location. Error: %r' %r)

BASEDIR = os.path.join(os.getenv('HOME'), 'projects/cwpols/results2/')
FIGDIR = os.getenv('HOME') + '/results2_plots/'

# ###############################################################################
# # CRAB G4V/GR S5_H1 vs S5S6_H1H2L1V1 -- SCATTER
#
# # S5 H1
# outdir = BASEDIR+'crab_iG4V_sG4VGR_S5S6_H1/'
# crab_iG4V_sG4VGR_S5S6_H1 = ppe.ResultsDict.collect(outdir+'results/(RUN)/J0534+2200/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/(RUN)/J0534+2200/inj_G4V_(N).par', search_keys=('G4V', 'GR'), inject_key='G4V', macro='run', nf=199, macro_values=['S5'])
# crab_iG4V_sG4VGR_S5_H1 = crab_iG4V_sG4VGR_S5S6_H1['S5']
# fig, ax = crab_iG4V_sG4VGR_S5_H1.plot('heff', 'sqrtb', '.', label='S5 H1', color='#7A303F')
#
# ax.legend(numpoints=1, prop={'size': 18}, loc='upper left')
# ax.set_xlabel(r'$h_{\rm G4v}$ (effective)')
# ax.set_ylabel(r"${\rm sgn}{\left(B^{\rm G4v}_{\rm GR}\right)}|\ln B^{\rm G4v}_{\rm GR}|^\frac{1}{2}$")
# ax.set_ylim(-5, 50)
# fig.savefig(FIGDIR + '/crab_iG4V_sG4VGR_S5H1.pdf', bbox_inches='tight')
#
# # S5S6 H1H2L1V1
# outdir = BASEDIR+'crab_iG4V_sG4VGR_S5S6_H1H2L1V1/'
# fig, ax = crab_iG4V_sG4VGR_S5_H1.plot('heff', 'sqrtb', '.', label='S5 H1', color='#7A303F')
#
# crab_iG4V_sG4VGR_S5S6_H1H2L1V1 = ppe.ResultsDict.collect(outdir+'results/(RUN)/J0534+2200/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/(RUN)/J0534+2200/inj_G4V_(N).par', search_keys=('G4V', 'GR'), inject_key='G4V', macro='run', nf=199, macro_values=['S5', 'S6'])
#
# crab_iG4V_sG4VGR_S5S6_H1H2L1V1_folded = crab_iG4V_sG4VGR_S5S6_H1H2L1V1.fold()
#
# fig, ax = crab_iG4V_sG4VGR_S5S6_H1H2L1V1_folded.plot('heff', 'sqrtb', '.', label='S5,S6 H1,H2,L1,V1', fig=fig, ax=ax, marker='s', markeredgewidth=0, markersize=5, markerfacecolor="#E53E30")#"#7A303F")
#
# ax.legend(numpoints=1, prop={'size': 18}, loc='upper left')
# ax.set_xlabel(r'$h_{\rm G4v}$ (effective)')
# ax.set_ylabel(r"${\rm sgn}{\left(B^{\rm G4v}_{\rm GR}\right)}|\ln B^{\rm G4v}_{\rm GR}|^\frac{1}{2}$")
# ax.set_ylim(-5, 50)
# fig.savefig(FIGDIR + '/crab_iG4V_sG4VGR.pdf', bbox_inches='tight')
#
# ###############################################################################
# # CRAB G4V/GR S5_H1 vs S5S6_H1H2L1V1 -- HISTOGRAM
#
# ALPHA = 0.5
# NBINS = 20
# NORMED = True
# RANGE = (-6, 6)
#
# # S5 H1
# outdir = BASEDIR+'crab_noinj_sG4VGR_S5S6_H1/'
# crab_noinj_sG4VGR_S5S6_H1 = ppe.ResultsDict.collect(outdir+'results/(RUN)/J0534+2200/(M)/out_(N).txt_B.txt', search_keys=('G4V', 'GR'), macro='run', nf=99, macro_values=['S5'])
# crab_noinj_sG4VGR_S5_H1 = crab_noinj_sG4VGR_S5S6_H1['S5']
# fig, ax = crab_noinj_sG4VGR_S5_H1.bayes.hist(NBINS, normed=NORMED, linewidth=0, histtype='stepfilled', alpha=ALPHA, label='S5 H1', range=RANGE, facecolor="#FF6E1E")
#
# # S5S6 H1H2L1V1
# outdir = BASEDIR+'crab_noinj_sG4VGR_S5S6_H1H2L1V1/'
#
# crab_noinj_sG4VGR_S5S6_H1H2L1V1 = ppe.ResultsDict.collect(outdir+'results/(RUN)/J0534+2200/(M)/out_(N).txt_B.txt', search_keys=('G4V', 'GR'), macro='run', nf=99, macro_values=['S5', 'S6'])
#
# crab_noinj_sG4VGR_S5S6_H1H2L1V1_folded = crab_noinj_sG4VGR_S5S6_H1H2L1V1.fold()
#
# fig, ax = crab_noinj_sG4VGR_S5S6_H1H2L1V1_folded.bayes.hist(NBINS, normed=NORMED, linewidth=0, histtype='stepfilled', alpha=ALPHA, label='S5,S6 H1,H2,L1,V1', fig=fig, ax=ax, range=RANGE, facecolor="#E53E30")
# ax.legend(numpoints=1, prop={'size': 18}, loc='upper left')
# ax.set_xlabel(r"$\ln B^{\rm G4v}_{\rm GR}$")
# ax.set_ylabel(r"$P\left(\ln B^{\rm G4v}_{\rm GR}\right)$")
# fig.savefig(FIGDIR + '/crab_noinj_sG4VGR.pdf', bbox_inches='tight')
#
#
# ###############################################################################
# # CRAB G4V/GR S5_H1 vs S5S6_H1H2L1V1 -- SQRTB HISTOGRAM
#
# ALPHA = 0.5
# NBINS = 30
# NORMED = True
# RANGE = (-4, 4)
#
# # S5 H1
# signb = np.sign(crab_noinj_sG4VGR_S5_H1.bayes)
# sqrtb = signb * np.sqrt(np.absolute(crab_noinj_sG4VGR_S5_H1.bayes))
# fig, ax = sqrtb.hist(NBINS, normed=NORMED, linewidth=0, histtype='stepfilled', alpha=ALPHA, label='S5 H1', range=RANGE, facecolor="#FF6E1E")
#
# # S5S6 H1H2L1V1
# signb = np.sign(crab_noinj_sG4VGR_S5S6_H1H2L1V1_folded.bayes)
# sqrtb = signb * np.sqrt(np.absolute(crab_noinj_sG4VGR_S5S6_H1H2L1V1_folded.bayes))
# fig, ax = sqrtb.hist(NBINS, normed=NORMED, linewidth=0, histtype='stepfilled', alpha=ALPHA, label='S5,S6 H1,H2,L1,V1', fig=fig, ax=ax, range=RANGE, facecolor="#E53E30")
# ax.legend(numpoints=1, prop={'size': 18}, loc='upper left')
# ax.set_xlabel(r"${\rm sgn}{\left(B^{\rm G4v}_{\rm GR}\right)}|\ln B^{\rm G4v}_{\rm GR}|^\frac{1}{2}$")
# ax.set_ylabel(r"$P\left[{\rm sgn}{\left(B^{\rm G4v}_{\rm GR}\right)}|\ln B^{\rm G4v}_{\rm GR}|^\frac{1}{2}\right]$")
# fig.savefig(FIGDIR + '/crab_noinj_sG4VGR_sqrt.pdf', bbox_inches='tight')
#
#
# ###############################################################################
# # CRAB GR S5_H1 vs S5S6_H1H2L1V1 -- HISTOGRAM
#
# ALPHA = 0.5
# NBINS = 30
# NORMED = True
# RANGE = (-20, 0)
#
# # S5 H1
# outdir = BASEDIR+'crab_noinj_sG4VGR_S5S6_H1/'
# crab_noinj_sGR_S5S6_H1 = ppe.ResultsDict.collect(outdir+'results/(RUN)/J0534+2200/(M)/out_(N).txt_B.txt', search_keys=('GR', 'n'), macro='run', nf=99, macro_values=['S5'])
# crab_noinj_sGR_S5_H1 = crab_noinj_sGR_S5S6_H1['S5']
# fig, ax = crab_noinj_sGR_S5_H1.bayes.hist(NBINS, normed=NORMED, linewidth=0, histtype='stepfilled', alpha=ALPHA, label='S5 H1', range=RANGE, facecolor="#FF6E1E")
#
# # S5S6 H1H2L1V1
# outdir = BASEDIR+'crab_noinj_sG4VGR_S5S6_H1H2L1V1/'
#
# crab_noinj_sGR_S5S6_H1H2L1V1 = ppe.ResultsDict.collect(outdir+'results/(RUN)/J0534+2200/(M)/out_(N).txt_B.txt', search_keys=('GR', 'n'), macro='run', nf=99, macro_values=['S5', 'S6'])
# crab_noinj_sGR_S5S6_H1H2L1V1_folded = crab_noinj_sGR_S5S6_H1H2L1V1.fold()
# fig, ax = crab_noinj_sGR_S5S6_H1H2L1V1_folded.bayes.hist(NBINS, normed=NORMED, linewidth=0, histtype='stepfilled', alpha=ALPHA, label='S5,S6 H1,H2,L1,V1', fig=fig, ax=ax, range=RANGE, facecolor="#E53E30")
# ax.legend(numpoints=1, prop={'size': 18}, loc='upper left')
# ax.set_xlabel(r"$\ln B^{\rm GR}_{\rm n}$")
# ax.set_ylabel(r"$P\left(\ln B^{\rm GR}_{\rm n}\right)$")
# fig.savefig(FIGDIR + '/crab_noinj_sGR.pdf', bbox_inches='tight')
#

# ###############################################################################
# MP G4V/GR S5_H1 vs S5S6_H1H2L1V1 -- FIT

# # S5S6 H1H2L1V1
# outdir = BASEDIR+'mp_iG4V_sG4VGR_S5S6_H1L1/'
#
# mp_iG4V_sG4VGR_S5S6_H1H2L1V1 = ppe.ResultsMatrix.collect(outdir+'results/(RUN)/(PSRJ)/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/(RUN)/(PSRJ)/inj_G4V_(N).par', search_keys=('G4V', 'GR'), inject_key='G4V', macro1='run', nf=4, macro1_values=['S5', 'S6'], macro2='psrj')
#
# mp_iG4V_sG4VGR_S5S6_H1H2L1V1_folded = mp_iG4V_sG4VGR_S5S6_H1H2L1V1.fold(preserve_injections=True)
#
# fig, ax = mp_iG4V_sG4VGR_S5S6_H1H2L1V1_folded.plot('FGW', 'fit', '+')
# ax.set_xscale('log')
# ax.set_xlim(30, 1500)
# fig.savefig(FIGDIR + '/mp_iG4V_sG4VGR_fits.png', bbox_inches='tight')
#
# fig, ax = mp_iG4V_sG4VGR_S5S6_H1H2L1V1['S5'].plot('FGW', 'fit', '+')
# ax.set_xscale('log')
# ax.set_xlim(30, 1500)
# fig.savefig(FIGDIR + '/mp_iG4V_sG4VGR_fits_S5.png', bbox_inches='tight')
#
# fig, ax = mp_iG4V_sG4VGR_S5S6_H1H2L1V1['S6'].plot('FGW', 'fit', '+')
# ax.set_xscale('log')
# ax.set_xlim(30, 1500)
# fig.savefig(FIGDIR + '/mp_iG4V_sG4VGR_fits_S6.png', bbox_inches='tight')

###############################################################################
# # MP G4V/GR S5_H1 vs S5S6_H1H2L1V1 -- RANDOM SETS
# NSETS = 200
# HMAX = 5E-25
# NORMED = True
# RANGE = (-400, 400)
# NBINS = 100
# from matplotlib import pyplot as plt
# fig, ax = plt.subplots(1)
#
# # GR
# outdir = BASEDIR+'mp_iGR_sG4VGR_S5S6_H1L1V1/'
# mp_iGR_sG4VGR_S5S6_H1L1V1 = ppe.ResultsMatrix.collect(outdir+'results/(RUN)/(PSRJ)/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/(RUN)/(PSRJ)/inj_GR_(N).par', search_keys=('G4V', 'GR'), inject_key='GR', macro1='run', nf=9, macro1_values=['S5', 'S6'], macro2='psrj')
# mp_iGR_sG4VGR_S5S6_H1L1V1_sets = mp_iGR_sG4VGR_S5S6_H1L1V1.random_set(nsets=NSETS, constrain_param='HEFF', constrain_max_value=HMAX)
# keys = mp_iGR_sG4VGR_S5S6_H1L1V1._all_inner_keys()
# ax.hist(np.sign(mp_iGR_sG4VGR_S5S6_H1L1V1_sets)*np.sqrt(np.absolute(mp_iGR_sG4VGR_S5S6_H1L1V1_sets)), NBINS, linewidth=0, histtype='stepfilled', alpha=0.66, normed=NORMED, range=RANGE, facecolor="#005851", label='GR injections')
#
# # G4V
# outdir = BASEDIR+'mp_iG4V_sG4VGR_S5S6_H1L1/'
# mp_iG4V_sG4VGR_S5S6_H1H2L1V1 = ppe.ResultsMatrix.collect(outdir+'results/(RUN)/(PSRJ)/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/(RUN)/(PSRJ)/inj_G4V_(N).par', search_keys=('G4V', 'GR'), inject_key='G4V', macro1='run', nf=4, macro1_values=['S5'], macro2='psrj')
#
# mp_iG4V_sG4VGR_S5S6_H1H2L1V1_sets = mp_iG4V_sG4VGR_S5S6_H1H2L1V1.random_set(nsets=NSETS, constrain_param='HEFF', constrain_max_value=HMAX, keys=keys)
#
# ax.hist(np.sqrt(mp_iG4V_sG4VGR_S5S6_H1H2L1V1_sets), NBINS, linewidth=0, histtype='stepfilled', alpha=0.6, normed=NORMED, range=RANGE, facecolor="#7A303F", label='G4v injections')
#
# ax.set_xlabel(r"${\rm sgn}{\left(B^{\rm G4v}_{\rm GR}\right)}|\ln B^{\rm G4v}_{\rm GR}|^\frac{1}{2}$")
# ax.set_ylabel("Set count (total %i)" % NSETS)
# ax.legend(numpoints=1, prop={'size': 18}, loc='upper right')
# fig.savefig(FIGDIR + '/mp_iG4VGR_sG4VGR_500sets_hist.pdf', bbox_inches='tight')


####################################################################################################
# # CRAB ST/GR S5_H1H2L1V1 - GR injections
#
# NINST = 39
#
# # sSTGR
# outdir = BASEDIR+'crab_iGR_sSTGR_S5S6_H1H2L1V1/'
# crab_iGR_sSTGR_S5_H1H2L1V1 = ppe.Results.collect(outdir+'results/S5/J0534+2200/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/S5/J0534+2200/inj_GR_(N).par', search_keys=('ST', 'GR'), inject_key='GR', nf=NINST)
#
# fig, ax = crab_iGR_sSTGR_S5_H1H2L1V1.plot('heff', 'sqrtb', '.', color='#7A303F')
# ax.set_xlabel(r'$h_{\rm GR}$ (effective)')
# ax.set_ylabel(r"${\rm sgn}{\left(B^{\rm ST}_{\rm GR}\right)}|\ln B^{\rm ST}_{\rm GR}|^\frac{1}{2}$")
# fig.savefig(FIGDIR + '/crab_iGR_sSTGR_S5_H1H2L1V1.png', bbox_inches='tight')
#
# # sGR
# outdir = BASEDIR+'crab_iGR_sSTGR_S5S6_H1H2L1V1/'
# crab_iGR_sGR_S5_H1H2L1V1 = ppe.Results.collect(outdir+'results/S5/J0534+2200/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/S5/J0534+2200/inj_GR_(N).par', search_keys=('GR', 'n'), inject_key='GR', nf=NINST)
#
# fig, ax = crab_iGR_sGR_S5_H1H2L1V1.plot('heff', 'sqrtb', '.', color='#7A303F')
# ax.set_xlabel(r'$h_{\rm GR}$ (effective)')
# ax.set_ylabel(r"${\rm sgn}{\left(B^{\rm GR}_{\rm n}\right)}|\ln B^{\rm GR}_{\rm n}|^\frac{1}{2}$")
# fig.savefig(FIGDIR + '/crab_iGR_sGR_S5_H1H2L1V1.png', bbox_inches='tight')


#############################################################################
# CRAB ST/GR S5S6_H1H2L1V1 - ST injections
from matplotlib import pyplot as plt

NINST = 99
outdir = os.path.join(BASEDIR, 'crab_iST_sSTGR_S5S6_H1H2L1V1/')


def st_load_and_plot(key1, key2):
    # load
    outpath = os.path.join(outdir, 'results/(RUN)/J0534+2200/(M)/out_(N).txt_B'
                                   '.txt')
    injpath = os.path.join(outdir, 'injections/(RUN)/J0534+2200/inj_ST_(N).par')
    resultsdict = ppe.ResultsDict.collect(outpath, injpath=injpath,
                                          search_keys=(key1, key2),
                                          inject_key='ST', nf=NINST,
                                          wc='run',
                                          wc_values=['S5', 'S6'])
    results = resultsdict.fold(preserve_injections=True)
    gr = basic.Model('GR')
    h0 = results.getparam('H0')
    cosiota = results.getparam('COSIOTA')
    hscalar = results.getparam('HSCALARB')
    sqrtb = results.getparam('sqrtb')
    sbayes = []
    hgreff = []
    ratio = []
    for (h, ci, hb, b) in zip(h0, cosiota, hscalar, sqrtb):
        if h > 0:
            hgr = gr.heff(h/2.0, ci)
            hgreff.append(hgr)
            ratio.append(hb/hgr)
            sbayes.append(b)
    x = np.array(hgreff)
    y = np.array(ratio)
    c = np.array(sbayes)
    # plot
    fig, ax = plt.subplots(1)
    im = ax.scatter(x, y, c=c, lw=0, s=100)
    ax.set_xlabel(r'$h_{\rm GR}$ (effective)')
    ax.set_ylabel(r"$h_{\rm B}/h_{\rm GR}$")
    cb = plt.colorbar(im, ax=ax)
    cb.set_label(r"${\rm sgn}{\left(\ln B^{\rm %(k0)s}_{\rm %(k1)s}\right)}|"
                 r"\ln B^{\rm %(k0)s}_{\rm %(k1)s}|^\frac{1}{2}$"
                 % {'k0': key1, 'k1': key2})
    ax.set_ylim(-0.001, max(y)+0.001)
    ax.set_xlim(0, max(x)+max(x)/10.0)
    figpath = os.path.join(FIGDIR, 'crab_iST_s%s%s_r.png' % (key1, key2))
    fig.savefig(figpath, bbox_inches='tight')
    print "Saved figure: %s" % figpath
    plt.close()

st_load_and_plot('ST', 'GR')
st_load_and_plot('ST', 'n')
st_load_and_plot('GR', 'n')


###############################################################################
# # MP G4V/ST/GR S5S6_H1H2L1V1 -- OPEN BOX!
#
# # S5S6 H1H2L1V1
# outdir = BASEDIR+'mp_open_sG4VSTGR_S5S6_H1H2L1V1/'
#
# mp_open_sGR_S5S6_H1H2L1V1 = ppe.OpenResultsMatrix.collect(outdir+'results/(RUN)/(PSRJ)/(M)/out_(N).txt_B.txt', search_keys=('GR', 'n'), macro1='run', nf=0, macro1_values=['S5', 'S6'], pulsar_par='/home/misi/projects/cwpols/pulsar_par/')
# print mp_open_sGR_S5S6_H1H2L1V1.fold(preserve_injections=True).fold(full=True)
# fig, ax = mp_open_sGR_S5S6_H1H2L1V1.plot_by_run('FGW', 'B', "*")
# ax.legend(numpoints=1, prop={'size': 18}, loc='center left')
# ax.set_ylabel(r"$B^{\rm GR}_{\rm n}$")
# ax.set_xlabel(r"$f_{\rm GW}$")
# ax.set_xscale('log')
# ax.set_xlim(50, 1500)
# fig.savefig(FIGDIR + '/mp_open_sGR_S5S6_H1H2L1V1.pdf', bbox_inches='tight')

# mp_open_sG4V_S5S6_H1H2L1V1 = ppe.OpenResultsMatrix.collect(outdir+'results/(RUN)/(PSRJ)/(M)/out_(N).txt_B.txt', search_keys=('G4V', 'n'), macro1='run', nf=0, macro1_values=['S5', 'S6'], pulsar_par='/home/misi/projects/cwpols/pulsar_par/')
# mp_open_sG4V_S5S6_H1H2L1V1_folded = mp_open_sG4V_S5S6_H1H2L1V1.fold(preserve_injections=True)
# print mp_open_sG4V_S5S6_H1H2L1V1_folded.fold(full=True)
# fig, ax = mp_open_sG4V_S5S6_H1H2L1V1.plot_by_run('FGW', 'B', "*")
# ax.legend(numpoints=1, prop={'size': 18}, loc='lower left')
# ax.set_ylabel(r"$B^{\rm G4v}_{\rm n}$")
# ax.set_xlabel(r"$f_{\rm GW}$")
# ax.set_xscale('log')
# ax.set_xlim(50, 1500)
# fig.savefig(FIGDIR + '/mp_open_sG4V_S5S6_H1H2L1V1.pdf', bbox_inches='tight')
#
# mp_open_sG4VGR_S5S6_H1H2L1V1 = ppe.OpenResultsMatrix.collect(outdir+'results/(RUN)/(PSRJ)/(M)/out_(N).txt_B.txt', search_keys=('G4V', 'GR'), macro1='run', nf=0, macro1_values=['S5', 'S6'], pulsar_par='/home/misi/projects/cwpols/pulsar_par/')
# mp_open_sG4VGR_S5S6_H1H2L1V1_folded = mp_open_sG4VGR_S5S6_H1H2L1V1.fold(preserve_injections=True)
# print mp_open_sG4VGR_S5S6_H1H2L1V1_folded.fold(full=True)
# fig, ax = mp_open_sG4VGR_S5S6_H1H2L1V1.plot_by_run('FGW', 'B', "*")
# ax.legend(numpoints=1, prop={'size': 18}, loc='lower right')
# ax.set_ylabel(r"$B^{\rm G4v}_{\rm GR}$")
# ax.set_xlabel(r"$f_{\rm GW}$")
# ax.set_xscale('log')
# ax.set_xlim(50, 1500)
# fig.savefig(FIGDIR + '/mp_open_sG4VGR_S5S6_H1H2L1V1.pdf', bbox_inches='tight')
