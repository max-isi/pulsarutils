#! /usr/bin/env python

""" This is test script is part of the pulsarutils package. Its main function 
is to check that the probability distributions defined in pulsarutils/stats 
behave as expected by producing several key plots that can be compared to 
standard references.

Max Isi - max.isi@ligo.org
July 26, 201t6
"""

import os
import sys
import numpy as np
repodir = os.getenv('PULSARUTILS') or os.path.dirname(os.path.dirname(
    os.path.abspath(__file__)))
try:
    sys.path.append(repodir)
    from pulsarutils import stats
except ImportError, e:
    raise ImportError('stats module not found. Set $PULSARUTILS to point to'
                      'package location. Error: %r' % e)
from matplotlib import pyplot as plt

##############################################################################
# FERMI-DIRAC

print "Checking Fermi-Dirac distribution with mu=10, sigma=1."
print "Compare to: https://www.authorea.com/users/50521/articles/65214/_show_article"
fd = stats.fermidirac()
fd_frozen = fd(loc=10, scale=1)

# PDF
h = np.arange(0, 20, 0.1)
plt.figure()
plt.plot(h, fd_frozen.pdf(h))
plt.hist(fd_frozen.rvs(size=1e5), histtype='step', range=(0,20), bins=100,
         normed=True)
plt.xlim(0, 20)
plt.ylim(0, 0.1)
plt.xlabel(r'$h_0$')
plt.ylabel(r'$p(h_0|\mu=10, \sigma=1,I)$')
figname = "fermidirac_mu10_sigma1_pdf.png"
plt.savefig(figname, bbox_inches='tight')
plt.close()
print "PDF plot saved: %r" % figname

# CDF
plt.figure()
plt.plot(h, fd_frozen.cdf(h))
plt.hist(fd_frozen.rvs(size=1e5), histtype='step', range=(0,20), bins=100, 
         normed=True, cumulative=True)
plt.xlim(0, 20)
plt.ylim(0, 1)
plt.xlabel(r'$h_0$')
plt.ylabel(r'$C(h_0|\mu=10,\sigma=1,I)$')
figname = "fermidirac_mu10_sigma1_cdf.png"
plt.savefig(figname, bbox_inches='tight')
plt.close()
print "CDF plot saved: %r" % figname

# PPF
cdf = np.arange(0, 1, 1e-4)
plt.figure()
plt.plot(cdf, fd_frozen.ppf(cdf))
plt.xlim(0, 1)
plt.ylim(0, 20)
plt.xlabel(r'$C(h_0|\mu=10,\sigma=1,I)$')
plt.ylabel(r'$h_0$')
figname = "fermidirac_mu10_sigma1_ppf.png"
plt.savefig(figname, bbox_inches='tight')
plt.close()
print "CDF plot saved: %r" % figname


##############################################################################
# LOGUNIFORM

loghrange = (-26, -24)
print "Checking loguniform (1/x) distribution with x in (1e%i, 1e%i)." \
      % loghrange
hrange = tuple([10**logh for logh in loghrange])
lu = stats.loguniform(a=hrange[0], b=hrange[1])

# PDF
h = np.logspace(loghrange[0], loghrange[1], 100)
plt.figure()
plt.plot(h, lu.pdf(h))
plt.hist(lu.rvs(size=1e5), histtype='step', bins=h, normed=True)
plt.xlim(*hrange)
plt.yscale('log')
plt.xscale('log')
plt.xlabel(r'$h_0$')
plt.ylabel(r'$p\left(h_0\left|10^{%i}<h<10^{%i},I\right.\right)$' % loghrange)
figname = "loguniform_1e%i_1e%i_pdf.png" % loghrange
plt.savefig(figname, bbox_inches='tight')
plt.close()
print "PDF plot saved: %r" % figname

# CDF
plt.figure()
plt.plot(h, lu.cdf(h))
plt.hist(lu.rvs(size=1e5), histtype='step', bins=h, normed=True, cumulative=1)
plt.xlim(*hrange)
plt.ylim(0, 1)
plt.xscale('log')
plt.xlabel(r'$h_0$')
plt.ylabel(r'$C\left(h_0\left|10^{%i}<h<10^{%i},I\right.\right)$' % loghrange)
figname = "loguniform_1e%i_1e%i_cdf.png" % loghrange 
plt.savefig(figname, bbox_inches='tight')
plt.close()
print "CDF plot saved: %r" % figname

# PPF
cdf = np.arange(0, 1, 1e-4)
plt.figure()
plt.plot(cdf, lu.ppf(cdf))
plt.xlim(0, 1)
plt.ylim(*hrange)
plt.yscale('log')
plt.xlabel(r'$C\left(h_0\left|10^{%i}<h<10^{%i},I\right.\right)$' % loghrange)
plt.ylabel(r'$h_0$')
figname = "loguniform_1e%i_1e%i_ppf.png" % loghrange 
plt.savefig(figname, bbox_inches='tight')
plt.close()
print "CDF plot saved: %r" % figname
