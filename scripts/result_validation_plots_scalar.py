#! /usr/bin/env python

import os
import sys
import numpy as np
repodir = os.getenv('PULSARUTILS') or os.path.dirname(os.path.dirname(
    os.path.abspath(__file__)))
try:
    sys.path.append(repodir)
    from pulsarutils import ppe, basic
except ImportError, e:
    raise ImportError('ppe module not found. Set $PULSARUTILS to point to'
                      'package location. Error: %r' %r)

BASEDIR = os.path.join(os.getenv('HOME'), 'projects/cwpols/result_validation_20160423/')
FIGDIR = os.path.join(os.getenv('HOME'), 'public_html/cw/result_validation/')

################################################################################
# # CRAB ST/GR S5_H1H2L1V1 - GR injections
#
# NINST = 39
#
# # sSTGR
# outdir = BASEDIR+'crab_iGR_sSTGR_S5S6_H1H2L1V1/'
# crab_iGR_sSTGR_S5_H1H2L1V1 = ppe.Results.collect(outdir+'results/S5/J0534+2200/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/S5/J0534+2200/inj_GR_(N).par', search_keys=('ST', 'GR'), inject_key='GR', nf=NINST)
#
# fig, ax = crab_iGR_sSTGR_S5_H1H2L1V1.plot('heff', 'sqrtb', '.', color='#7A303F')
# ax.set_xlabel(r'$h_{\rm GR}$ (effective)')
# ax.set_ylabel(r"${\rm sgn}{\left(B^{\rm ST}_{\rm GR}\right)}|\ln B^{\rm ST}_{\rm GR}|^\frac{1}{2}$")
# fig.savefig(FIGDIR + '/crab_iGR_sSTGR_S5_H1H2L1V1.png', bbox_inches='tight')
#
# # sGR
# outdir = BASEDIR+'crab_iGR_sSTGR_S5S6_H1H2L1V1/'
# crab_iGR_sGR_S5_H1H2L1V1 = ppe.Results.collect(outdir+'results/S5/J0534+2200/(M)/out_(N).txt_B.txt', injpath=outdir+'injections/S5/J0534+2200/inj_GR_(N).par', search_keys=('GR', 'n'), inject_key='GR', nf=NINST)
#
# fig, ax = crab_iGR_sGR_S5_H1H2L1V1.plot('heff', 'sqrtb', '.', color='#7A303F')
# ax.set_xlabel(r'$h_{\rm GR}$ (effective)')
# ax.set_ylabel(r"${\rm sgn}{\left(B^{\rm GR}_{\rm n}\right)}|\ln B^{\rm GR}_{\rm n}|^\frac{1}{2}$")
# fig.savefig(FIGDIR + '/crab_iGR_sGR_S5_H1H2L1V1.png', bbox_inches='tight')


#############################################################################
# CRAB ST/GR S5S6_H1H2L1V1 - ST injections
from matplotlib import pyplot as plt

NINST = 499
outdir = os.path.join(BASEDIR, 'crab_iST_sGRG4V_S5S6_H1H2L1V1_fixed/')

def st_load_and_plot(key1, key2):
    # load
    outpath = os.path.join(outdir, 'results/(RUN)/J0534+2200/(M)/out_(N).txt_B'
                                   '.txt')
    injpath = os.path.join(outdir, 'injections/(RUN)/J0534+2200/inj_ST_(N).par')
    resultsdict = ppe.ResultsDict.collect(outpath, injpath=injpath,
                                          search_keys=(key1, key2),
                                          inject_key='ST', nf=NINST,
                                          wc='run',
                                          wc_values=['S5', 'S6'])
    results = resultsdict.fold()
    gr = basic.Model('GR')
    h0 = results.getparam('H0')
    cosiota = results.getparam('COSIOTA')
    hscalar = results.getparam('HSCALARB')
    sqrtb = results.getparam('sqrtb')
    sbayes = []
    hgreff = []
    ratio = []
    for (h, ci, hb, b) in zip(h0, cosiota, hscalar, sqrtb):
        if h > 0:
            hgr = gr.heff(h/2.0, ci)
            hgreff.append(hgr)
            ratio.append(hb/hgr)
            sbayes.append(b)
    x = np.array(hgreff)
    y = np.array(ratio)
    c = np.array(sbayes)
    # plot
    fig, ax = plt.subplots(1)
    im = ax.scatter(x, y, c=c, lw=0, s=100)
    ax.set_xlabel(r'$h_{\rm GR}$ (effective)')
    ax.set_ylabel(r"$h_{\rm B}/h_{\rm GR}$")
    cb = plt.colorbar(im, ax=ax)
    cb.set_label(r"${\rm sgn}{\left(\ln B^{\rm %(k0)s}_{\rm %(k1)s}\right)}|"
                 r"\ln B^{\rm %(k0)s}_{\rm %(k1)s}|^\frac{1}{2}$"
                 % {'k0': key1, 'k1': key2})
    ax.set_ylim(-0.001, max(y)+0.001)
    ax.set_xlim(0, max(x)+max(x)/10.0)
    figpath = os.path.join(FIGDIR, 'crab_iST_s%s%s_r.png' % (key1, key2))
    fig.savefig(figpath, bbox_inches='tight')
    print "Saved figure: %s" % figpath
    plt.close()

st_load_and_plot('ST', 'GR')
st_load_and_plot('ST', 'n')
st_load_and_plot('GR', 'n')

