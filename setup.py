#!/usr/bin/env python

from distutils.core import setup

setup(name='pulsarutils',
      version='1.0',
      description='GW pulsar utilities',
      author='Maximiliano Isi',
      author_email='max.isi@ligo.org',
      url='https://',
      packages=['pulsarutils'],
     )
