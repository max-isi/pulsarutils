#! /usr/bin/env python

import argparse
import os
import sys
from warnings import warn
from copy import copy, deepcopy
import numpy as np

"""Targeted CW Bayesian model selection post processing.
"""

##############################################################################
# -- PARSE ARGUMENTS --

argparser = argparse.ArgumentParser()

subparsers = argparser.add_subparsers(dest="subparser_name")
open_parser = subparsers.add_parser("open")
closed_parser = subparsers.add_parser("closed")

for parser in [open_parser, closed_parser]:
    parser.add_argument("configpath", help="Path to configuration file.")
    parser.add_argument("-o", "--out", default=os.getcwd(),
                        help="Output directory (created if needed)")
    parser.add_argument("-s", "--search-models", nargs='+', default=[],
                        help="Keys of models to load.")
    parser.add_argument("-p", "--psrj", nargs='+', help="PSRJ pulsar IDs.")
    parser.add_argument("-r", "--runs", nargs='+', help="Runs to be analyzed.")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-e", "--ext", default='pdf', help="Image extension.")
    parser.add_argument("--plot-all-psrs", action="store_true",
                        help="Produce single-pulsar plots for all sources.")
    parser.add_argument("--suffix", default='')
    parser.add_argument("--spectra", nargs='+')
    parser.add_argument("--hist-grid-format")
    parser.add_argument("--credible-level", type=int, default=95)
    parser.add_argument("--pulsarutils", help="Path to pulsarutils repo (opt)")
    parser.add_argument("--figsize", nargs='+', default=(13.5, 5))
    parser.add_argument("--ylim", nargs='+')
# options exclusive to closed mode
closed_parser.add_argument("--nmax", default=np.inf, type=int,
                           help="Maximum background instantiations.")

args = argparser.parse_args()
CONFIG_PATH = os.path.abspath(args.configpath)
VERBOSE = args.verbose
MODE = args.subparser_name
EXT = args.ext
SUFFIX = args.suffix

CL = args.credible_level

if MODE == 'open':
    SUFFIX = "_open%s" % SUFFIX
    nmax = 1
elif MODE == 'closed':
    nmax = args.nmax
else:
    raise ValueError('invalid mode %r' % MODE)

if not os.path.isfile(CONFIG_PATH):
    raise IOError("invalid configuration file %r" % CONFIG_PATH)

# Import pulsarutils
repodir = os.getenv('PULSARUTILS') or args.pulsarutils
try:
    sys.path.append(repodir)
    from pulsarutils import ppe, basic, plots
except ImportError, e:
    raise ImportError('ppe module not found. Set $PULSARUTILS to point to'
                      'package location. Error: %r' % e)
# import seaborn as sns
from matplotlib import pyplot as plt
tokenize = basic.tokenize
get_label = basic.get_label


##############################################################################
# -- PARSE CONFIGURATION FILE --

configfile = ppe.ConfigFile()
configfile.read(CONFIG_PATH, mode=MODE)

# General
IFOS = configfile.ifos
RUNS = args.runs or configfile.runs
PSRJ = args.psrj or configfile.psrj
TOTAL_N = min(configfile.total_n, nmax)
NLIVE = configfile.ppe['nlive']

# Paths
# Expected: injections, par, exe, logs, results, repo, prior, dag,
#           bashscript, temp_out, output, data, sub
PATHS = configfile.paths
BPATH = configfile.bpath()
res_file, res_ext = os.path.splitext(PATHS['results'])
PPATH = "%s_pos%s" % (res_file,res_ext)

# Search models
SEARCH_MODELS = basic.ModelList(args.search_models) or configfile.search_models
SEARCH_MODEL_KEYS = SEARCH_MODELS.keys

# Create output directory
ANALYSIS_NAME = configfile.name
OUTPUT_DIR = os.path.join(os.path.abspath(args.out), "%s_post/" % ANALYSIS_NAME)
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)
else:
    warn("output directory already exists, may overwrite existing plots.")

# Setup spectra for plotting
if args.spectra:
    FREQS = np.logspace(1, np.log10(5e3), 1e4)
    if args.spectra[0] == 'none':
        EFFASD = None
    else:
        EFFASD = basic.SpectrumList.load(args.spectra, kind='asd').hmean_asd()
elif MODE == 'closed':
    FREQS = np.logspace(0.5, 4, 1e4)
    EFFASD = basic.SpectrumList.from_lal(IFOS, freq=FREQS).hmean_asd()
else:
    FREQS = np.logspace(1, np.log10(5e3), 1e4)
    EFFASD = None

spec_defaults = {
    'open': {
        'spec_color': 'gray',
        'spec_alpha': 0.5,
        'spec_lw': 1,
    },
    'closed': {
        'spec_color': 'red',
        'spec_alpha': 1,
        'spec_lw': 2,
    }
}


##############################################################################
# -- FUNCTIONS --

def freqplot(rdict, inject_key='none', suffix=SUFFIX, ext=EXT,
             output_dir=OUTPUT_DIR, name='lnb', **kwargs):
    """Wrapper around `plots.freqplot`.
    """
    m0k, m1k = rdict.search_keys
    inject_key = inject_key or rdict.inject_key
    fig, ax1, ax2 = plots.freqplot(rdict, yscale='log', **kwargs)
    for ax in [ax1, ax2]:
        ax.xaxis.label.set_fontsize(16)
    figname = "freqplot_ul_%s_i-%s_s-%s-%s%s.%s" \
              % (name, inject_key, m0k, m1k, suffix, ext)
    figpath = os.path.join(output_dir, figname)
    fig.savefig(figpath, bbox_inches='tight')
    plt.close(fig)
    print "Figure saved: %s" % figpath


def load_posteriors(results, mkey='', psrj=''):
    print "Loading posteriors..."
    mkeys = results.search_keys()
    mkey = mkey or mkeys[0]
    ppath = tokenize(PPATH, run=RUNS[0], psrj=psrj, m=mkeys[0])
    if results.posteriors is None:
        results.load_posteriors(ppath, NLIVE)
    hkeys = basic.Model(mkey).uls
    print "%s: %r" % (mkey, hkeys)
    hul_dict = {hk: results.posterior_percentiles(hk, CL) for hk in hkeys}
    return hul_dict


##############################################################################
# -- LOAD AND PLOT --

print "Models to load:"
print SEARCH_MODELS.keys

# dictionary containing scalar upper limits
hul_matrix = {m.key: {uk: {} for uk in m.uls} for m in SEARCH_MODELS}

# if a list of PSRJs was provided use, those; otherwise, just load
# automatically from disk
wc2val = {run: PSRJ for run in RUNS} if PSRJ else None
results_model_matrix = ppe.ResultsModelMatrix({})
PSRJ = set()

# Note: I'm leaving the treatment of runs as it is in case we want to combine
# runs in a smarter way in the future.
for model in SEARCH_MODELS:
    m = model.key
    results_matrix = ppe.ResultsMatrix.collect(BPATH, search_keys=(m, 'n'),
                                               nf=TOTAL_N-1,
                                               parpath=PATHS['par'],
                                               wc2_values=wc2val)
    if len(RUNS) == 1:
        warn("Overflow! Taking first run.")
        results_dict = results_matrix[RUNS[0]]
    else:
        raise ValueError("Unable to handle multiple runs simultaneously.")
    PSRJ = PSRJ.union(results_dict.keys())
    results_model_matrix[m] = results_dict
    # load posteriors
    for psrj, results in results_dict.iteritems():
        hul_dict = load_posteriors(results, psrj=psrj)
        for hul_key, post_dict in hul_dict.iteritems():
            hul_matrix[m][hul_key][psrj] = post_dict

print "Number of pulsars: %i" % len(PSRJ)

# HUL VS FREQ PLOTS
spec = EFFASD.asd() if EFFASD is not None else None
if len(PSRJ) >= 1:
    for mkey, hul_param_dict in hul_matrix.iteritems():
        for hkey, hdict in hul_param_dict.iteritems():
            ylabel = get_label(hkey, ul=CL)
            freqplot(results_model_matrix[mkey], yval=hdict, ylabel=ylabel,
                     name=hkey, spectrum=spec, figsize=args.figsize,
                     ylim=args.ylim, **spec_defaults[MODE])

with open(os.path.join(OUTPUT_DIR, "config.ini"), 'w') as f:
    configfile.write(f)

command_string = ''
for argument in sys.argv:
    command_string += '%s ' % argument

with open(os.path.join(OUTPUT_DIR, "postproc_alluls_command.txt"), 'w') as f:
    f.write('DIRECTORY:\n%s\n\n' % os.getcwd())
    f.write('COMMAND:\n%s\n' % command_string)
